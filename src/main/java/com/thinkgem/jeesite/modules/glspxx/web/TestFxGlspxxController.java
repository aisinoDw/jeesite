/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.glspxx.web;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.glspxx.entity.TestFxGlspxx;
import com.thinkgem.jeesite.modules.glspxx.service.TestFxGlspxxService;

/**
 * 1Controller
 * @author 1
 * @version 2016-01-26
 */
@Controller
@RequestMapping(value = "${adminPath}/glspxx/testFxGlspxx")
public class TestFxGlspxxController extends BaseController {

	@Autowired
	private TestFxGlspxxService testFxGlspxxService;
	
	@ModelAttribute
	public TestFxGlspxx get(@RequestParam(required=false) String id) {
		TestFxGlspxx entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = testFxGlspxxService.get(id);
		}
		if (entity == null){
			entity = new TestFxGlspxx();
		}
		return entity;
	}
	
	@RequiresPermissions("glspxx:testFxGlspxx:view")
	@RequestMapping(value = {"list", ""})
	public String list(TestFxGlspxx testFxGlspxx, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<TestFxGlspxx> page = testFxGlspxxService.findPage(new Page<TestFxGlspxx>(request, response), testFxGlspxx); 
		model.addAttribute("page", page);
		return "modules/glspxx/testFxGlspxxList";
	}
	
	
	@RequiresPermissions("glspxx:testFxGlspxx:view")
	@RequestMapping(value = {"ajaxglspxx"})
	@ResponseBody
	public List<List<TestFxGlspxx>> ajaxglspxx(TestFxGlspxx testFxGlspxx, HttpServletRequest request, HttpServletResponse response, Model model) throws UnsupportedEncodingException {
	//	String guanxi = URLDecoder.decode(request.getParameter("guanxi"),"utf-8");
		String guanxi =  new String(request.getParameter("guanxi").getBytes("ISO-8859-1"), "UTF-8");

		String st1 = guanxi.split("_")[0];
		String st2 = guanxi.split("_")[1];
		System.out.println("st1:"+st1);
		System.out.println("st2:"+st2);
		
		List<List<TestFxGlspxx>> listout = new ArrayList<List<TestFxGlspxx>>();
		testFxGlspxx.setSpmc(st1);
		List<TestFxGlspxx> list1 = testFxGlspxxService.findListGraph(testFxGlspxx);
		listout.add(list1);
		testFxGlspxx.setSpmc(st2);
		List<TestFxGlspxx> list2 = testFxGlspxxService.findListGraph(testFxGlspxx);
		listout.add(list2);
		return listout;

	}

	@RequiresPermissions("glspxx:testFxGlspxx:view")
	@RequestMapping(value = "form")
	public String form(TestFxGlspxx testFxGlspxx, Model model) {
		model.addAttribute("testFxGlspxx", testFxGlspxx);
		return "modules/glspxx/testFxGlspxxForm";
	}

	@RequiresPermissions("glspxx:testFxGlspxx:edit")
	@RequestMapping(value = "save")
	public String save(TestFxGlspxx testFxGlspxx, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, testFxGlspxx)){
			return form(testFxGlspxx, model);
		}
		testFxGlspxxService.save(testFxGlspxx);
		addMessage(redirectAttributes, "保存商品成功");
		return "redirect:"+Global.getAdminPath()+"/glspxx/testFxGlspxx/?repage";
	}
	
	@RequiresPermissions("glspxx:testFxGlspxx:edit")
	@RequestMapping(value = "delete")
	public String delete(TestFxGlspxx testFxGlspxx, RedirectAttributes redirectAttributes) {
		testFxGlspxxService.delete(testFxGlspxx);
		addMessage(redirectAttributes, "删除商品成功");
		return "redirect:"+Global.getAdminPath()+"/glspxx/testFxGlspxx/?repage";
	}

}