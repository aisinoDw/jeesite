/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.glspxx.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 1Entity
 * @author 1
 * @version 2016-01-26
 */
public class TestFxGlspxx extends DataEntity<TestFxGlspxx> {
	
	private static final long serialVersionUID = 1L;
	private String spmc;		// 商品名称
	private Double hjje;		// 合计金额
	private String kprq;		// 开票日期
	private String nsrmc;		// 纳税人名称
	
	public TestFxGlspxx() {
		super();
	}

	public TestFxGlspxx(String id){
		super(id);
	}

	@Length(min=0, max=255, message="商品名称长度必须介于 0 和 255 之间")
	public String getSpmc() {
		return spmc;
	}

	public void setSpmc(String spmc) {
		this.spmc = spmc;
	}
	
	public Double getHjje() {
		return hjje;
	}

	public void setHjje(Double hjje) {
		this.hjje = hjje;
	}
	
	@Length(min=0, max=255, message="开票日期长度必须介于 0 和 255 之间")
	public String getKprq() {
		return kprq;
	}

	public void setKprq(String kprq) {
		this.kprq = kprq;
	}
	
	@Length(min=0, max=255, message="纳税人名称长度必须介于 0 和 255 之间")
	public String getNsrmc() {
		return nsrmc;
	}

	public void setNsrmc(String nsrmc) {
		this.nsrmc = nsrmc;
	}
	
}