/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.glspxx.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.glspxx.entity.TestFxGlspxx;
import com.thinkgem.jeesite.modules.glspxx.dao.TestFxGlspxxDao;

/**
 * 1Service
 * @author 1
 * @version 2016-01-26
 */
@Service
@Transactional(readOnly = true)
public class TestFxGlspxxService extends CrudService<TestFxGlspxxDao, TestFxGlspxx> {

	public TestFxGlspxx get(String id) {
		return super.get(id);
	}
	
	public List<TestFxGlspxx> findList(TestFxGlspxx testFxGlspxx) {
		return super.findList(testFxGlspxx);
	}
	
	public Page<TestFxGlspxx> findPage(Page<TestFxGlspxx> page, TestFxGlspxx testFxGlspxx) {
		return super.findPage(page, testFxGlspxx);
	}
	
	@Transactional(readOnly = false)
	public void save(TestFxGlspxx testFxGlspxx) {
		super.save(testFxGlspxx);
	}
	
	@Transactional(readOnly = false)
	public void delete(TestFxGlspxx testFxGlspxx) {
		super.delete(testFxGlspxx);
	}
	
}