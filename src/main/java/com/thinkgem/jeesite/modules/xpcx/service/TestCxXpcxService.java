/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.xpcx.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.xpcx.entity.TestCxXpcx;
import com.thinkgem.jeesite.modules.xpcx.dao.TestCxXpcxDao;

/**
 * 1Service
 * @author 1
 * @version 2016-02-19
 */
@Service
@Transactional(readOnly = true)
public class TestCxXpcxService extends CrudService<TestCxXpcxDao, TestCxXpcx> {

	public TestCxXpcx get(String id) {
		return super.get(id);
	}
	
	public List<TestCxXpcx> findList(TestCxXpcx testCxXpcx) {
		return super.findList(testCxXpcx);
	}
	
	public Page<TestCxXpcx> findPage(Page<TestCxXpcx> page, TestCxXpcx testCxXpcx) {
		return super.findPage(page, testCxXpcx);
	}
	
	@Transactional(readOnly = false)
	public void save(TestCxXpcx testCxXpcx) {
		super.save(testCxXpcx);
	}
	
	@Transactional(readOnly = false)
	public void delete(TestCxXpcx testCxXpcx) {
		super.delete(testCxXpcx);
	}
	
}