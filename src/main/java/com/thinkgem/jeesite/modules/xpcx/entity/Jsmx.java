package com.thinkgem.jeesite.modules.xpcx.entity;

public class Jsmx {
	private String kpbz;
	private String kh;
	private String je;
	private String xphm;
	private String jsfs;
	private String jskzzd;
	private String xpdm;
	private String jsfsxh;
	
	public String getKpbz() {
		return kpbz;
	}
	public void setKpbz(String kpbz) {
		this.kpbz = kpbz;
	}
	public String getKh() {
		return kh;
	}
	public void setKh(String kh) {
		this.kh = kh;
	}
	public String getJe() {
		return je;
	}
	public void setJe(String je) {
		this.je = je;
	}
	public String getXphm() {
		return xphm;
	}
	public void setXphm(String xphm) {
		this.xphm = xphm;
	}
	public String getJsfs() {
		return jsfs;
	}
	public void setJsfs(String jsfs) {
		this.jsfs = jsfs;
	}
	public String getJskzzd() {
		return jskzzd;
	}
	public void setJskzzd(String jskzzd) {
		this.jskzzd = jskzzd;
	}
	public String getXpdm() {
		return xpdm;
	}
	public void setXpdm(String xpdm) {
		this.xpdm = xpdm;
	}
	public String getJsfsxh() {
		return jsfsxh;
	}
	public void setJsfsxh(String jsfsxh) {
		this.jsfsxh = jsfsxh;
	}
	
}
