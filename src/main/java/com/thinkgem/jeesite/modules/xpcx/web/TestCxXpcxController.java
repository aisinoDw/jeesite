/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.xpcx.web;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.impl.util.json.JSONArray;
import org.activiti.engine.impl.util.json.JSONObject;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.modules.xpcx.entity.Jsmx;
import com.thinkgem.jeesite.modules.xpcx.entity.TestCxXpcx;
import com.thinkgem.jeesite.modules.xpcx.entity.Xpmx;
import com.thinkgem.jeesite.modules.xpcx.entity.Xpxx;
import com.thinkgem.jeesite.modules.xpcx.service.TestCxXpcxService;


/**
 * 1Controller
 * @author 1
 * @version 2016-02-19
 */
@Controller
@RequestMapping(value = "${adminPath}/xpcx/testCxXpcx")
public class TestCxXpcxController extends BaseController {

	@Autowired
	private TestCxXpcxService testCxXpcxService;
	
	@ModelAttribute
	public TestCxXpcx get(@RequestParam(required=false) String id) {
		TestCxXpcx entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = testCxXpcxService.get(id);
		}
		if (entity == null){
			entity = new TestCxXpcx();
		}
		return entity;
	}
	
	@RequiresPermissions("xpcx:testCxXpcx:view")
	@RequestMapping(value = {"list", ""})
	public String list(TestCxXpcx testCxXpcx, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<TestCxXpcx> page = testCxXpcxService.findPage(new Page<TestCxXpcx>(request, response), testCxXpcx); 
		model.addAttribute("page", page);
		return "modules/xpcx/testCxXpcxList";
	}
	
	
	
	@RequiresPermissions("xpcx:testCxXpcx:view")
	@RequestMapping(value = {"xpxxjson"})
	public String xpxxjson(TestCxXpcx testCxXpcx, HttpServletRequest request, HttpServletResponse response, Model model) throws Exception{
		String url = "http://192.168.2.176:8889/DSCSClient";
		HttpPost httpPost = new HttpPost(url);
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		String cmd = "queryReceipt";
		nvps.add(new BasicNameValuePair("cmd", cmd));  
		
		String nsrsbhParam = testCxXpcx.getNsrsbh();	
		String ywlsh = testCxXpcx.getYwlsh();
		String bsqsrq = testCxXpcx.getBsqsrq();
		String bsjzrq = testCxXpcx.getBsjzrq();
		String xpdmParam = testCxXpcx.getXpdm();	//为了避免命名冲突
		String xphmParam = testCxXpcx.getXphm();
		String keys = "";
		if(StringUtils.isNotBlank(nsrsbhParam)){
			if(keys.length()>0){
				keys += ",";
			}
			keys += "\"nsrsbh\":\""+nsrsbhParam+"\"";
		}
		if(StringUtils.isNotBlank(ywlsh)){
			if(keys.length()>0){
				keys += ",";
			}
			keys += "\"lsh\":\""+ywlsh+"\"";
		}
		if(StringUtils.isNotBlank(bsqsrq)){
			if(keys.length()>0){
				keys += ",";
			}
			keys += "\"KPRQ_1\":\""+bsqsrq+"\"";
		}
		if(StringUtils.isNotBlank(bsjzrq)){
			if(keys.length()>0){
				keys += ",";
			}
			keys += "\"KPRQ_2\":\""+bsjzrq+"\"";
		}
		if(StringUtils.isNotBlank(xpdmParam)){
			if(keys.length()>0){
				keys += ",";
			}
			keys += "\"xpdm\":\""+xpdmParam+"\"";
		}
		if(StringUtils.isNotBlank(xphmParam)){
			if(keys.length()>0){
				keys += ",";
			}
			keys += "\"xphm\":\""+xphmParam+"\"";
		}
		nvps.add(new BasicNameValuePair("arg", "{\"Keys\":{"+keys+"}}")); 
		httpPost.setEntity(new UrlEncodedFormEntity(nvps));
		HttpClient httpclient = new DefaultHttpClient();	
		HttpResponse resp= httpclient.execute(httpPost);
		
		HttpEntity entity = resp.getEntity();
		String jsonresp = EntityUtils.toString(entity);
		logger.info("jsonresp====="+jsonresp);
		JSONObject json = new JSONObject(jsonresp);
		List<Xpxx> xpxxlist = new ArrayList<Xpxx>();
		//正常调用
		if(json.getInt("CompleteCode")==0){
			JSONArray jsonarray = json.getJSONArray("Data");
			for(int i=0;i<jsonarray.length();i++){
				//logger.info("#####  i===="+i);
				JSONObject o = jsonarray.getJSONObject(i);
				Xpxx xp = new Xpxx();
				String lzxphm = o.getString("lzxphm");
				String hykzzd = o.getString("hykzzd");
				String nsrsbh = o.getString("nsrsbh");
				String bz = o.getString("bz");
				String hjse = o.getDouble("hjse")+"";
				String gflxfs = o.getString("gflxfs");
				String bkkpsm = o.getString("bkkpsm");
				String lzxpdm = o.getString("lzxpdm");
				String gfsh = o.getString("gfsh");
				String kpjh = o.getString("kpjh");
				String jym = o.getString("jym");
				String xfsh = o.getString("xfsh");
				String sbbh = o.getString("sbbh");
				String dkbz = o.getString("dkbz");
				String sky = o.getString("sky");
				String gfmc = o.getString("gfmc");
				String xfdzdh = o.getString("xfdzdh");
				String bkkpje = o.getString("bkkpje");
				String xfmc = o.getString("xfmc");
				String xpdm = o.getString("xpdm");
				String hjje = o.getDouble("hjje")+"";
				String lsh = o.getString("lsh");
				String nsrmc = o.getString("nsrmc");
				String xphm = o.getString("xphm");
				String bxbz = o.getString("bxbz");
				String dknsrsbh = o.getString("dknsrsbh");
				String yqjg = o.getString("yqjg");
				String fmrq = o.getString("fmrq");
				String thhyy = o.getString("thhyy");
				String xfyhzh = o.getString("xfyhzh");
				String xslx = o.getString("xslx");
				String sign = o.getString("sign");
				String y_lsh = o.getString("y_lsh");
				String dknsrmc = o.getString("dknsrmc");
				String sphhs = o.getString("sphhs");
				String jshj = o.getString("jshj");
				String jylx = o.getString("jylx");
				String kprq = o.getString("kprq");
				String jyxplb = o.getString("jyxplb");
				xp.setLzxphm(lzxphm);
				xp.setHykzzd(hykzzd);
				xp.setNsrsbh(nsrsbh);
				xp.setBz(bz);
				xp.setHjse(hjse);
				xp.setGflxfs(gflxfs);
				xp.setBkkpsm(bkkpsm);
				xp.setLzxpdm(lzxpdm);
				xp.setGfsh(gfsh);
				xp.setKpjh(kpjh);
				xp.setJym(jym);
				xp.setXfsh(xfsh);
				xp.setSbbh(sbbh);
				xp.setDkbz(dkbz);
				xp.setSky(sky);
				xp.setGfmc(gfmc);
				xp.setXfdzdh(xfdzdh);
				xp.setBkkpje(bkkpje);
				xp.setXfmc(xfmc);
				xp.setXpdm(xpdm);
				xp.setHjje(hjje);
				xp.setLsh(lsh);
				xp.setNsrmc(nsrmc);
				xp.setXphm(xphm);
				xp.setBxbz(bxbz);
				xp.setDknsrsbh(dknsrsbh);
				xp.setYqjg(yqjg);
				xp.setFmrq(fmrq);
				xp.setThhyy(thhyy);
				xp.setXfyhzh(xfyhzh);
				xp.setXslx(xslx);
				xp.setSign(sign);
				xp.setY_lsh(y_lsh);
				xp.setDknsrmc(dknsrmc);
				xp.setSphhs(sphhs);
				xp.setJshj(jshj);
				xp.setJylx(jylx);
				xp.setKprq(kprq);
				xp.setJyxplb(jyxplb);
				
				List<Xpmx> mxlist = new ArrayList<Xpmx>();
				JSONArray mxjson = o.getJSONArray("mxlist");
				for(int j=0;j<mxjson.length();j++){
					JSONObject p = mxjson.getJSONObject(j);
					Xpmx x = new Xpmx();
					String sl = p.getString("sl");
					String xphm2 = p.getString("xphm");
					String xmkzzd = p.getString("xmkzzd");
					String jldw = p.getString("jldw");
					String se = p.getString("se");
					String shul = p.getString("shul");
					String xmsx = p.getString("xmsx");
					String je = p.getString("je");
					String mc = p.getString("mc");
					String dj = p.getString("dj");
					String ggxh = p.getString("ggxh");
					String xmbm = p.getString("xmbm");
					String xpdm2 = p.getString("xpdm");
					String mxxh = p.getString("mxxh");
					x.setSl(sl);
					x.setXphm(xphm2);
					x.setXmkzzd(xmkzzd);
					x.setJldw(jldw);
					x.setSe(se);
					x.setShul(shul);
					x.setXmsx(xmsx);
					x.setJe(je);
					x.setMc(mc);
					x.setDj(dj);
					x.setGgxh(ggxh);
					x.setXmbm(xmbm);
					x.setXpdm(xpdm2);
					x.setMxxh(mxxh);
					mxlist.add(x);
				}
				xp.setMxlist(mxlist);
				
				
				List<Jsmx> jslist = new ArrayList<Jsmx>();
				JSONArray jsjson = o.getJSONArray("jslist");
				for(int k=0;k<jsjson.length();k++){
					JSONObject q = jsjson.getJSONObject(k);
					Jsmx js = new Jsmx();
					String kpbz = q.getString("kpbz");
					String kh = q.getString("kh");
					String je = q.getString("je");
					String xphm3 = q.getString("xphm");
					String jsfs = q.getString("jsfs");
					String jskzzd = q.getString("jskzzd");
					String xpdm3 = q.getString("xpdm");
					String jsfsxh = q.getString("jsfsxh");	
					js.setKpbz(kpbz);
					js.setKh(kh);
					js.setJe(je);
					js.setXphm(xphm3);
					js.setJsfs(jsfs);
					js.setJskzzd(jskzzd);
					js.setXpdm(xpdm3);
					js.setJsfsxh(jsfsxh);
					jslist.add(js);
				}
				xp.setJslist(jslist);
				xpxxlist.add(xp);
			}
			
			
			logger.info(json.toString());
		}else{
			logger.info(json.toString());
			throw new Exception("接口调用失败!");
			
		}
		
		model.addAttribute("page1", xpxxlist);
		return "modules/xpcx/testCxXpcxList";
	}
	
	
	@RequiresPermissions("xpcx:testCxXpcx:view")
	@RequestMapping(value = "form")
	public String form(TestCxXpcx testCxXpcx, Model model) {
		model.addAttribute("testCxXpcx", testCxXpcx);
		return "modules/xpcx/testCxXpcxForm";
	}

	@RequiresPermissions("xpcx:testCxXpcx:edit")
	@RequestMapping(value = "save")
	public String save(TestCxXpcx testCxXpcx, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, testCxXpcx)){
			return form(testCxXpcx, model);
		}
		testCxXpcxService.save(testCxXpcx);
		addMessage(redirectAttributes, "保存小票成功");
		return "redirect:"+Global.getAdminPath()+"/xpcx/testCxXpcx/?repage";
	}
	
	@RequiresPermissions("xpcx:testCxXpcx:edit")
	@RequestMapping(value = "delete")
	public String delete(TestCxXpcx testCxXpcx, RedirectAttributes redirectAttributes) {
		testCxXpcxService.delete(testCxXpcx);
		addMessage(redirectAttributes, "删除小票成功");
		return "redirect:"+Global.getAdminPath()+"/xpcx/testCxXpcx/?repage";
	}

}