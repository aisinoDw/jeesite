package com.thinkgem.jeesite.modules.xpcx.entity;

import java.util.List;

public class Xpxx {
	private static final long serialVersionUID = 1L;
	private String lzxphm;
	private String hykzzd;
	private String nsrsbh;
	private String bz;
	private String hjse;
	private String gflxfs;
	private String bkkpsm;
	private String lzxpdm;
	private String gfsh;
	private String kpjh;
	private String jym;
	private String xfsh;
	private String sbbh;
	private String dkbz;
	private String sky;
	private String gfmc;
	private String xfdzdh;
	private String bkkpje;
	private String xfmc;
	private String xpdm;
	private String hjje;
	private String lsh;
	private String nsrmc;
	private String xphm;
	private String bxbz;
	private String dknsrsbh;
	private String yqjg;
	private String fmrq;
	private String thhyy;
	private String xfyhzh;
	private String xslx;
	private String sign;
	private String y_lsh;
	private String dknsrmc;
	private String sphhs;
	private String jshj;
	private String jylx;
	private String kprq;
	private String jyxplb;
	private String insertTime;
	private List<Jsmx> jslist;
	private List<Xpmx> mxlist;
	public String getLzxphm() {
		return lzxphm;
	}
	public void setLzxphm(String lzxphm) {
		this.lzxphm = lzxphm;
	}
	public String getHykzzd() {
		return hykzzd;
	}
	public void setHykzzd(String hykzzd) {
		this.hykzzd = hykzzd;
	}
	public String getNsrsbh() {
		return nsrsbh;
	}
	public void setNsrsbh(String nsrsbh) {
		this.nsrsbh = nsrsbh;
	}
	public String getBz() {
		return bz;
	}
	public void setBz(String bz) {
		this.bz = bz;
	}
	public String getHjse() {
		return hjse;
	}
	public void setHjse(String hjse) {
		this.hjse = hjse;
	}
	public String getGflxfs() {
		return gflxfs;
	}
	public void setGflxfs(String gflxfs) {
		this.gflxfs = gflxfs;
	}
	public String getBkkpsm() {
		return bkkpsm;
	}
	public void setBkkpsm(String bkkpsm) {
		this.bkkpsm = bkkpsm;
	}
	public String getLzxpdm() {
		return lzxpdm;
	}
	public void setLzxpdm(String lzxpdm) {
		this.lzxpdm = lzxpdm;
	}
	public String getGfsh() {
		return gfsh;
	}
	public void setGfsh(String gfsh) {
		this.gfsh = gfsh;
	}
	public String getKpjh() {
		return kpjh;
	}
	public void setKpjh(String kpjh) {
		this.kpjh = kpjh;
	}
	public String getJym() {
		return jym;
	}
	public void setJym(String jym) {
		this.jym = jym;
	}
	public String getXfsh() {
		return xfsh;
	}
	public void setXfsh(String xfsh) {
		this.xfsh = xfsh;
	}
	public String getSbbh() {
		return sbbh;
	}
	public void setSbbh(String sbbh) {
		this.sbbh = sbbh;
	}
	public String getDkbz() {
		return dkbz;
	}
	public void setDkbz(String dkbz) {
		this.dkbz = dkbz;
	}
	public String getSky() {
		return sky;
	}
	public void setSky(String sky) {
		this.sky = sky;
	}
	public String getGfmc() {
		return gfmc;
	}
	public void setGfmc(String gfmc) {
		this.gfmc = gfmc;
	}
	public String getXfdzdh() {
		return xfdzdh;
	}
	public void setXfdzdh(String xfdzdh) {
		this.xfdzdh = xfdzdh;
	}
	public String getBkkpje() {
		return bkkpje;
	}
	public void setBkkpje(String bkkpje) {
		this.bkkpje = bkkpje;
	}
	public String getXfmc() {
		return xfmc;
	}
	public void setXfmc(String xfmc) {
		this.xfmc = xfmc;
	}
	public String getXpdm() {
		return xpdm;
	}
	public void setXpdm(String xpdm) {
		this.xpdm = xpdm;
	}
	public String getHjje() {
		return hjje;
	}
	public void setHjje(String hjje) {
		this.hjje = hjje;
	}
	public String getLsh() {
		return lsh;
	}
	public void setLsh(String lsh) {
		this.lsh = lsh;
	}
	public String getNsrmc() {
		return nsrmc;
	}
	public void setNsrmc(String nsrmc) {
		this.nsrmc = nsrmc;
	}
	public String getXphm() {
		return xphm;
	}
	public void setXphm(String xphm) {
		this.xphm = xphm;
	}
	public String getBxbz() {
		return bxbz;
	}
	public void setBxbz(String bxbz) {
		this.bxbz = bxbz;
	}
	public String getDknsrsbh() {
		return dknsrsbh;
	}
	public void setDknsrsbh(String dknsrsbh) {
		this.dknsrsbh = dknsrsbh;
	}
	public String getYqjg() {
		return yqjg;
	}
	public void setYqjg(String yqjg) {
		this.yqjg = yqjg;
	}
	public String getFmrq() {
		return fmrq;
	}
	public void setFmrq(String fmrq) {
		this.fmrq = fmrq;
	}
	public String getThhyy() {
		return thhyy;
	}
	public void setThhyy(String thhyy) {
		this.thhyy = thhyy;
	}
	public String getXfyhzh() {
		return xfyhzh;
	}
	public void setXfyhzh(String xfyhzh) {
		this.xfyhzh = xfyhzh;
	}
	public String getXslx() {
		return xslx;
	}
	public void setXslx(String xslx) {
		this.xslx = xslx;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	public String getY_lsh() {
		return y_lsh;
	}
	public void setY_lsh(String y_lsh) {
		this.y_lsh = y_lsh;
	}
	public String getDknsrmc() {
		return dknsrmc;
	}
	public void setDknsrmc(String dknsrmc) {
		this.dknsrmc = dknsrmc;
	}
	public String getSphhs() {
		return sphhs;
	}
	public void setSphhs(String sphhs) {
		this.sphhs = sphhs;
	}
	public String getJshj() {
		return jshj;
	}
	public void setJshj(String jshj) {
		this.jshj = jshj;
	}
	public String getJylx() {
		return jylx;
	}
	public void setJylx(String jylx) {
		this.jylx = jylx;
	}
	public String getKprq() {
		return kprq;
	}
	public void setKprq(String kprq) {
		this.kprq = kprq;
	}
	public String getJyxplb() {
		return jyxplb;
	}
	public void setJyxplb(String jyxplb) {
		this.jyxplb = jyxplb;
	}
	public String getInsertTime() {
		return insertTime;
	}
	public void setInsertTime(String insertTime) {
		this.insertTime = insertTime;
	}
	public List<Jsmx> getJslist() {
		return jslist;
	}
	public void setJslist(List<Jsmx> jslist) {
		this.jslist = jslist;
	}
	public List<Xpmx> getMxlist() {
		return mxlist;
	}
	public void setMxlist(List<Xpmx> mxlist) {
		this.mxlist = mxlist;
	}
	
}
