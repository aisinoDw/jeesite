/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.xpcx.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 1Entity
 * @author 1
 * @version 2016-02-19
 */
public class TestCxXpcx extends DataEntity<TestCxXpcx> {
	
	private static final long serialVersionUID = 1L;
	private String nsrsbh;		// 企业税号
	private String ywlsh;		// 业务流水号
	private String bsqsrq;		// 报税起始日期
	private String bsjzrq;		// 报税截止日期
	private String xpdm;		// 小票代码
	private String xphm;		// 小票号码
	
	public TestCxXpcx() {
		super();
	}

	public TestCxXpcx(String id){
		super(id);
	}

	@Length(min=0, max=255, message="企业税号长度必须介于 0 和 255 之间")
	public String getNsrsbh() {
		return nsrsbh;
	}

	public void setNsrsbh(String nsrsbh) {
		this.nsrsbh = nsrsbh;
	}
	
	@Length(min=0, max=255, message="业务流水号长度必须介于 0 和 255 之间")
	public String getYwlsh() {
		return ywlsh;
	}

	public void setYwlsh(String ywlsh) {
		this.ywlsh = ywlsh;
	}
	
	@Length(min=0, max=255, message="报税起始日期长度必须介于 0 和 255 之间")
	public String getBsqsrq() {
		return bsqsrq;
	}

	public void setBsqsrq(String bsqsrq) {
		this.bsqsrq = bsqsrq;
	}
	
	@Length(min=0, max=255, message="报税截止日期长度必须介于 0 和 255 之间")
	public String getBsjzrq() {
		return bsjzrq;
	}

	public void setBsjzrq(String bsjzrq) {
		this.bsjzrq = bsjzrq;
	}
	
	@Length(min=0, max=255, message="小票代码长度必须介于 0 和 255 之间")
	public String getXpdm() {
		return xpdm;
	}

	public void setXpdm(String xpdm) {
		this.xpdm = xpdm;
	}
	
	@Length(min=0, max=255, message="小票号码长度必须介于 0 和 255 之间")
	public String getXphm() {
		return xphm;
	}

	public void setXphm(String xphm) {
		this.xphm = xphm;
	}
	
}