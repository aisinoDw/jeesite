package com.thinkgem.jeesite.modules.xpcx.entity;

public class Xpmx {
	private String sl;
	private String xphm;
	private String xmkzzd;
	private String jldw;
	private String se;
	private String shul;
	private String xmsx;
	private String je;
	private String mc;
	private String dj;
	private String ggxh;
	private String xmbm;
	private String xpdm;
	private String mxxh;
	public String getSl() {
		return sl;
	}
	public void setSl(String sl) {
		this.sl = sl;
	}
	public String getXphm() {
		return xphm;
	}
	public void setXphm(String xphm) {
		this.xphm = xphm;
	}
	public String getXmkzzd() {
		return xmkzzd;
	}
	public void setXmkzzd(String xmkzzd) {
		this.xmkzzd = xmkzzd;
	}
	public String getJldw() {
		return jldw;
	}
	public void setJldw(String jldw) {
		this.jldw = jldw;
	}
	public String getSe() {
		return se;
	}
	public void setSe(String se) {
		this.se = se;
	}
	public String getShul() {
		return shul;
	}
	public void setShul(String shul) {
		this.shul = shul;
	}
	public String getXmsx() {
		return xmsx;
	}
	public void setXmsx(String xmsx) {
		this.xmsx = xmsx;
	}
	public String getJe() {
		return je;
	}
	public void setJe(String je) {
		this.je = je;
	}
	public String getMc() {
		return mc;
	}
	public void setMc(String mc) {
		this.mc = mc;
	}
	public String getDj() {
		return dj;
	}
	public void setDj(String dj) {
		this.dj = dj;
	}
	public String getGgxh() {
		return ggxh;
	}
	public void setGgxh(String ggxh) {
		this.ggxh = ggxh;
	}
	public String getXmbm() {
		return xmbm;
	}
	public void setXmbm(String xmbm) {
		this.xmbm = xmbm;
	}
	public String getXpdm() {
		return xpdm;
	}
	public void setXpdm(String xpdm) {
		this.xpdm = xpdm;
	}
	public String getMxxh() {
		return mxxh;
	}
	public void setMxxh(String mxxh) {
		this.mxxh = mxxh;
	}
	
}
