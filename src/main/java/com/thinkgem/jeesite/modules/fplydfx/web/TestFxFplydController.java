/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.fplydfx.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.fplydfx.entity.TestFxFplyd;
import com.thinkgem.jeesite.modules.fplydfx.service.TestFxFplydService;

/**
 * 1Controller
 * @author 1
 * @version 2016-01-19
 */
@Controller
@RequestMapping(value = "${adminPath}/fplydfx/testFxFplyd")
public class TestFxFplydController extends BaseController {

	@Autowired
	private TestFxFplydService testFxFplydService;
	
	@ModelAttribute
	public TestFxFplyd get(@RequestParam(required=false) String id) {
		TestFxFplyd entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = testFxFplydService.get(id);
		}
		if (entity == null){
			entity = new TestFxFplyd();
		}
		return entity;
	}
	
	@RequiresPermissions("fplydfx:testFxFplyd:view")
	@RequestMapping(value = {"list", ""})
	public String list(TestFxFplyd testFxFplyd, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<TestFxFplyd> page = testFxFplydService.findPage(new Page<TestFxFplyd>(request, response), testFxFplyd); 
		model.addAttribute("page", page);
		return "modules/fplydfx/testFxFplydList";
	}

	@RequiresPermissions("fplydfx:testFxFplyd:view")
	@RequestMapping(value = "form")
	public String form(TestFxFplyd testFxFplyd, Model model) {
		model.addAttribute("testFxFplyd", testFxFplyd);
		return "modules/fplydfx/testFxFplydForm";
	}

	@RequiresPermissions("fplydfx:testFxFplyd:edit")
	@RequestMapping(value = "save")
	public String save(TestFxFplyd testFxFplyd, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, testFxFplyd)){
			return form(testFxFplyd, model);
		}
		testFxFplydService.save(testFxFplyd);
		addMessage(redirectAttributes, "保存发票成功");
		return "redirect:"+Global.getAdminPath()+"/fplydfx/testFxFplyd/?repage";
	}
	
	@RequiresPermissions("fplydfx:testFxFplyd:edit")
	@RequestMapping(value = "delete")
	public String delete(TestFxFplyd testFxFplyd, RedirectAttributes redirectAttributes) {
		testFxFplydService.delete(testFxFplyd);
		addMessage(redirectAttributes, "删除发票成功");
		return "redirect:"+Global.getAdminPath()+"/fplydfx/testFxFplyd/?repage";
	}

}