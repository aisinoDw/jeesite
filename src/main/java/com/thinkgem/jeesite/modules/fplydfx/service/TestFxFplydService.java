/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.fplydfx.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.fplydfx.entity.TestFxFplyd;
import com.thinkgem.jeesite.modules.fplydfx.dao.TestFxFplydDao;

/**
 * 1Service
 * @author 1
 * @version 2016-01-19
 */
@Service
@Transactional(readOnly = true)
public class TestFxFplydService extends CrudService<TestFxFplydDao, TestFxFplyd> {

	public TestFxFplyd get(String id) {
		return super.get(id);
	}
	
	public List<TestFxFplyd> findList(TestFxFplyd testFxFplyd) {
		return super.findList(testFxFplyd);
	}
	
	public Page<TestFxFplyd> findPage(Page<TestFxFplyd> page, TestFxFplyd testFxFplyd) {
		return super.findPage(page, testFxFplyd);
	}
	
	@Transactional(readOnly = false)
	public void save(TestFxFplyd testFxFplyd) {
		super.save(testFxFplyd);
	}
	
	@Transactional(readOnly = false)
	public void delete(TestFxFplyd testFxFplyd) {
		super.delete(testFxFplyd);
	}
	
}