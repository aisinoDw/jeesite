/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.spjgsj.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 1Entity
 * @author 1
 * @version 2016-01-14
 */
public class TestSpjgSj extends DataEntity<TestSpjgSj> {
	
	private static final long serialVersionUID = 1L;
	private String mc;		// 商品名称
	private String kprq;		// 开票日期
	private String dj;		// 不含税单价
	
	public TestSpjgSj() {
		super();
	}

	public TestSpjgSj(String id){
		super(id);
	}

	@Length(min=1, max=255, message="商品名称长度必须介于 1 和 255 之间")
	public String getMc() {
		return mc;
	}

	public void setMc(String mc) {
		this.mc = mc;
	}
	
	@Length(min=1, max=255, message="开票日期长度必须介于 1 和 255 之间")
	public String getKprq() {
		return kprq;
	}

	public void setKprq(String kprq) {
		this.kprq = kprq;
	}
	
	@Length(min=0, max=255, message="不含税单价长度必须介于 0 和 255 之间")
	public String getDj() {
		return dj;
	}

	public void setDj(String dj) {
		this.dj = dj;
	}
	
}