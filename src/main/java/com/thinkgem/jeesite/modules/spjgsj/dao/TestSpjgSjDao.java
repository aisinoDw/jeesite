/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.spjgsj.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.spjgsj.entity.TestSpjgSj;

/**
 * 1DAO接口
 * @author 1
 * @version 2016-01-14
 */
@MyBatisDao
public interface TestSpjgSjDao extends CrudDao<TestSpjgSj> {
	
}