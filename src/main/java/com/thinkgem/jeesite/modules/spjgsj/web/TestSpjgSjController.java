/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.spjgsj.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.spjgsj.entity.TestSpjgSj;
import com.thinkgem.jeesite.modules.spjgsj.service.TestSpjgSjService;

/**
 * 1Controller
 * @author 1
 * @version 2016-01-14
 */
@Controller
@RequestMapping(value = "${adminPath}/spjgsj/testSpjgSj")
public class TestSpjgSjController extends BaseController {

	@Autowired
	private TestSpjgSjService testSpjgSjService;
	
	@ModelAttribute
	public TestSpjgSj get(@RequestParam(required=false) String id) {
		TestSpjgSj entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = testSpjgSjService.get(id);
		}
		if (entity == null){
			entity = new TestSpjgSj();
		}
		return entity;
	}
	
	@RequiresPermissions("spjgsj:testSpjgSj:view")
	@RequestMapping(value = {"list", ""})
	public String list(TestSpjgSj testSpjgSj, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<TestSpjgSj> page = testSpjgSjService.findPage(new Page<TestSpjgSj>(request, response), testSpjgSj); 
		model.addAttribute("page", page);
		return "modules/spjgsj/testSpjgSjList";
	}

	@RequiresPermissions("spjgsj:testSpjgSj:view")
	@RequestMapping(value = "form")
	public String form(TestSpjgSj testSpjgSj, Model model) {
		model.addAttribute("testSpjgSj", testSpjgSj);
		return "modules/spjgsj/testSpjgSjForm";
	}

	@RequiresPermissions("spjgsj:testSpjgSj:edit")
	@RequestMapping(value = "save")
	public String save(TestSpjgSj testSpjgSj, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, testSpjgSj)){
			return form(testSpjgSj, model);
		}
		testSpjgSjService.save(testSpjgSj);
		addMessage(redirectAttributes, "保存商品成功");
		return "redirect:"+Global.getAdminPath()+"/spjgsj/testSpjgSj/?repage";
	}
	
	@RequiresPermissions("spjgsj:testSpjgSj:edit")
	@RequestMapping(value = "delete")
	public String delete(TestSpjgSj testSpjgSj, RedirectAttributes redirectAttributes) {
		testSpjgSjService.delete(testSpjgSj);
		addMessage(redirectAttributes, "删除商品成功");
		return "redirect:"+Global.getAdminPath()+"/spjgsj/testSpjgSj/?repage";
	}

}