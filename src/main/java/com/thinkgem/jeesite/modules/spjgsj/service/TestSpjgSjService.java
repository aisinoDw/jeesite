/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.spjgsj.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.spjgsj.entity.TestSpjgSj;
import com.thinkgem.jeesite.modules.spjgsj.dao.TestSpjgSjDao;

/**
 * 1Service
 * @author 1
 * @version 2016-01-14
 */
@Service
@Transactional(readOnly = true)
public class TestSpjgSjService extends CrudService<TestSpjgSjDao, TestSpjgSj> {

	public TestSpjgSj get(String id) {
		return super.get(id);
	}
	
	public List<TestSpjgSj> findList(TestSpjgSj testSpjgSj) {
		return super.findList(testSpjgSj);
	}
	
	public Page<TestSpjgSj> findPage(Page<TestSpjgSj> page, TestSpjgSj testSpjgSj) {
		return super.findPage(page, testSpjgSj);
	}
	
	@Transactional(readOnly = false)
	public void save(TestSpjgSj testSpjgSj) {
		super.save(testSpjgSj);
	}
	
	@Transactional(readOnly = false)
	public void delete(TestSpjgSj testSpjgSj) {
		super.delete(testSpjgSj);
	}
	
}