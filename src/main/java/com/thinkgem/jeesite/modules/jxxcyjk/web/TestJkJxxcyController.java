/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.jxxcyjk.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.jxxcyjk.entity.TestJkJxxcy;
import com.thinkgem.jeesite.modules.jxxcyjk.service.TestJkJxxcyService;

/**
 * 1Controller
 * @author 1
 * @version 2016-01-22
 */
@Controller
@RequestMapping(value = "${adminPath}/jxxcyjk/testJkJxxcy")
public class TestJkJxxcyController extends BaseController {

	@Autowired
	private TestJkJxxcyService testJkJxxcyService;
	
	@ModelAttribute
	public TestJkJxxcy get(@RequestParam(required=false) String id) {
		TestJkJxxcy entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = testJkJxxcyService.get(id);
		}
		if (entity == null){
			entity = new TestJkJxxcy();
		}
		return entity;
	}
	
	@RequiresPermissions("jxxcyjk:testJkJxxcy:view")
	@RequestMapping(value = {"list", ""})
	public String list(TestJkJxxcy testJkJxxcy, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<TestJkJxxcy> page = testJkJxxcyService.findPage(new Page<TestJkJxxcy>(request, response), testJkJxxcy); 
		model.addAttribute("page", page);
		return "modules/jxxcyjk/testJkJxxcyList";
	}

	@RequiresPermissions("jxxcyjk:testJkJxxcy:view")
	@RequestMapping(value = "form")
	public String form(TestJkJxxcy testJkJxxcy, Model model) {
		model.addAttribute("testJkJxxcy", testJkJxxcy);
		return "modules/jxxcyjk/testJkJxxcyForm";
	}

	@RequiresPermissions("jxxcyjk:testJkJxxcy:edit")
	@RequestMapping(value = "save")
	public String save(TestJkJxxcy testJkJxxcy, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, testJkJxxcy)){
			return form(testJkJxxcy, model);
		}
		testJkJxxcyService.save(testJkJxxcy);
		addMessage(redirectAttributes, "保存小票成功");
		return "redirect:"+Global.getAdminPath()+"/jxxcyjk/testJkJxxcy/?repage";
	}
	
	@RequiresPermissions("jxxcyjk:testJkJxxcy:edit")
	@RequestMapping(value = "delete")
	public String delete(TestJkJxxcy testJkJxxcy, RedirectAttributes redirectAttributes) {
		testJkJxxcyService.delete(testJkJxxcy);
		addMessage(redirectAttributes, "删除小票成功");
		return "redirect:"+Global.getAdminPath()+"/jxxcyjk/testJkJxxcy/?repage";
	}

}