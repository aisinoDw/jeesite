/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.jxxcyjk.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.jxxcyjk.entity.TestJkJxxcy;
import com.thinkgem.jeesite.modules.jxxcyjk.dao.TestJkJxxcyDao;

/**
 * 1Service
 * @author 1
 * @version 2016-01-22
 */
@Service
@Transactional(readOnly = true)
public class TestJkJxxcyService extends CrudService<TestJkJxxcyDao, TestJkJxxcy> {

	public TestJkJxxcy get(String id) {
		return super.get(id);
	}
	
	public List<TestJkJxxcy> findList(TestJkJxxcy testJkJxxcy) {
		return super.findList(testJkJxxcy);
	}
	
	public Page<TestJkJxxcy> findPage(Page<TestJkJxxcy> page, TestJkJxxcy testJkJxxcy) {
		return super.findPage(page, testJkJxxcy);
	}
	
	@Transactional(readOnly = false)
	public void save(TestJkJxxcy testJkJxxcy) {
		super.save(testJkJxxcy);
	}
	
	@Transactional(readOnly = false)
	public void delete(TestJkJxxcy testJkJxxcy) {
		super.delete(testJkJxxcy);
	}
	
}