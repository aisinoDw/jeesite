/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.jxxcyjk.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 1Entity
 * @author 1
 * @version 2016-01-22
 */
public class TestJkJxxcy extends DataEntity<TestJkJxxcy> {
	
	private static final long serialVersionUID = 1L;
	private String xpdm;		// 小票代码
	private String xphm;		// 小票号码
	private String kprq;		// 开票日期
	private String dj;		// 单价
	private String hjje;		// 合计金额
	private String hjse;		// 合计税额
	private String gfsh;		// 购方税号
	private String gfmc;		// 购方名称
	private String gflxfs;		// 购方联系方式
	private String xfsh;		// 销方税号
	private String xfmc;		// 销方名称
	private String xflxfs;		// 销方联系方式
	private String xfyhzh;		// 销货方银行账号
	private String cyz;		// 差异值
	private String cydj;		// 差异等级
	private String cymx;		// 差异明细
	
	public TestJkJxxcy() {
		super();
	}

	public TestJkJxxcy(String id){
		super(id);
	}

	@Length(min=1, max=255, message="小票代码长度必须介于 1 和 255 之间")
	public String getXpdm() {
		return xpdm;
	}

	public void setXpdm(String xpdm) {
		this.xpdm = xpdm;
	}
	
	@Length(min=1, max=255, message="小票号码长度必须介于 1 和 255 之间")
	public String getXphm() {
		return xphm;
	}

	public void setXphm(String xphm) {
		this.xphm = xphm;
	}
	
	@Length(min=0, max=255, message="开票日期长度必须介于 0 和 255 之间")
	public String getKprq() {
		return kprq;
	}

	public void setKprq(String kprq) {
		this.kprq = kprq;
	}
	
	@Length(min=0, max=255, message="单价长度必须介于 0 和 255 之间")
	public String getDj() {
		return dj;
	}

	public void setDj(String dj) {
		this.dj = dj;
	}
	
	@Length(min=0, max=255, message="合计金额长度必须介于 0 和 255 之间")
	public String getHjje() {
		return hjje;
	}

	public void setHjje(String hjje) {
		this.hjje = hjje;
	}
	
	@Length(min=0, max=255, message="合计税额长度必须介于 0 和 255 之间")
	public String getHjse() {
		return hjse;
	}

	public void setHjse(String hjse) {
		this.hjse = hjse;
	}
	
	@Length(min=0, max=255, message="购方税号长度必须介于 0 和 255 之间")
	public String getGfsh() {
		return gfsh;
	}

	public void setGfsh(String gfsh) {
		this.gfsh = gfsh;
	}
	
	@Length(min=0, max=255, message="购方名称长度必须介于 0 和 255 之间")
	public String getGfmc() {
		return gfmc;
	}

	public void setGfmc(String gfmc) {
		this.gfmc = gfmc;
	}
	
	@Length(min=0, max=255, message="购方联系方式长度必须介于 0 和 255 之间")
	public String getGflxfs() {
		return gflxfs;
	}

	public void setGflxfs(String gflxfs) {
		this.gflxfs = gflxfs;
	}
	
	@Length(min=0, max=255, message="销方税号长度必须介于 0 和 255 之间")
	public String getXfsh() {
		return xfsh;
	}

	public void setXfsh(String xfsh) {
		this.xfsh = xfsh;
	}
	
	@Length(min=0, max=255, message="销方名称长度必须介于 0 和 255 之间")
	public String getXfmc() {
		return xfmc;
	}

	public void setXfmc(String xfmc) {
		this.xfmc = xfmc;
	}
	
	@Length(min=0, max=255, message="销方联系方式长度必须介于 0 和 255 之间")
	public String getXflxfs() {
		return xflxfs;
	}

	public void setXflxfs(String xflxfs) {
		this.xflxfs = xflxfs;
	}
	
	@Length(min=0, max=255, message="销货方银行账号长度必须介于 0 和 255 之间")
	public String getXfyhzh() {
		return xfyhzh;
	}

	public void setXfyhzh(String xfyhzh) {
		this.xfyhzh = xfyhzh;
	}
	
	@Length(min=0, max=255, message="差异值长度必须介于 0 和 255 之间")
	public String getCyz() {
		return cyz;
	}

	public void setCyz(String cyz) {
		this.cyz = cyz;
	}
	
	@Length(min=0, max=255, message="差异等级长度必须介于 0 和 255 之间")
	public String getCydj() {
		return cydj;
	}

	public void setCydj(String cydj) {
		this.cydj = cydj;
	}
	
	@Length(min=0, max=255, message="差异明细长度必须介于 0 和 255 之间")
	public String getCymx() {
		return cymx;
	}

	public void setCymx(String cymx) {
		this.cymx = cymx;
	}
	
}