/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.swjggl.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.swjggl.entity.TestGlSwjg;
import com.thinkgem.jeesite.modules.swjggl.dao.TestGlSwjgDao;

/**
 * 1Service
 * @author 1
 * @version 2016-01-19
 */
@Service
@Transactional(readOnly = true)
public class TestGlSwjgService extends CrudService<TestGlSwjgDao, TestGlSwjg> {

	public TestGlSwjg get(String id) {
		return super.get(id);
	}
	
	public List<TestGlSwjg> findList(TestGlSwjg testGlSwjg) {
		return super.findList(testGlSwjg);
	}
	
	public Page<TestGlSwjg> findPage(Page<TestGlSwjg> page, TestGlSwjg testGlSwjg) {
		return super.findPage(page, testGlSwjg);
	}
	
	@Transactional(readOnly = false)
	public void save(TestGlSwjg testGlSwjg) {
		super.save(testGlSwjg);
	}
	
	@Transactional(readOnly = false)
	public void delete(TestGlSwjg testGlSwjg) {
		super.delete(testGlSwjg);
	}
	
}