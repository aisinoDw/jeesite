/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.swjggl.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.swjggl.entity.TestGlSwjg;
import com.thinkgem.jeesite.modules.swjggl.service.TestGlSwjgService;

/**
 * 1Controller
 * @author 1
 * @version 2016-01-19
 */
@Controller
@RequestMapping(value = "${adminPath}/swjggl/testGlSwjg")
public class TestGlSwjgController extends BaseController {

	@Autowired
	private TestGlSwjgService testGlSwjgService;
	
	@ModelAttribute
	public TestGlSwjg get(@RequestParam(required=false) String id) {
		TestGlSwjg entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = testGlSwjgService.get(id);
		}
		if (entity == null){
			entity = new TestGlSwjg();
		}
		return entity;
	}
	
	@RequiresPermissions("swjggl:testGlSwjg:view")
	@RequestMapping(value = {"list", ""})
	public String list(TestGlSwjg testGlSwjg, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<TestGlSwjg> page = testGlSwjgService.findPage(new Page<TestGlSwjg>(request, response), testGlSwjg); 
		model.addAttribute("page", page);
		return "modules/swjggl/testGlSwjgList";
	}

	@RequiresPermissions("swjggl:testGlSwjg:view")
	@RequestMapping(value = "form")
	public String form(TestGlSwjg testGlSwjg, Model model) {
		model.addAttribute("testGlSwjg", testGlSwjg);
		return "modules/swjggl/testGlSwjgForm";
	}

	@RequiresPermissions("swjggl:testGlSwjg:edit")
	@RequestMapping(value = "save")
	public String save(TestGlSwjg testGlSwjg, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, testGlSwjg)){
			return form(testGlSwjg, model);
		}
		testGlSwjgService.save(testGlSwjg);
		addMessage(redirectAttributes, "保存税务机关成功");
		return "redirect:"+Global.getAdminPath()+"/swjggl/testGlSwjg/?repage";
	}
	
	@RequiresPermissions("swjggl:testGlSwjg:edit")
	@RequestMapping(value = "delete")
	public String delete(TestGlSwjg testGlSwjg, RedirectAttributes redirectAttributes) {
		testGlSwjgService.delete(testGlSwjg);
		addMessage(redirectAttributes, "删除税务机关成功");
		return "redirect:"+Global.getAdminPath()+"/swjggl/testGlSwjg/?repage";
	}

}