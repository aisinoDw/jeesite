/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.swjggl.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 1Entity
 * @author 1
 * @version 2016-01-19
 */
public class TestGlSwjg extends DataEntity<TestGlSwjg> {
	
	private static final long serialVersionUID = 1L;
	private String swjgdm;		// 税务机关代码
	private String swjgmc;		// 税务机关名称
	private String dqbh;		// 地区编号
	private String dqmc;		// 地区名称
	
	public TestGlSwjg() {
		super();
	}

	public TestGlSwjg(String id){
		super(id);
	}

	@Length(min=1, max=255, message="税务机关代码长度必须介于 1 和 255 之间")
	public String getSwjgdm() {
		return swjgdm;
	}

	public void setSwjgdm(String swjgdm) {
		this.swjgdm = swjgdm;
	}
	
	@Length(min=0, max=255, message="税务机关名称长度必须介于 0 和 255 之间")
	public String getSwjgmc() {
		return swjgmc;
	}

	public void setSwjgmc(String swjgmc) {
		this.swjgmc = swjgmc;
	}
	
	@Length(min=0, max=255, message="地区编号长度必须介于 0 和 255 之间")
	public String getDqbh() {
		return dqbh;
	}

	public void setDqbh(String dqbh) {
		this.dqbh = dqbh;
	}
	
	@Length(min=0, max=255, message="地区名称长度必须介于 0 和 255 之间")
	public String getDqmc() {
		return dqmc;
	}

	public void setDqmc(String dqmc) {
		this.dqmc = dqmc;
	}
	
}