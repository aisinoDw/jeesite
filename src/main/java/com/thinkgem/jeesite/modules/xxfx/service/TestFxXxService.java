/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.xxfx.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.xxfx.entity.TestFxXx;
import com.thinkgem.jeesite.modules.xxfx.dao.TestFxXxDao;

/**
 * 1Service
 * @author 1
 * @version 2016-01-19
 */
@Service
@Transactional(readOnly = true)
public class TestFxXxService extends CrudService<TestFxXxDao, TestFxXx> {

	public TestFxXx get(String id) {
		return super.get(id);
	}
	
	public List<TestFxXx> findList(TestFxXx testFxXx) {
		return super.findList(testFxXx);
	}
	
	public Page<TestFxXx> findPage(Page<TestFxXx> page, TestFxXx testFxXx) {
		return super.findPage(page, testFxXx);
	}
	
	@Transactional(readOnly = false)
	public void save(TestFxXx testFxXx) {
		super.save(testFxXx);
	}
	
	@Transactional(readOnly = false)
	public void delete(TestFxXx testFxXx) {
		super.delete(testFxXx);
	}
	
}