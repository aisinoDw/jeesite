/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.xxfx.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.xxfx.entity.TestFxXx;
import com.thinkgem.jeesite.modules.xxfx.service.TestFxXxService;

/**
 * 1Controller
 * @author 1
 * @version 2016-01-19
 */
@Controller
@RequestMapping(value = "${adminPath}/xxfx/testFxXx")
public class TestFxXxController extends BaseController {

	@Autowired
	private TestFxXxService testFxXxService;
	
	@ModelAttribute
	public TestFxXx get(@RequestParam(required=false) String id) {
		TestFxXx entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = testFxXxService.get(id);
		}
		if (entity == null){
			entity = new TestFxXx();
		}
		return entity;
	}
	
	@RequiresPermissions("xxfx:testFxXx:view")
	@RequestMapping(value = {"list", ""})
	public String list(TestFxXx testFxXx, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<TestFxXx> page = testFxXxService.findPage(new Page<TestFxXx>(request, response), testFxXx); 
		model.addAttribute("page", page);
		return "modules/xxfx/testFxXxList";
	}

	@RequiresPermissions("xxfx:testFxXx:view")
	@RequestMapping(value = "form")
	public String form(TestFxXx testFxXx, Model model) {
		model.addAttribute("testFxXx", testFxXx);
		return "modules/xxfx/testFxXxForm";
	}

	@RequiresPermissions("xxfx:testFxXx:edit")
	@RequestMapping(value = "save")
	public String save(TestFxXx testFxXx, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, testFxXx)){
			return form(testFxXx, model);
		}
		testFxXxService.save(testFxXx);
		addMessage(redirectAttributes, "保存销方成功");
		return "redirect:"+Global.getAdminPath()+"/xxfx/testFxXx/?repage";
	}
	
	@RequiresPermissions("xxfx:testFxXx:edit")
	@RequestMapping(value = "delete")
	public String delete(TestFxXx testFxXx, RedirectAttributes redirectAttributes) {
		testFxXxService.delete(testFxXx);
		addMessage(redirectAttributes, "删除销方成功");
		return "redirect:"+Global.getAdminPath()+"/xxfx/testFxXx/?repage";
	}

}