/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.spjgdq.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 1Entity
 * @author 1
 * @version 2016-01-14
 */
public class TestSpjgDq extends DataEntity<TestSpjgDq> {
	
	private static final long serialVersionUID = 1L;
	private String mc;		// 商品名称
	private String swjgdm;		// 税务机关代码
	private String swjgmc;		// 税务机关名称
	private String dj;		// 不含税单价
	
	public TestSpjgDq() {
		super();
	}

	public TestSpjgDq(String id){
		super(id);
	}

	@Length(min=1, max=255, message="商品名称长度必须介于 1 和 255 之间")
	public String getMc() {
		return mc;
	}

	public void setMc(String mc) {
		this.mc = mc;
	}
	
	@Length(min=1, max=255, message="税务机关代码长度必须介于 1 和 255 之间")
	public String getSwjgdm() {
		return swjgdm;
	}

	public void setSwjgdm(String swjgdm) {
		this.swjgdm = swjgdm;
	}
	
	@Length(min=0, max=255, message="税务机关名称长度必须介于 0 和 255 之间")
	public String getSwjgmc() {
		return swjgmc;
	}

	public void setSwjgmc(String swjgmc) {
		this.swjgmc = swjgmc;
	}
	
	@Length(min=0, max=255, message="不含税单价长度必须介于 0 和 255 之间")
	public String getDj() {
		return dj;
	}

	public void setDj(String dj) {
		this.dj = dj;
	}
	
}