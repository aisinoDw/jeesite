/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.spjgdq.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.spjgdq.entity.TestSpjgDq;
import com.thinkgem.jeesite.modules.spjgdq.service.TestSpjgDqService;

/**
 * 1Controller
 * @author 1
 * @version 2016-01-14
 */
@Controller
@RequestMapping(value = "${adminPath}/spjgdq/testSpjgDq")
public class TestSpjgDqController extends BaseController {

	@Autowired
	private TestSpjgDqService testSpjgDqService;
	
	@ModelAttribute
	public TestSpjgDq get(@RequestParam(required=false) String id) {
		TestSpjgDq entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = testSpjgDqService.get(id);
		}
		if (entity == null){
			entity = new TestSpjgDq();
		}
		return entity;
	}
	
	@RequiresPermissions("spjgdq:testSpjgDq:view")
	@RequestMapping(value = {"list", ""})
	public String list(TestSpjgDq testSpjgDq, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<TestSpjgDq> page = testSpjgDqService.findPage(new Page<TestSpjgDq>(request, response), testSpjgDq); 
		model.addAttribute("page", page);
		return "modules/spjgdq/testSpjgDqList";
	}

	@RequiresPermissions("spjgdq:testSpjgDq:view")
	@RequestMapping(value = "form")
	public String form(TestSpjgDq testSpjgDq, Model model) {
		model.addAttribute("testSpjgDq", testSpjgDq);
		return "modules/spjgdq/testSpjgDqForm";
	}

	@RequiresPermissions("spjgdq:testSpjgDq:edit")
	@RequestMapping(value = "save")
	public String save(TestSpjgDq testSpjgDq, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, testSpjgDq)){
			return form(testSpjgDq, model);
		}
		testSpjgDqService.save(testSpjgDq);
		addMessage(redirectAttributes, "保存商品成功");
		return "redirect:"+Global.getAdminPath()+"/spjgdq/testSpjgDq/?repage";
	}
	
	@RequiresPermissions("spjgdq:testSpjgDq:edit")
	@RequestMapping(value = "delete")
	public String delete(TestSpjgDq testSpjgDq, RedirectAttributes redirectAttributes) {
		testSpjgDqService.delete(testSpjgDq);
		addMessage(redirectAttributes, "删除商品成功");
		return "redirect:"+Global.getAdminPath()+"/spjgdq/testSpjgDq/?repage";
	}

}