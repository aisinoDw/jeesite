/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.spjgdq.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.spjgdq.entity.TestSpjgDq;
import com.thinkgem.jeesite.modules.spjgdq.dao.TestSpjgDqDao;

/**
 * 1Service
 * @author 1
 * @version 2016-01-14
 */
@Service
@Transactional(readOnly = true)
public class TestSpjgDqService extends CrudService<TestSpjgDqDao, TestSpjgDq> {

	public TestSpjgDq get(String id) {
		return super.get(id);
	}
	
	public List<TestSpjgDq> findList(TestSpjgDq testSpjgDq) {
		return super.findList(testSpjgDq);
	}
	
	public Page<TestSpjgDq> findPage(Page<TestSpjgDq> page, TestSpjgDq testSpjgDq) {
		return super.findPage(page, testSpjgDq);
	}
	
	@Transactional(readOnly = false)
	public void save(TestSpjgDq testSpjgDq) {
		super.save(testSpjgDq);
	}
	
	@Transactional(readOnly = false)
	public void delete(TestSpjgDq testSpjgDq) {
		super.delete(testSpjgDq);
	}
	
}