/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.jxfx.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.jxfx.entity.TestFxJx;
import com.thinkgem.jeesite.modules.jxfx.service.TestFxJxService;

/**
 * 1Controller
 * @author 1
 * @version 2016-01-19
 */
@Controller
@RequestMapping(value = "${adminPath}/jxfx/testFxJx")
public class TestFxJxController extends BaseController {

	@Autowired
	private TestFxJxService testFxJxService;
	
	@ModelAttribute
	public TestFxJx get(@RequestParam(required=false) String id) {
		TestFxJx entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = testFxJxService.get(id);
		}
		if (entity == null){
			entity = new TestFxJx();
		}
		return entity;
	}
	
	@RequiresPermissions("jxfx:testFxJx:view")
	@RequestMapping(value = {"list", ""})
	public String list(TestFxJx testFxJx, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<TestFxJx> page = testFxJxService.findPage(new Page<TestFxJx>(request, response), testFxJx); 
		model.addAttribute("page", page);
		return "modules/jxfx/testFxJxList";
	}

	@RequiresPermissions("jxfx:testFxJx:view")
	@RequestMapping(value = "form")
	public String form(TestFxJx testFxJx, Model model) {
		model.addAttribute("testFxJx", testFxJx);
		return "modules/jxfx/testFxJxForm";
	}

	@RequiresPermissions("jxfx:testFxJx:edit")
	@RequestMapping(value = "save")
	public String save(TestFxJx testFxJx, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, testFxJx)){
			return form(testFxJx, model);
		}
		testFxJxService.save(testFxJx);
		addMessage(redirectAttributes, "保存购方成功");
		return "redirect:"+Global.getAdminPath()+"/jxfx/testFxJx/?repage";
	}
	
	@RequiresPermissions("jxfx:testFxJx:edit")
	@RequestMapping(value = "delete")
	public String delete(TestFxJx testFxJx, RedirectAttributes redirectAttributes) {
		testFxJxService.delete(testFxJx);
		addMessage(redirectAttributes, "删除购方成功");
		return "redirect:"+Global.getAdminPath()+"/jxfx/testFxJx/?repage";
	}

}