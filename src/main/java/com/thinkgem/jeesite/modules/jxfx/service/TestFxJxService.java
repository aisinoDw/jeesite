/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.jxfx.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.jxfx.entity.TestFxJx;
import com.thinkgem.jeesite.modules.jxfx.dao.TestFxJxDao;

/**
 * 1Service
 * @author 1
 * @version 2016-01-19
 */
@Service
@Transactional(readOnly = true)
public class TestFxJxService extends CrudService<TestFxJxDao, TestFxJx> {

	public TestFxJx get(String id) {
		return super.get(id);
	}
	
	public List<TestFxJx> findList(TestFxJx testFxJx) {
		return super.findList(testFxJx);
	}
	
	public Page<TestFxJx> findPage(Page<TestFxJx> page, TestFxJx testFxJx) {
		return super.findPage(page, testFxJx);
	}
	
	@Transactional(readOnly = false)
	public void save(TestFxJx testFxJx) {
		super.save(testFxJx);
	}
	
	@Transactional(readOnly = false)
	public void delete(TestFxJx testFxJx) {
		super.delete(testFxJx);
	}
	
}