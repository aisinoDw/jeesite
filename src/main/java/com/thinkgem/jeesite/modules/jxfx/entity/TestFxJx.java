/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.jxfx.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 1Entity
 * @author 1
 * @version 2016-01-19
 */
public class TestFxJx extends DataEntity<TestFxJx> {
	
	private static final long serialVersionUID = 1L;
	private String gfsh;		// 购方税号
	private String gfmc;		// 购方名称
	private String gflxfs;		// 购方联系方式
	private String hjje;		// 合计金额
	private String hjse;		// 合计税额
	
	public TestFxJx() {
		super();
	}

	public TestFxJx(String id){
		super(id);
	}

	@Length(min=1, max=255, message="购方税号长度必须介于 1 和 255 之间")
	public String getGfsh() {
		return gfsh;
	}

	public void setGfsh(String gfsh) {
		this.gfsh = gfsh;
	}
	
	@Length(min=0, max=255, message="购方名称长度必须介于 0 和 255 之间")
	public String getGfmc() {
		return gfmc;
	}

	public void setGfmc(String gfmc) {
		this.gfmc = gfmc;
	}
	
	@Length(min=0, max=255, message="购方联系方式长度必须介于 0 和 255 之间")
	public String getGflxfs() {
		return gflxfs;
	}

	public void setGflxfs(String gflxfs) {
		this.gflxfs = gflxfs;
	}
	
	@Length(min=0, max=255, message="合计金额长度必须介于 0 和 255 之间")
	public String getHjje() {
		return hjje;
	}

	public void setHjje(String hjje) {
		this.hjje = hjje;
	}
	
	@Length(min=0, max=255, message="合计税额长度必须介于 0 和 255 之间")
	public String getHjse() {
		return hjse;
	}

	public void setHjse(String hjse) {
		this.hjse = hjse;
	}
	
}