/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.xptj.web;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.impl.util.json.JSONObject;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.modules.xptj.entity.TestCxXptj;
import com.thinkgem.jeesite.modules.xptj.entity.Xptj;
import com.thinkgem.jeesite.modules.xptj.service.TestCxXptjService;

/**
 * 1Controller
 * @author 1
 * @version 2016-02-25
 */
@Controller
@RequestMapping(value = "${adminPath}/xptj/testCxXptj")
public class TestCxXptjController extends BaseController {

	@Autowired
	private TestCxXptjService testCxXptjService;
	
	@ModelAttribute
	public TestCxXptj get(@RequestParam(required=false) String id) {
		TestCxXptj entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = testCxXptjService.get(id);
		}
		if (entity == null){
			entity = new TestCxXptj();
		}
		return entity;
	}
	
	@RequiresPermissions("xptj:testCxXptj:view")
	@RequestMapping(value = {"list", ""})
	public String list(TestCxXptj testCxXptj, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<TestCxXptj> page = testCxXptjService.findPage(new Page<TestCxXptj>(request, response), testCxXptj); 
		model.addAttribute("page", page);
		return "modules/xptj/testCxXptjList";
	}

	
	
	
	
	@RequiresPermissions("xptj:testCxXptj:view")
	@RequestMapping(value = {"xptjjson"})
	public String xptjjson(TestCxXptj testCxXptj, HttpServletRequest request, HttpServletResponse response, Model model)throws Exception {
		String url = "http://192.168.2.176:8889/DSCSClient";
		HttpPost httpPost = new HttpPost(url);
		List<NameValuePair> nvps1 = new ArrayList<NameValuePair>();
		List<NameValuePair> nvps2 = new ArrayList<NameValuePair>();
		String cmd = "sumOfQuantity";//数量统计
		String cmd2 = "sumOfAmount";//数量统计
		nvps1.add(new BasicNameValuePair("cmd", cmd)); 
		nvps2.add(new BasicNameValuePair("cmd", cmd2)); 
		String nsrsbh = testCxXptj.getNsrsbh();
		String kpjh = testCxXptj.getKpjh();
		String kprq_1 = testCxXptj.getBsqsrq();//6位yyyyMM
		String kprq_2 = testCxXptj.getBsjzrq();//6位yyyyMM
		String PNFlag = testCxXptj.getZfp();//0或1  建议0  1查不出来
		String keys = "";
		if(StringUtils.isNotBlank(nsrsbh)){
			if(keys.length()>0){
				keys += ",";
			}
			keys += "\"nsrsbh\":\""+nsrsbh+"\"";
		}
		if(StringUtils.isNotBlank(kpjh)){
			if(keys.length()>0){
				keys += ",";
			}
			keys += "\"kpjh\":\""+kpjh+"\"";
		}
		if(StringUtils.isNotBlank(kprq_1)){
			if(keys.length()>0){
				keys += ",";
			}
			keys += "\"KPRQ_1\":\""+kprq_1+"\"";
		}
		if(StringUtils.isNotBlank(kprq_2)){
			if(keys.length()>0){
				keys += ",";
			}
			keys += "\"KPRQ_2\":\""+kprq_2+"\"";
		}
		if(StringUtils.isNotBlank(PNFlag)){
			if(keys.length()>0){
				keys += ",";
			}
			keys += "\"PNFlag\":"+PNFlag+"";
		}
		
		nvps1.add(new BasicNameValuePair("arg", "{"+keys+"}")); 
		nvps2.add(new BasicNameValuePair("arg", "{"+keys+"}")); 
		httpPost.setEntity(new UrlEncodedFormEntity(nvps1));
		HttpClient httpclient = new DefaultHttpClient();	
		HttpResponse resp= httpclient.execute(httpPost);
		HttpEntity entity = resp.getEntity();
		String jsonresp = EntityUtils.toString(entity);
		logger.info("jsonresp====="+jsonresp);
		JSONObject json = new JSONObject(jsonresp);
		Xptj xptj = new Xptj();
		if(json.getInt("CompleteCode")==0){
			String slzj = json.getInt("Data")+"";
			xptj.setSlzj(slzj);
		}else{
			logger.info(json.toString());
			throw new Exception("接口调用失败!");
			
		}
		
		HttpPost httpPost2 = new HttpPost(url);
		httpPost2.setEntity(new UrlEncodedFormEntity(nvps2));
		HttpClient httpclient2 = new DefaultHttpClient();	
		HttpResponse resp2= httpclient2.execute(httpPost2);
		HttpEntity entity2 = resp2.getEntity();
		String jsonresp2 = EntityUtils.toString(entity2);
		logger.info("jsonresp2====="+jsonresp2);
		JSONObject json2 = new JSONObject(jsonresp2);
		
		if(json2.getInt("CompleteCode")==0){
			JSONObject o = json2.getJSONObject("Data");
			String hjje = o.getString("hjje");
			String hjse = o.getDouble("hjse")+"";
			xptj.setHjje(hjje);
			xptj.setHjse(hjse);
		}else{
			logger.info(json2.toString());
			throw new Exception("接口调用失败!");
			
		}
		List<Xptj> xptjList = new ArrayList<Xptj>();
		xptjList.add(xptj);
		model.addAttribute("page1", xptjList);
		return "modules/xptj/testCxXptjList";
	}
	
	
	
	
	
	
	
	
	
	
	
	@RequiresPermissions("xptj:testCxXptj:view")
	@RequestMapping(value = "form")
	public String form(TestCxXptj testCxXptj, Model model) {
		model.addAttribute("testCxXptj", testCxXptj);
		return "modules/xptj/testCxXptjForm";
	}

	@RequiresPermissions("xptj:testCxXptj:edit")
	@RequestMapping(value = "save")
	public String save(TestCxXptj testCxXptj, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, testCxXptj)){
			return form(testCxXptj, model);
		}
		testCxXptjService.save(testCxXptj);
		addMessage(redirectAttributes, "保存小票成功");
		return "redirect:"+Global.getAdminPath()+"/xptj/testCxXptj/?repage";
	}
	
	@RequiresPermissions("xptj:testCxXptj:edit")
	@RequestMapping(value = "delete")
	public String delete(TestCxXptj testCxXptj, RedirectAttributes redirectAttributes) {
		testCxXptjService.delete(testCxXptj);
		addMessage(redirectAttributes, "删除小票成功");
		return "redirect:"+Global.getAdminPath()+"/xptj/testCxXptj/?repage";
	}

}