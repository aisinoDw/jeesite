/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.xptj.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.xptj.entity.TestCxXptj;
import com.thinkgem.jeesite.modules.xptj.dao.TestCxXptjDao;

/**
 * 1Service
 * @author 1
 * @version 2016-02-25
 */
@Service
@Transactional(readOnly = true)
public class TestCxXptjService extends CrudService<TestCxXptjDao, TestCxXptj> {

	public TestCxXptj get(String id) {
		return super.get(id);
	}
	
	public List<TestCxXptj> findList(TestCxXptj testCxXptj) {
		return super.findList(testCxXptj);
	}
	
	public Page<TestCxXptj> findPage(Page<TestCxXptj> page, TestCxXptj testCxXptj) {
		return super.findPage(page, testCxXptj);
	}
	
	@Transactional(readOnly = false)
	public void save(TestCxXptj testCxXptj) {
		super.save(testCxXptj);
	}
	
	@Transactional(readOnly = false)
	public void delete(TestCxXptj testCxXptj) {
		super.delete(testCxXptj);
	}
	
}