/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.xptj.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 1Entity
 * @author 1
 * @version 2016-02-25
 */
public class TestCxXptj extends DataEntity<TestCxXptj> {
	
	private static final long serialVersionUID = 1L;
	private String nsrsbh;		// 企业税号
	private String kpjh;		// 开票机号
	private String bsqsrq;		// 报税起始日期
	private String bsjzrq;		// 报税截止日期
	private String zfp;		// 正负票
	
	public TestCxXptj() {
		super();
	}

	public TestCxXptj(String id){
		super(id);
	}

	@Length(min=0, max=255, message="企业税号长度必须介于 0 和 255 之间")
	public String getNsrsbh() {
		return nsrsbh;
	}

	public void setNsrsbh(String nsrsbh) {
		this.nsrsbh = nsrsbh;
	}
	
	@Length(min=0, max=255, message="开票机号长度必须介于 0 和 255 之间")
	public String getKpjh() {
		return kpjh;
	}

	public void setKpjh(String kpjh) {
		this.kpjh = kpjh;
	}
	
	@Length(min=0, max=255, message="报税起始日期长度必须介于 0 和 255 之间")
	public String getBsqsrq() {
		return bsqsrq;
	}

	public void setBsqsrq(String bsqsrq) {
		this.bsqsrq = bsqsrq;
	}
	
	@Length(min=0, max=255, message="报税截止日期长度必须介于 0 和 255 之间")
	public String getBsjzrq() {
		return bsjzrq;
	}

	public void setBsjzrq(String bsjzrq) {
		this.bsjzrq = bsjzrq;
	}
	
	@Length(min=0, max=255, message="正负票长度必须介于 0 和 255 之间")
	public String getZfp() {
		return zfp;
	}

	public void setZfp(String zfp) {
		this.zfp = zfp;
	}
	
}