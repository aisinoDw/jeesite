/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.xptj.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.xptj.entity.TestCxXptj;

/**
 * 1DAO接口
 * @author 1
 * @version 2016-02-25
 */
@MyBatisDao
public interface TestCxXptjDao extends CrudDao<TestCxXptj> {
	
}