package com.thinkgem.jeesite.modules.xptj.entity;

public class Xptj {
	private String nsrsbh;
	private String hjje;
	private String hjse;
	private String slzj;
	public String getNsrsbh() {
		return nsrsbh;
	}
	public void setNsrsbh(String nsrsbh) {
		this.nsrsbh = nsrsbh;
	}
	public String getHjje() {
		return hjje;
	}
	public void setHjje(String hjje) {
		this.hjje = hjje;
	}
	public String getHjse() {
		return hjse;
	}
	public void setHjse(String hjse) {
		this.hjse = hjse;
	}
	public String getSlzj() {
		return slzj;
	}
	public void setSlzj(String slzj) {
		this.slzj = slzj;
	}
	
	
}
