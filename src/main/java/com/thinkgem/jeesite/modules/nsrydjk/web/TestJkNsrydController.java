/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.nsrydjk.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.nsrydjk.entity.TestJkNsryd;
import com.thinkgem.jeesite.modules.nsrydjk.service.TestJkNsrydService;

/**
 * 1Controller
 * @author 1
 * @version 2016-01-19
 */
@Controller
@RequestMapping(value = "${adminPath}/nsrydjk/testJkNsryd")
public class TestJkNsrydController extends BaseController {

	@Autowired
	private TestJkNsrydService testJkNsrydService;
	
	@ModelAttribute
	public TestJkNsryd get(@RequestParam(required=false) String id) {
		TestJkNsryd entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = testJkNsrydService.get(id);
		}
		if (entity == null){
			entity = new TestJkNsryd();
		}
		return entity;
	}
	
	@RequiresPermissions("nsrydjk:testJkNsryd:view")
	@RequestMapping(value = {"list", ""})
	public String list(TestJkNsryd testJkNsryd, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<TestJkNsryd> page = testJkNsrydService.findPage(new Page<TestJkNsryd>(request, response), testJkNsryd); 
		model.addAttribute("page", page);
		return "modules/nsrydjk/testJkNsrydList";
	}

	@RequiresPermissions("nsrydjk:testJkNsryd:view")
	@RequestMapping(value = "form")
	public String form(TestJkNsryd testJkNsryd, Model model) {
		model.addAttribute("testJkNsryd", testJkNsryd);
		return "modules/nsrydjk/testJkNsrydForm";
	}

	@RequiresPermissions("nsrydjk:testJkNsryd:edit")
	@RequestMapping(value = "save")
	public String save(TestJkNsryd testJkNsryd, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, testJkNsryd)){
			return form(testJkNsryd, model);
		}
		testJkNsrydService.save(testJkNsryd);
		addMessage(redirectAttributes, "保存纳税人成功");
		return "redirect:"+Global.getAdminPath()+"/nsrydjk/testJkNsryd/?repage";
	}
	
	@RequiresPermissions("nsrydjk:testJkNsryd:edit")
	@RequestMapping(value = "delete")
	public String delete(TestJkNsryd testJkNsryd, RedirectAttributes redirectAttributes) {
		testJkNsrydService.delete(testJkNsryd);
		addMessage(redirectAttributes, "删除纳税人成功");
		return "redirect:"+Global.getAdminPath()+"/nsrydjk/testJkNsryd/?repage";
	}

}