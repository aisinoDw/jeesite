/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.nsrydjk.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 1Entity
 * @author 1
 * @version 2016-01-19
 */
public class TestJkNsryd extends DataEntity<TestJkNsryd> {
	
	private static final long serialVersionUID = 1L;
	private String nsrsbh;		// 纳税人识别号
	private String nsrmc;		// 纳税人名称
	private String fxz;		// 风险值
	private String ydbh;		// 疑点编号
	private String ydmc;		// 疑点名称
	private String ydmx;		// 疑点明细
	
	public TestJkNsryd() {
		super();
	}

	public TestJkNsryd(String id){
		super(id);
	}

	@Length(min=1, max=255, message="纳税人识别号长度必须介于 1 和 255 之间")
	public String getNsrsbh() {
		return nsrsbh;
	}

	public void setNsrsbh(String nsrsbh) {
		this.nsrsbh = nsrsbh;
	}
	
	@Length(min=0, max=255, message="纳税人名称长度必须介于 0 和 255 之间")
	public String getNsrmc() {
		return nsrmc;
	}

	public void setNsrmc(String nsrmc) {
		this.nsrmc = nsrmc;
	}
	
	@Length(min=0, max=255, message="风险值长度必须介于 0 和 255 之间")
	public String getFxz() {
		return fxz;
	}

	public void setFxz(String fxz) {
		this.fxz = fxz;
	}
	
	@Length(min=0, max=255, message="疑点编号长度必须介于 0 和 255 之间")
	public String getYdbh() {
		return ydbh;
	}

	public void setYdbh(String ydbh) {
		this.ydbh = ydbh;
	}
	
	@Length(min=0, max=255, message="疑点名称长度必须介于 0 和 255 之间")
	public String getYdmc() {
		return ydmc;
	}

	public void setYdmc(String ydmc) {
		this.ydmc = ydmc;
	}
	
	@Length(min=0, max=255, message="疑点明细长度必须介于 0 和 255 之间")
	public String getYdmx() {
		return ydmx;
	}

	public void setYdmx(String ydmx) {
		this.ydmx = ydmx;
	}
	
}