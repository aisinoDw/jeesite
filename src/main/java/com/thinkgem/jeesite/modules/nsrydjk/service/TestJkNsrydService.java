/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.nsrydjk.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.nsrydjk.entity.TestJkNsryd;
import com.thinkgem.jeesite.modules.nsrydjk.dao.TestJkNsrydDao;

/**
 * 1Service
 * @author 1
 * @version 2016-01-19
 */
@Service
@Transactional(readOnly = true)
public class TestJkNsrydService extends CrudService<TestJkNsrydDao, TestJkNsryd> {

	public TestJkNsryd get(String id) {
		return super.get(id);
	}
	
	public List<TestJkNsryd> findList(TestJkNsryd testJkNsryd) {
		return super.findList(testJkNsryd);
	}
	
	public Page<TestJkNsryd> findPage(Page<TestJkNsryd> page, TestJkNsryd testJkNsryd) {
		return super.findPage(page, testJkNsryd);
	}
	
	@Transactional(readOnly = false)
	public void save(TestJkNsryd testJkNsryd) {
		super.save(testJkNsryd);
	}
	
	@Transactional(readOnly = false)
	public void delete(TestJkNsryd testJkNsryd) {
		super.delete(testJkNsryd);
	}
	
}