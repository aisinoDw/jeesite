/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.jhdq.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.modules.jhdq.entity.TestJhDq;
import com.thinkgem.jeesite.modules.jhdq.service.TestJhDqService;

/**
 * 1Controller
 * @author 1
 * @version 2016-01-14
 */
@Controller
@RequestMapping(value = "${adminPath}/jhdq/testJhDq")
public class TestJhDqController extends BaseController {

	@Autowired
	private TestJhDqService testJhDqService;
	
	@ModelAttribute
	public TestJhDq get(@RequestParam(required=false) String id) {
		TestJhDq entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = testJhDqService.get(id);
		}
		if (entity == null){
			entity = new TestJhDq();
		}
		return entity;
	}
	
	@RequiresPermissions("jhdq:testJhDq:view")
	@RequestMapping(value = {"list", ""})
	public String list(TestJhDq testJhDq, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<TestJhDq> page = testJhDqService.findPage(new Page<TestJhDq>(request, response), testJhDq); 
		List<TestJhDq> list = testJhDqService.findListGraph(testJhDq);
		
		//logger.info("list.size==== "+list.size());
		model.addAttribute("page", page);
		model.addAttribute("list",list);
		return "modules/jhdq/testJhDqList";
	}

	@RequiresPermissions("jhdq:testJhDq:view")
	@RequestMapping(value = "form")
	public String form(TestJhDq testJhDq, Model model) {
		model.addAttribute("testJhDq", testJhDq);
		return "modules/jhdq/testJhDqForm";
	}

	@RequiresPermissions("jhdq:testJhDq:edit")
	@RequestMapping(value = "save")
	public String save(TestJhDq testJhDq, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, testJhDq)){
			return form(testJhDq, model);
		}
		testJhDqService.save(testJhDq);
		addMessage(redirectAttributes, "保存地区成功");
		return "redirect:"+Global.getAdminPath()+"/jhdq/testJhDq/?repage";
	}
	
	@RequiresPermissions("jhdq:testJhDq:edit")
	@RequestMapping(value = "delete")
	public String delete(TestJhDq testJhDq, RedirectAttributes redirectAttributes) {
		testJhDqService.delete(testJhDq);
		addMessage(redirectAttributes, "删除地区成功");
		return "redirect:"+Global.getAdminPath()+"/jhdq/testJhDq/?repage";
	}
	
	@RequiresPermissions("jhdq:testJhDq:view")
	@RequestMapping(value = {"ajaxjhdq"})
	@ResponseBody
	public List<TestJhDq> ajaxjhdq(TestJhDq testJhDq, HttpServletRequest request, HttpServletResponse response, Model model)throws IOException{
		List<TestJhDq> list = testJhDqService.findListGraph(testJhDq);
	    //logger.info("list.size()=====  "+list.size());
		return list;
	}
}