/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.jhdq.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 1Entity
 * @author 1
 * @version 2016-01-14
 */
public class TestJhDq extends DataEntity<TestJhDq> {
	
	private static final long serialVersionUID = 1L;
	private String swjgdm;		// 税务机关代码
	private String swjgmc;		// 税务机关名称
	private String hjse;		// 合计税额
	private String xpsl;		// 小票数量	
	public TestJhDq() {
		super();
	}

	public TestJhDq(String id){
		super(id);
	}

	@Length(min=1, max=255, message="税务机关代码长度必须介于 1 和 255 之间")
	public String getSwjgdm() {
		return swjgdm;
	}

	public void setSwjgdm(String swjgdm) {
		this.swjgdm = swjgdm;
	}
	
	@Length(min=0, max=255, message="税务机关名称长度必须介于 0 和 255 之间")
	public String getSwjgmc() {
		return swjgmc;
	}

	public void setSwjgmc(String swjgmc) {
		this.swjgmc = swjgmc;
	}
	
	@Length(min=0, max=255, message="合计税额长度必须介于 0 和 255 之间")
	public String getHjse() {
		return hjse;
	}

	public void setHjse(String hjse) {
		this.hjse = hjse;
	}
	
	@Length(min=0, max=255, message="小票数量长度必须介于 0 和 255 之间")
	public String getXpsl() {
		return xpsl;
	}

	public void setXpsl(String xpsl) {
		this.xpsl = xpsl;
	}

}