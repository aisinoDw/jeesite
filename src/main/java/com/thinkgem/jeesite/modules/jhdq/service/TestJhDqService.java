/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.jhdq.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.jhdq.entity.TestJhDq;
import com.thinkgem.jeesite.modules.jhdq.dao.TestJhDqDao;

/**
 * 1Service
 * @author 1
 * @version 2016-01-14
 */
@Service
@Transactional(readOnly = true)
public class TestJhDqService extends CrudService<TestJhDqDao, TestJhDq> {

	public TestJhDq get(String id) {
		return super.get(id);
	}
	
	public List<TestJhDq> findList(TestJhDq testJhDq) {
		return super.findList(testJhDq);
	}
	
	public List<TestJhDq> findListGraph(TestJhDq testJhDq){
		return super.findListGraph(testJhDq);
	}
	
	public Page<TestJhDq> findPage(Page<TestJhDq> page, TestJhDq testJhDq) {
		return super.findPage(page, testJhDq);
	}
	
	@Transactional(readOnly = false)
	public void save(TestJhDq testJhDq) {
		super.save(testJhDq);
	}
	
	@Transactional(readOnly = false)
	public void delete(TestJhDq testJhDq) {
		super.delete(testJhDq);
	}
	
}