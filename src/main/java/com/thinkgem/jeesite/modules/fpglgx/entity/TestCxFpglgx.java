/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.fpglgx.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 1Entity
 * @author 1
 * @version 2016-02-26
 */
public class TestCxFpglgx extends DataEntity<TestCxFpglgx> {
	
	private static final long serialVersionUID = 1L;
	private String fpdm;		// 发票代码
	private String fphm;		// 发票号码
	
	public TestCxFpglgx() {
		super();
	}

	public TestCxFpglgx(String id){
		super(id);
	}

	@Length(min=0, max=255, message="发票代码长度必须介于 0 和 255 之间")
	public String getFpdm() {
		return fpdm;
	}

	public void setFpdm(String fpdm) {
		this.fpdm = fpdm;
	}
	
	@Length(min=0, max=255, message="发票号码长度必须介于 0 和 255 之间")
	public String getFphm() {
		return fphm;
	}

	public void setFphm(String fphm) {
		this.fphm = fphm;
	}
	
}