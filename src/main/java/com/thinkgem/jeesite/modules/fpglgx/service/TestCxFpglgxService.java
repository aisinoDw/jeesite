/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.fpglgx.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.fpglgx.entity.TestCxFpglgx;
import com.thinkgem.jeesite.modules.fpglgx.dao.TestCxFpglgxDao;

/**
 * 1Service
 * @author 1
 * @version 2016-02-26
 */
@Service
@Transactional(readOnly = true)
public class TestCxFpglgxService extends CrudService<TestCxFpglgxDao, TestCxFpglgx> {

	public TestCxFpglgx get(String id) {
		return super.get(id);
	}
	
	public List<TestCxFpglgx> findList(TestCxFpglgx testCxFpglgx) {
		return super.findList(testCxFpglgx);
	}
	
	public Page<TestCxFpglgx> findPage(Page<TestCxFpglgx> page, TestCxFpglgx testCxFpglgx) {
		return super.findPage(page, testCxFpglgx);
	}
	
	@Transactional(readOnly = false)
	public void save(TestCxFpglgx testCxFpglgx) {
		super.save(testCxFpglgx);
	}
	
	@Transactional(readOnly = false)
	public void delete(TestCxFpglgx testCxFpglgx) {
		super.delete(testCxFpglgx);
	}
	
}