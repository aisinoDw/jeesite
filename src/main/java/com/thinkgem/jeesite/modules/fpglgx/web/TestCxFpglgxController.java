/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.fpglgx.web;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.impl.util.json.JSONArray;
import org.activiti.engine.impl.util.json.JSONObject;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.fpglgx.entity.Fpglgx;
import com.thinkgem.jeesite.modules.fpglgx.entity.TestCxFpglgx;
import com.thinkgem.jeesite.modules.fpglgx.service.TestCxFpglgxService;
import com.thinkgem.jeesite.modules.xplist.entity.TestCxXplist;

/**
 * 1Controller
 * @author 1
 * @version 2016-02-26
 */
@Controller
@RequestMapping(value = "${adminPath}/fpglgx/testCxFpglgx")
public class TestCxFpglgxController extends BaseController {

	@Autowired
	private TestCxFpglgxService testCxFpglgxService;
	
	@ModelAttribute
	public TestCxFpglgx get(@RequestParam(required=false) String id) {
		TestCxFpglgx entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = testCxFpglgxService.get(id);
		}
		if (entity == null){
			entity = new TestCxFpglgx();
		}
		return entity;
	}
	
	@RequiresPermissions("fpglgx:testCxFpglgx:view")
	@RequestMapping(value = {"list", ""})
	public String list(TestCxFpglgx testCxFpglgx, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<TestCxFpglgx> page = testCxFpglgxService.findPage(new Page<TestCxFpglgx>(request, response), testCxFpglgx); 
		model.addAttribute("page", page);
		return "modules/fpglgx/testCxFpglgxList";
	}


	@RequiresPermissions("fpglgx:testCxFpglgx:view")
	@RequestMapping(value = {"fpglgxjson"})
	public String fpglgxjson(TestCxFpglgx testCxFpglgx, HttpServletRequest request, HttpServletResponse response, Model model) throws Exception{
		String url = "http://192.168.2.176:8889/DSCSClient";
		HttpPost httpPost = new HttpPost(url);
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		String cmd = "getReceiptInfoByInvoiceId";
		nvps.add(new BasicNameValuePair("cmd", cmd));  
		
		String fpdmParam = testCxFpglgx.getFpdm();	
		String fphmParam = testCxFpglgx.getFphm();
		String keys = "";
		if(StringUtils.isNotBlank(fpdmParam)){
			if(keys.length()>0){
				keys += ",";
			}
			keys += "\"fpdm\":\""+fpdmParam+"\"";
		}
		if(StringUtils.isNotBlank(fphmParam)){
			if(keys.length()>0){
				keys += ",";
			}
			keys += "\"fphm\":\""+fphmParam+"\"";
		}
		
		nvps.add(new BasicNameValuePair("arg", "{"+keys+"}")); 
		httpPost.setEntity(new UrlEncodedFormEntity(nvps));
		HttpClient httpclient = new DefaultHttpClient();	
		HttpResponse resp= httpclient.execute(httpPost);
		
		HttpEntity entity = resp.getEntity();
		String jsonresp = EntityUtils.toString(entity);
		logger.info("jsonresp====="+jsonresp);
		JSONObject json = new JSONObject(jsonresp);
		List<Fpglgx> fpglgxQueryList = new ArrayList<Fpglgx>();
		//正常调用
		if(json.getInt("CompleteCode")==0){
			JSONArray jsonarray = json.getJSONArray("Data");
			for(int i=0;i<jsonarray.length();i++){
				JSONObject o = jsonarray.getJSONObject(i);
				
				
					Fpglgx xp = new Fpglgx();
					String nsrsbh = o.getString("nsrsbh");
					String bkkpje = o.getString("bkkpje");
					String dyfpzt = o.getString("dyfpzt");
					String gxid = o.getString("gxid");
					String jshj = o.getString("jshj");
					String kpje = o.getString("kpje");
					String xplsh = o.getString("xplsh");
					
					xp.setBkkpje(bkkpje);
					xp.setDyfpzt(dyfpzt);
					xp.setGxid(gxid);
					xp.setJshj(jshj);
					xp.setKpje(kpje);
					xp.setNsrsbh(nsrsbh);
					xp.setXplsh(xplsh);
					
					fpglgxQueryList.add(xp);
			}
			
			
			logger.info(json.toString());
		}else{
			logger.info(json.toString());
			throw new Exception("接口调用失败!");
			
		}
		
		model.addAttribute("page1", fpglgxQueryList);
		return "modules/fpglgx/testCxFpglgxList";
	}
	
	
	
	
	
	@RequiresPermissions("fpglgx:testCxFpglgx:view")
	@RequestMapping(value = "form")
	public String form(TestCxFpglgx testCxFpglgx, Model model) {
		model.addAttribute("testCxFpglgx", testCxFpglgx);
		return "modules/fpglgx/testCxFpglgxForm";
	}

	@RequiresPermissions("fpglgx:testCxFpglgx:edit")
	@RequestMapping(value = "save")
	public String save(TestCxFpglgx testCxFpglgx, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, testCxFpglgx)){
			return form(testCxFpglgx, model);
		}
		testCxFpglgxService.save(testCxFpglgx);
		addMessage(redirectAttributes, "保存发票成功");
		return "redirect:"+Global.getAdminPath()+"/fpglgx/testCxFpglgx/?repage";
	}
	
	@RequiresPermissions("fpglgx:testCxFpglgx:edit")
	@RequestMapping(value = "delete")
	public String delete(TestCxFpglgx testCxFpglgx, RedirectAttributes redirectAttributes) {
		testCxFpglgxService.delete(testCxFpglgx);
		addMessage(redirectAttributes, "删除发票成功");
		return "redirect:"+Global.getAdminPath()+"/fpglgx/testCxFpglgx/?repage";
	}

}