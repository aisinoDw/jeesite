package com.thinkgem.jeesite.modules.fpglgx.entity;

public class Fpglgx {
	private String nsrsbh;
	private String bkkpje;
	private String dyfpzt;
	private String gxid;
	private String jshj;
	private String kpje;
	private String xplsh;
	public String getNsrsbh() {
		return nsrsbh;
	}
	public void setNsrsbh(String nsrsbh) {
		this.nsrsbh = nsrsbh;
	}
	public String getBkkpje() {
		return bkkpje;
	}
	public void setBkkpje(String bkkpje) {
		this.bkkpje = bkkpje;
	}
	public String getDyfpzt() {
		return dyfpzt;
	}
	public void setDyfpzt(String dyfpzt) {
		this.dyfpzt = dyfpzt;
	}
	public String getGxid() {
		return gxid;
	}
	public void setGxid(String gxid) {
		this.gxid = gxid;
	}
	public String getJshj() {
		return jshj;
	}
	public void setJshj(String jshj) {
		this.jshj = jshj;
	}
	public String getKpje() {
		return kpje;
	}
	public void setKpje(String kpje) {
		this.kpje = kpje;
	}
	public String getXplsh() {
		return xplsh;
	}
	public void setXplsh(String xplsh) {
		this.xplsh = xplsh;
	}
	
}
