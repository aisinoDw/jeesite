/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.xplist.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 1Entity
 * @author 1
 * @version 2016-02-25
 */
public class TestCxXplist extends DataEntity<TestCxXplist> {
	
	private static final long serialVersionUID = 1L;
	private String xpdm;		// 小票代码
	private String xphm;		// 小票号码
	private String slxlh;		// 受理序列号
	
	public TestCxXplist() {
		super();
	}

	public TestCxXplist(String id){
		super(id);
	}

	@Length(min=0, max=255, message="小票代码长度必须介于 0 和 255 之间")
	public String getXpdm() {
		return xpdm;
	}

	public void setXpdm(String xpdm) {
		this.xpdm = xpdm;
	}
	
	@Length(min=0, max=255, message="小票号码长度必须介于 0 和 255 之间")
	public String getXphm() {
		return xphm;
	}

	public void setXphm(String xphm) {
		this.xphm = xphm;
	}
	
	@Length(min=0, max=255, message="受理序列号长度必须介于 0 和 255 之间")
	public String getSlxlh() {
		return slxlh;
	}

	public void setSlxlh(String slxlh) {
		this.slxlh = slxlh;
	}
	
}