/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.xplist.web;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.impl.util.json.JSONArray;
import org.activiti.engine.impl.util.json.JSONObject;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.xplist.entity.TestCxXplist;
import com.thinkgem.jeesite.modules.xplist.entity.XpList;
import com.thinkgem.jeesite.modules.xplist.service.TestCxXplistService;

/**
 * 1Controller
 * @author 1
 * @version 2016-02-25
 */
@Controller
@RequestMapping(value = "${adminPath}/xplist/testCxXplist")
public class TestCxXplistController extends BaseController {

	@Autowired
	private TestCxXplistService testCxXplistService;
	
	@ModelAttribute
	public TestCxXplist get(@RequestParam(required=false) String id) {
		TestCxXplist entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = testCxXplistService.get(id);
		}
		if (entity == null){
			entity = new TestCxXplist();
		}
		return entity;
	}
	
	@RequiresPermissions("xplist:testCxXplist:view")
	@RequestMapping(value = {"list", ""})
	public String list(TestCxXplist testCxXplist, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<TestCxXplist> page = testCxXplistService.findPage(new Page<TestCxXplist>(request, response), testCxXplist); 
		model.addAttribute("page", page);
		return "modules/xplist/testCxXplistList";
	}
	

	@RequiresPermissions("xplist:testCxXplist:view")
	@RequestMapping(value = {"xplistjson"})
	public String xplistjson(TestCxXplist testCxXplist, HttpServletRequest request, HttpServletResponse response, Model model) throws Exception{
		String url = "http://192.168.2.176:8889/DSCSClient";
		HttpPost httpPost = new HttpPost(url);
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		String cmd = "queryData";
		nvps.add(new BasicNameValuePair("cmd", cmd));  
		
		String xpdmParam = testCxXplist.getXpdm();	
		String xphmParam = testCxXplist.getXphm();
		String slxlh = testCxXplist.getSlxlh();
		String keys = "";
		if(StringUtils.isNotBlank(xpdmParam)){
			if(keys.length()>0){
				keys += ",";
			}
			keys += "\"XPDM\":\""+xpdmParam+"\"";
		}
		if(StringUtils.isNotBlank(xphmParam)){
			if(keys.length()>0){
				keys += ",";
			}
			keys += "\"XPHM\":\""+xphmParam+"\"";
		}
		if(StringUtils.isNotBlank(slxlh)){
			if(keys.length()>0){
				keys += ",";
			}
			keys += "\"BILL_NO\":\""+slxlh+"\"";
		}
		
		nvps.add(new BasicNameValuePair("arg", "{\"Keys\":{"+keys+"},\"TableName\":\"yccb_xpxx_cljg_list\"}")); 
		httpPost.setEntity(new UrlEncodedFormEntity(nvps));
		HttpClient httpclient = new DefaultHttpClient();	
		HttpResponse resp= httpclient.execute(httpPost);
		
		HttpEntity entity = resp.getEntity();
		String jsonresp = EntityUtils.toString(entity);
		logger.info("jsonresp====="+jsonresp);
		JSONObject json = new JSONObject(jsonresp);
		List<XpList> xplistQueryList = new ArrayList<XpList>();
		//正常调用
		if(json.getInt("CompleteCode")==0){
			JSONArray jsonarray = json.getJSONArray("Data");
			for(int i=0;i<jsonarray.length();i++){
				//logger.info("#####  i===="+i);
				
				JSONObject o = jsonarray.getJSONObject(i);
				String timeRe;
				// 验签成功和签名验证错误
				if(o.getInt("YQ_STATUS")==0 ||o.getInt("YQ_STATUS")==39 ){ 
					if(!o.has("insertTime")){
						JSONObject jo_insertTime = o.getJSONObject("INSERT_DATE");
						JSONObject jo_time = jo_insertTime.getJSONObject("time");
						Long timeLong = jo_time.getLong("$numberLong");
						logger.info("timelong===="+timeLong);
						Date date = new Date(timeLong);
				        SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				        timeRe = sd.format(date);
					}else{
						timeRe = o.getString("insertTime");
					}
					XpList xp = new XpList();
					String cljg = o.getString("CLJG");
					String error_message = o.getString("ERROR_MESSAGE");
					
					String response_code = o.getString("RESPONSE_CODE");
					String server_name = o.getString("SERVER_NAME");
					String xpdm = o.getString("XPDM");
					String xphm = o.getString("XPHM");
					String yqzt = o.getString("YQZT");
					String yq_msg = o.getString("YQ_MSG");
					String yq_status = o.getInt("YQ_STATUS")+"";
					String zfbz = o.getInt("ZFBZ")+"";
					String bill_no = o.getString("BILL_NO");
					
					xp.setCljg(cljg);
					xp.setError_message(error_message);
					xp.setInsert_date(timeRe);
					xp.setResponse_code(response_code);
					xp.setServer_name(server_name);
					xp.setXpdm(xpdm);
					xp.setXphm(xphm);
					xp.setYqzt(yqzt);
					xp.setYq_msg(yq_msg);
					xp.setYq_status(yq_status);
					xp.setZfbz(zfbz);
					xp.setBill_no(bill_no);
					
					xplistQueryList.add(xp);
				}
				
			}
			
			
			logger.info(json.toString());
		}else{
			logger.info(json.toString());
			throw new Exception("接口调用失败!");
			
		}
		
		model.addAttribute("page1", xplistQueryList);
		return "modules/xplist/testCxXplistList";
	}
	
	
	

	@RequiresPermissions("xplist:testCxXplist:view")
	@RequestMapping(value = "form")
	public String form(TestCxXplist testCxXplist, Model model) {
		model.addAttribute("testCxXplist", testCxXplist);
		return "modules/xplist/testCxXplistForm";
	}

	@RequiresPermissions("xplist:testCxXplist:edit")
	@RequestMapping(value = "save")
	public String save(TestCxXplist testCxXplist, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, testCxXplist)){
			return form(testCxXplist, model);
		}
		testCxXplistService.save(testCxXplist);
		addMessage(redirectAttributes, "保存小票成功");
		return "redirect:"+Global.getAdminPath()+"/xplist/testCxXplist/?repage";
	}
	
	@RequiresPermissions("xplist:testCxXplist:edit")
	@RequestMapping(value = "delete")
	public String delete(TestCxXplist testCxXplist, RedirectAttributes redirectAttributes) {
		testCxXplistService.delete(testCxXplist);
		addMessage(redirectAttributes, "删除小票成功");
		return "redirect:"+Global.getAdminPath()+"/xplist/testCxXplist/?repage";
	}

}