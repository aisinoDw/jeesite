/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.xplist.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.xplist.entity.TestCxXplist;
import com.thinkgem.jeesite.modules.xplist.dao.TestCxXplistDao;

/**
 * 1Service
 * @author 1
 * @version 2016-02-25
 */
@Service
@Transactional(readOnly = true)
public class TestCxXplistService extends CrudService<TestCxXplistDao, TestCxXplist> {

	public TestCxXplist get(String id) {
		return super.get(id);
	}
	
	public List<TestCxXplist> findList(TestCxXplist testCxXplist) {
		return super.findList(testCxXplist);
	}
	
	public Page<TestCxXplist> findPage(Page<TestCxXplist> page, TestCxXplist testCxXplist) {
		return super.findPage(page, testCxXplist);
	}
	
	@Transactional(readOnly = false)
	public void save(TestCxXplist testCxXplist) {
		super.save(testCxXplist);
	}
	
	@Transactional(readOnly = false)
	public void delete(TestCxXplist testCxXplist) {
		super.delete(testCxXplist);
	}
	
}