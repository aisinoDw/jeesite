package com.thinkgem.jeesite.modules.xplist.entity;

public class XpList {
	private String cljg;
	private String error_message;
	private String insert_date;
	private String je;
	private String kprq;
	private String response_code;
	private String server_name;
	private String xpdm;
	private String xphm;
	private String yqzt;
	private String yq_msg;
	private String yq_status;
	private String zfbz;
	private String bill_no;
	public String getCljg() {
		return cljg;
	}
	public void setCljg(String cljg) {
		this.cljg = cljg;
	}
	public String getError_message() {
		return error_message;
	}
	public void setError_message(String error_message) {
		this.error_message = error_message;
	}
	public String getInsert_date() {
		return insert_date;
	}
	public void setInsert_date(String insert_date) {
		this.insert_date = insert_date;
	}
	public String getJe() {
		return je;
	}
	public void setJe(String je) {
		this.je = je;
	}
	public String getKprq() {
		return kprq;
	}
	public void setKprq(String kprq) {
		this.kprq = kprq;
	}
	public String getResponse_code() {
		return response_code;
	}
	public void setResponse_code(String response_code) {
		this.response_code = response_code;
	}
	public String getServer_name() {
		return server_name;
	}
	public void setServer_name(String server_name) {
		this.server_name = server_name;
	}
	public String getXpdm() {
		return xpdm;
	}
	public void setXpdm(String xpdm) {
		this.xpdm = xpdm;
	}
	public String getXphm() {
		return xphm;
	}
	public void setXphm(String xphm) {
		this.xphm = xphm;
	}
	public String getYqzt() {
		return yqzt;
	}
	public void setYqzt(String yqzt) {
		this.yqzt = yqzt;
	}
	public String getYq_msg() {
		return yq_msg;
	}
	public void setYq_msg(String yq_msg) {
		this.yq_msg = yq_msg;
	}
	public String getYq_status() {
		return yq_status;
	}
	public void setYq_status(String yq_status) {
		this.yq_status = yq_status;
	}
	public String getZfbz() {
		return zfbz;
	}
	public void setZfbz(String zfbz) {
		this.zfbz = zfbz;
	}
	public String getBill_no() {
		return bill_no;
	}
	public void setBill_no(String bill_no) {
		this.bill_no = bill_no;
	}
}
