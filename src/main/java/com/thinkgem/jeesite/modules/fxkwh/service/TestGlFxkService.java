/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.fxkwh.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.fxkwh.entity.TestGlFxk;
import com.thinkgem.jeesite.modules.fxkwh.dao.TestGlFxkDao;

/**
 * 1Service
 * @author 1
 * @version 2016-01-19
 */
@Service
@Transactional(readOnly = true)
public class TestGlFxkService extends CrudService<TestGlFxkDao, TestGlFxk> {

	public TestGlFxk get(String id) {
		return super.get(id);
	}
	
	public List<TestGlFxk> findList(TestGlFxk testGlFxk) {
		return super.findList(testGlFxk);
	}
	
	public Page<TestGlFxk> findPage(Page<TestGlFxk> page, TestGlFxk testGlFxk) {
		return super.findPage(page, testGlFxk);
	}
	
	@Transactional(readOnly = false)
	public void save(TestGlFxk testGlFxk) {
		super.save(testGlFxk);
	}
	
	@Transactional(readOnly = false)
	public void delete(TestGlFxk testGlFxk) {
		super.delete(testGlFxk);
	}
	
}