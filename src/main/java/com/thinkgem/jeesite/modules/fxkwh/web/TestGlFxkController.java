/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.fxkwh.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.fxkwh.entity.TestGlFxk;
import com.thinkgem.jeesite.modules.fxkwh.service.TestGlFxkService;

/**
 * 1Controller
 * @author 1
 * @version 2016-01-19
 */
@Controller
@RequestMapping(value = "${adminPath}/fxkwh/testGlFxk")
public class TestGlFxkController extends BaseController {

	@Autowired
	private TestGlFxkService testGlFxkService;
	
	@ModelAttribute
	public TestGlFxk get(@RequestParam(required=false) String id) {
		TestGlFxk entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = testGlFxkService.get(id);
		}
		if (entity == null){
			entity = new TestGlFxk();
		}
		return entity;
	}
	
	@RequiresPermissions("fxkwh:testGlFxk:view")
	@RequestMapping(value = {"list", ""})
	public String list(TestGlFxk testGlFxk, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<TestGlFxk> page = testGlFxkService.findPage(new Page<TestGlFxk>(request, response), testGlFxk); 
		model.addAttribute("page", page);
		return "modules/fxkwh/testGlFxkList";
	}

	@RequiresPermissions("fxkwh:testGlFxk:view")
	@RequestMapping(value = "form")
	public String form(TestGlFxk testGlFxk, Model model) {
		model.addAttribute("testGlFxk", testGlFxk);
		return "modules/fxkwh/testGlFxkForm";
	}

	@RequiresPermissions("fxkwh:testGlFxk:edit")
	@RequestMapping(value = "save")
	public String save(TestGlFxk testGlFxk, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, testGlFxk)){
			return form(testGlFxk, model);
		}
		testGlFxkService.save(testGlFxk);
		addMessage(redirectAttributes, "保存风险成功");
		return "redirect:"+Global.getAdminPath()+"/fxkwh/testGlFxk/?repage";
	}
	
	@RequiresPermissions("fxkwh:testGlFxk:edit")
	@RequestMapping(value = "delete")
	public String delete(TestGlFxk testGlFxk, RedirectAttributes redirectAttributes) {
		testGlFxkService.delete(testGlFxk);
		addMessage(redirectAttributes, "删除风险成功");
		return "redirect:"+Global.getAdminPath()+"/fxkwh/testGlFxk/?repage";
	}

}