/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.fxkwh.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 1Entity
 * @author 1
 * @version 2016-01-19
 */
public class TestGlFxk extends DataEntity<TestGlFxk> {
	
	private static final long serialVersionUID = 1L;
	private String fxdm;		// 风险代码
	private String fxmc;		// 风险名称
	private String fxz;		// 风险值
	private String lrr;		// 录入人
	private String lrrq;		// 录入日期
	
	public TestGlFxk() {
		super();
	}

	public TestGlFxk(String id){
		super(id);
	}

	@Length(min=1, max=255, message="风险代码长度必须介于 1 和 255 之间")
	public String getFxdm() {
		return fxdm;
	}

	public void setFxdm(String fxdm) {
		this.fxdm = fxdm;
	}
	
	@Length(min=0, max=255, message="风险名称长度必须介于 0 和 255 之间")
	public String getFxmc() {
		return fxmc;
	}

	public void setFxmc(String fxmc) {
		this.fxmc = fxmc;
	}
	
	@Length(min=0, max=255, message="风险值长度必须介于 0 和 255 之间")
	public String getFxz() {
		return fxz;
	}

	public void setFxz(String fxz) {
		this.fxz = fxz;
	}
	
	@Length(min=0, max=255, message="录入人长度必须介于 0 和 255 之间")
	public String getLrr() {
		return lrr;
	}

	public void setLrr(String lrr) {
		this.lrr = lrr;
	}
	
	@Length(min=0, max=255, message="录入日期长度必须介于 0 和 255 之间")
	public String getLrrq() {
		return lrrq;
	}

	public void setLrrq(String lrrq) {
		this.lrrq = lrrq;
	}
	
}