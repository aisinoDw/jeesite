/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.csxsefx.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.csxsefx.entity.TestFxCsxse;

/**
 * 1DAO接口
 * @author 1
 * @version 2016-01-25
 */
@MyBatisDao
public interface TestFxCsxseDao extends CrudDao<TestFxCsxse> {
	
}