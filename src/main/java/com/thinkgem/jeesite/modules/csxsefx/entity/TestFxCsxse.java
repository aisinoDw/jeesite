/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.csxsefx.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 1Entity
 * @author 1
 * @version 2016-01-25
 */
public class TestFxCsxse extends DataEntity<TestFxCsxse> {
	
	private static final long serialVersionUID = 1L;
	private String spfl;		// 商品分类
	private Double flxl;		// 分类销量
	private String tjsj;		// 统计时间
	private Double nxse;		// 年销售额
	
	public TestFxCsxse() {
		super();
	}

	public TestFxCsxse(String id){
		super(id);
	}

	@Length(min=0, max=255, message="商品分类长度必须介于 0 和 255 之间")
	public String getSpfl() {
		return spfl;
	}

	public void setSpfl(String spfl) {
		this.spfl = spfl;
	}
	
	public Double getFlxl() {
		return flxl;
	}

	public void setFlxl(Double flxl) {
		this.flxl = flxl;
	}
	
	@Length(min=0, max=255, message="统计时间长度必须介于 0 和 255 之间")
	public String getTjsj() {
		return tjsj;
	}

	public void setTjsj(String tjsj) {
		this.tjsj = tjsj;
	}
	
	public Double getNxse() {
		return nxse;
	}

	public void setNxse(Double nxse) {
		this.nxse = nxse;
	}
	
}