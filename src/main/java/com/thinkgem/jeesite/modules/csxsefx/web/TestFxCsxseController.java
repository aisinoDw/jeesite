/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.csxsefx.web;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.modules.csxsefx.entity.TestFxCsxse;
import com.thinkgem.jeesite.modules.csxsefx.service.TestFxCsxseService;

/**
 * 1Controller
 * @author 1
 * @version 2016-01-25
 */
@Controller
@RequestMapping(value = "${adminPath}/csxsefx/testFxCsxse")
public class TestFxCsxseController extends BaseController {

	@Autowired
	private TestFxCsxseService testFxCsxseService;
	
	@ModelAttribute
	public TestFxCsxse get(@RequestParam(required=false) String id) {
		TestFxCsxse entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = testFxCsxseService.get(id);
		}
		if (entity == null){
			entity = new TestFxCsxse();
		}
		return entity;
	}
	
	@RequiresPermissions("csxsefx:testFxCsxse:view")
	@RequestMapping(value = {"list", ""})
	public String list(TestFxCsxse testFxCsxse, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<TestFxCsxse> page = testFxCsxseService.findPage(new Page<TestFxCsxse>(request, response), testFxCsxse); 
		model.addAttribute("page", page);
		return "modules/csxsefx/testFxCsxseList";
	}
	
	
	@RequiresPermissions("csxsefx:testFxCsxse:view")
	@RequestMapping(value = {"ajaxsefx"})
	@ResponseBody
	public List<List<TestFxCsxse>> ajaxsefx(TestFxCsxse testFxCsxse, HttpServletRequest request, HttpServletResponse response, Model model) {
		String year = request.getParameter("year");
		String month = "";
		List<List<TestFxCsxse>> listout = new ArrayList<List<TestFxCsxse>>();
		for(int i=1;i<=12;i++){
			if(i<10){
				month = year+"0"+i;
			}else{
				month = year+""+i;
			}
			testFxCsxse.setTjsj(month);
			List<TestFxCsxse> list = testFxCsxseService.findListGraph(testFxCsxse);
			listout.add(list);
		}
		return listout;
	}

	@RequiresPermissions("csxsefx:testFxCsxse:view")
	@RequestMapping(value = "form")
	public String form(TestFxCsxse testFxCsxse, Model model) {
		model.addAttribute("testFxCsxse", testFxCsxse);
		return "modules/csxsefx/testFxCsxseForm";
	}

	@RequiresPermissions("csxsefx:testFxCsxse:edit")
	@RequestMapping(value = "save")
	public String save(TestFxCsxse testFxCsxse, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, testFxCsxse)){
			return form(testFxCsxse, model);
		}
		testFxCsxseService.save(testFxCsxse);
		addMessage(redirectAttributes, "保存商品分类成功");
		return "redirect:"+Global.getAdminPath()+"/csxsefx/testFxCsxse/?repage";
	}
	
	@RequiresPermissions("csxsefx:testFxCsxse:edit")
	@RequestMapping(value = "delete")
	public String delete(TestFxCsxse testFxCsxse, RedirectAttributes redirectAttributes) {
		testFxCsxseService.delete(testFxCsxse);
		addMessage(redirectAttributes, "删除商品分类成功");
		return "redirect:"+Global.getAdminPath()+"/csxsefx/testFxCsxse/?repage";
	}

}