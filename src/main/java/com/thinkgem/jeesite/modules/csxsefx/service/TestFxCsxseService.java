/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.csxsefx.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.csxsefx.entity.TestFxCsxse;
import com.thinkgem.jeesite.modules.csxsefx.dao.TestFxCsxseDao;

/**
 * 1Service
 * @author 1
 * @version 2016-01-25
 */
@Service
@Transactional(readOnly = true)
public class TestFxCsxseService extends CrudService<TestFxCsxseDao, TestFxCsxse> {

	public TestFxCsxse get(String id) {
		return super.get(id);
	}
	
	public List<TestFxCsxse> findList(TestFxCsxse testFxCsxse) {
		return super.findList(testFxCsxse);
	}
	
	public Page<TestFxCsxse> findPage(Page<TestFxCsxse> page, TestFxCsxse testFxCsxse) {
		return super.findPage(page, testFxCsxse);
	}
	
	@Transactional(readOnly = false)
	public void save(TestFxCsxse testFxCsxse) {
		super.save(testFxCsxse);
	}
	
	@Transactional(readOnly = false)
	public void delete(TestFxCsxse testFxCsxse) {
		super.delete(testFxCsxse);
	}
	
}