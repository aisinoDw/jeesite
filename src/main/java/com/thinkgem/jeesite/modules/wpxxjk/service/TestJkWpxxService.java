/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.wpxxjk.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.wpxxjk.entity.TestJkWpxx;
import com.thinkgem.jeesite.modules.wpxxjk.dao.TestJkWpxxDao;

/**
 * 1Service
 * @author 1
 * @version 2016-01-26
 */
@Service
@Transactional(readOnly = true)
public class TestJkWpxxService extends CrudService<TestJkWpxxDao, TestJkWpxx> {

	public TestJkWpxx get(String id) {
		return super.get(id);
	}
	
	public List<TestJkWpxx> findList(TestJkWpxx testJkWpxx) {
		return super.findList(testJkWpxx);
	}
	
	public Page<TestJkWpxx> findPage(Page<TestJkWpxx> page, TestJkWpxx testJkWpxx) {
		return super.findPage(page, testJkWpxx);
	}
	
	@Transactional(readOnly = false)
	public void save(TestJkWpxx testJkWpxx) {
		super.save(testJkWpxx);
	}
	
	@Transactional(readOnly = false)
	public void delete(TestJkWpxx testJkWpxx) {
		super.delete(testJkWpxx);
	}
	
}