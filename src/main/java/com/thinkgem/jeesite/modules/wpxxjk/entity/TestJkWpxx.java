/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.wpxxjk.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 1Entity
 * @author 1
 * @version 2016-01-26
 */
public class TestJkWpxx extends DataEntity<TestJkWpxx> {
	
	private static final long serialVersionUID = 1L;
	private String nsrsbh;		// 纳税人识别号
	private String nsrmc;		// 纳税人名称
	private Double hjje;		// 合计金额
	private Double wpje;		// 无票金额
	private Double wpbl;		// 无票比例
	
	public TestJkWpxx() {
		super();
	}

	public TestJkWpxx(String id){
		super(id);
	}

	@Length(min=0, max=255, message="纳税人识别号长度必须介于 0 和 255 之间")
	public String getNsrsbh() {
		return nsrsbh;
	}

	public void setNsrsbh(String nsrsbh) {
		this.nsrsbh = nsrsbh;
	}
	
	@Length(min=0, max=255, message="纳税人名称长度必须介于 0 和 255 之间")
	public String getNsrmc() {
		return nsrmc;
	}

	public void setNsrmc(String nsrmc) {
		this.nsrmc = nsrmc;
	}
	
	public Double getHjje() {
		return hjje;
	}

	public void setHjje(Double hjje) {
		this.hjje = hjje;
	}
	
	public Double getWpje() {
		return wpje;
	}

	public void setWpje(Double wpje) {
		this.wpje = wpje;
	}
	
	public Double getWpbl() {
		return wpbl;
	}

	public void setWpbl(Double wpbl) {
		this.wpbl = wpbl;
	}
	
}