/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.wpxxjk.web;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.modules.wpxxjk.entity.TestJkWpxx;
import com.thinkgem.jeesite.modules.wpxxjk.service.TestJkWpxxService;

/**
 * 1Controller
 * @author 1
 * @version 2016-01-26
 */
@Controller
@RequestMapping(value = "${adminPath}/wpxxjk/testJkWpxx")
public class TestJkWpxxController extends BaseController {

	@Autowired
	private TestJkWpxxService testJkWpxxService;
	
	@ModelAttribute
	public TestJkWpxx get(@RequestParam(required=false) String id) {
		TestJkWpxx entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = testJkWpxxService.get(id);
		}
		if (entity == null){
			entity = new TestJkWpxx();
		}
		return entity;
	}
	
	@RequiresPermissions("wpxxjk:testJkWpxx:view")
	@RequestMapping(value = {"list", ""})
	public String list(TestJkWpxx testJkWpxx, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<TestJkWpxx> page = testJkWpxxService.findPage(new Page<TestJkWpxx>(request, response), testJkWpxx); 
		model.addAttribute("page", page);
		return "modules/wpxxjk/testJkWpxxList";
	}
	
	@RequiresPermissions("wpxxjk:testJkWpxx:view")
	@RequestMapping(value = {"ajaxwpxxjk"})
	@ResponseBody
	public List<TestJkWpxx> ajaxwpxxjk(TestJkWpxx testJkWpxx, HttpServletRequest request, HttpServletResponse response, Model model) {
		List<TestJkWpxx> list = testJkWpxxService.findListGraph(testJkWpxx);
		List<TestJkWpxx> listnew = new ArrayList<TestJkWpxx>();
		for(int i=0 ; i<3; i++){
			listnew.add(list.get(i));
		}
		return listnew;
	}
	
	
	@RequiresPermissions("wpxxjk:testJkWpxx:view")
	@RequestMapping(value = "form")
	public String form(TestJkWpxx testJkWpxx, Model model) {
		model.addAttribute("testJkWpxx", testJkWpxx);
		return "modules/wpxxjk/testJkWpxxForm";
	}

	@RequiresPermissions("wpxxjk:testJkWpxx:edit")
	@RequestMapping(value = "save")
	public String save(TestJkWpxx testJkWpxx, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, testJkWpxx)){
			return form(testJkWpxx, model);
		}
		testJkWpxxService.save(testJkWpxx);
		addMessage(redirectAttributes, "保存纳税人成功");
		return "redirect:"+Global.getAdminPath()+"/wpxxjk/testJkWpxx/?repage";
	}
	
	@RequiresPermissions("wpxxjk:testJkWpxx:edit")
	@RequestMapping(value = "delete")
	public String delete(TestJkWpxx testJkWpxx, RedirectAttributes redirectAttributes) {
		testJkWpxxService.delete(testJkWpxx);
		addMessage(redirectAttributes, "删除纳税人成功");
		return "redirect:"+Global.getAdminPath()+"/wpxxjk/testJkWpxx/?repage";
	}

}