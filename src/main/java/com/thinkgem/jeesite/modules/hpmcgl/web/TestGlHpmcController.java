/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.hpmcgl.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.hpmcgl.entity.TestGlHpmc;
import com.thinkgem.jeesite.modules.hpmcgl.service.TestGlHpmcService;

/**
 * 1Controller
 * @author 1
 * @version 2016-01-19
 */
@Controller
@RequestMapping(value = "${adminPath}/hpmcgl/testGlHpmc")
public class TestGlHpmcController extends BaseController {

	@Autowired
	private TestGlHpmcService testGlHpmcService;
	
	@ModelAttribute
	public TestGlHpmc get(@RequestParam(required=false) String id) {
		TestGlHpmc entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = testGlHpmcService.get(id);
		}
		if (entity == null){
			entity = new TestGlHpmc();
		}
		return entity;
	}
	
	@RequiresPermissions("hpmcgl:testGlHpmc:view")
	@RequestMapping(value = {"list", ""})
	public String list(TestGlHpmc testGlHpmc, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<TestGlHpmc> page = testGlHpmcService.findPage(new Page<TestGlHpmc>(request, response), testGlHpmc); 
		model.addAttribute("page", page);
		return "modules/hpmcgl/testGlHpmcList";
	}

	@RequiresPermissions("hpmcgl:testGlHpmc:view")
	@RequestMapping(value = "form")
	public String form(TestGlHpmc testGlHpmc, Model model) {
		model.addAttribute("testGlHpmc", testGlHpmc);
		return "modules/hpmcgl/testGlHpmcForm";
	}

	@RequiresPermissions("hpmcgl:testGlHpmc:edit")
	@RequestMapping(value = "save")
	public String save(TestGlHpmc testGlHpmc, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, testGlHpmc)){
			return form(testGlHpmc, model);
		}
		testGlHpmcService.save(testGlHpmc);
		addMessage(redirectAttributes, "保存货品成功");
		return "redirect:"+Global.getAdminPath()+"/hpmcgl/testGlHpmc/?repage";
	}
	
	@RequiresPermissions("hpmcgl:testGlHpmc:edit")
	@RequestMapping(value = "delete")
	public String delete(TestGlHpmc testGlHpmc, RedirectAttributes redirectAttributes) {
		testGlHpmcService.delete(testGlHpmc);
		addMessage(redirectAttributes, "删除货品成功");
		return "redirect:"+Global.getAdminPath()+"/hpmcgl/testGlHpmc/?repage";
	}

}