/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.hpmcgl.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 1Entity
 * @author 1
 * @version 2016-01-19
 */
public class TestGlHpmc extends DataEntity<TestGlHpmc> {
	
	private static final long serialVersionUID = 1L;
	private String mc;		// 名称
	private String spbm;		// 商品编号
	
	public TestGlHpmc() {
		super();
	}

	public TestGlHpmc(String id){
		super(id);
	}

	@Length(min=1, max=255, message="名称长度必须介于 1 和 255 之间")
	public String getMc() {
		return mc;
	}

	public void setMc(String mc) {
		this.mc = mc;
	}
	
	@Length(min=0, max=255, message="商品编号长度必须介于 0 和 255 之间")
	public String getSpbm() {
		return spbm;
	}

	public void setSpbm(String spbm) {
		this.spbm = spbm;
	}
	
}