/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.hpmcgl.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.hpmcgl.entity.TestGlHpmc;
import com.thinkgem.jeesite.modules.hpmcgl.dao.TestGlHpmcDao;

/**
 * 1Service
 * @author 1
 * @version 2016-01-19
 */
@Service
@Transactional(readOnly = true)
public class TestGlHpmcService extends CrudService<TestGlHpmcDao, TestGlHpmc> {

	public TestGlHpmc get(String id) {
		return super.get(id);
	}
	
	public List<TestGlHpmc> findList(TestGlHpmc testGlHpmc) {
		return super.findList(testGlHpmc);
	}
	
	public Page<TestGlHpmc> findPage(Page<TestGlHpmc> page, TestGlHpmc testGlHpmc) {
		return super.findPage(page, testGlHpmc);
	}
	
	@Transactional(readOnly = false)
	public void save(TestGlHpmc testGlHpmc) {
		super.save(testGlHpmc);
	}
	
	@Transactional(readOnly = false)
	public void delete(TestGlHpmc testGlHpmc) {
		super.delete(testGlHpmc);
	}
	
}