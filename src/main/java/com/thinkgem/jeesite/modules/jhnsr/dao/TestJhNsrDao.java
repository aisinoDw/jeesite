/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.jhnsr.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.jhnsr.entity.TestJhNsr;

/**
 * 1DAO接口
 * @author 1
 * @version 2016-01-14
 */
@MyBatisDao
public interface TestJhNsrDao extends CrudDao<TestJhNsr> {
	
}