/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.jhnsr.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 1Entity
 * @author 1
 * @version 2016-01-14
 */
public class TestJhNsr extends DataEntity<TestJhNsr> {
	
	private static final long serialVersionUID = 1L;
	private String nsrsbh;		// 纳税人识别号
	private String nsrmc;		// 纳税人名称
	private String hjje;		// 合计金额
	private String hjse;		// 合计税额
	private String xpsl;		// 小票数量
	
	public TestJhNsr() {
		super();
	}

	public TestJhNsr(String id){
		super(id);
	}

	@Length(min=1, max=255, message="纳税人识别号长度必须介于 1 和 255 之间")
	public String getNsrsbh() {
		return nsrsbh;
	}

	public void setNsrsbh(String nsrsbh) {
		this.nsrsbh = nsrsbh;
	}
	
	@Length(min=0, max=255, message="纳税人名称长度必须介于 0 和 255 之间")
	public String getNsrmc() {
		return nsrmc;
	}

	public void setNsrmc(String nsrmc) {
		this.nsrmc = nsrmc;
	}
	
	@Length(min=0, max=255, message="合计金额长度必须介于 0 和 255 之间")
	public String getHjje() {
		return hjje;
	}

	public void setHjje(String hjje) {
		this.hjje = hjje;
	}
	
	@Length(min=0, max=255, message="合计税额长度必须介于 0 和 255 之间")
	public String getHjse() {
		return hjse;
	}

	public void setHjse(String hjse) {
		this.hjse = hjse;
	}
	
	@Length(min=0, max=255, message="小票数量长度必须介于 0 和 255 之间")
	public String getXpsl() {
		return xpsl;
	}

	public void setXpsl(String xpsl) {
		this.xpsl = xpsl;
	}
	
}