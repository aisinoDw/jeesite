/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.jhnsr.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.jhnsr.entity.TestJhNsr;
import com.thinkgem.jeesite.modules.jhnsr.dao.TestJhNsrDao;

/**
 * 1Service
 * @author 1
 * @version 2016-01-14
 */

@Service
@Transactional(readOnly = true)
public class TestJhNsrService extends CrudService<TestJhNsrDao, TestJhNsr> {

	public TestJhNsr get(String id) {
		return super.get(id);
	}
	
	public List<TestJhNsr> findList(TestJhNsr testJhNsr) {
		return super.findList(testJhNsr);
	}
	
	public List<TestJhNsr> findListGraph(TestJhNsr testJhNsr){
		return super.findListGraph(testJhNsr);
		
	}
	
	public Page<TestJhNsr> findPage(Page<TestJhNsr> page, TestJhNsr testJhNsr) {
		return super.findPage(page, testJhNsr);
	}
	
	@Transactional(readOnly = false)
	public void save(TestJhNsr testJhNsr) {
		super.save(testJhNsr);
	}
	
	@Transactional(readOnly = false)
	public void delete(TestJhNsr testJhNsr) {
		super.delete(testJhNsr);
	}
	
}