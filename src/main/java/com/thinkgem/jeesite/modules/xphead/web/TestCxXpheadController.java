/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.xphead.web;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.impl.util.json.JSONArray;
import org.activiti.engine.impl.util.json.JSONObject;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.xphead.entity.TestCxXphead;
import com.thinkgem.jeesite.modules.xphead.entity.XpHead;
import com.thinkgem.jeesite.modules.xphead.service.TestCxXpheadService;

/**
 * 1Controller
 * @author 1
 * @version 2016-02-25
 */
@Controller
@RequestMapping(value = "${adminPath}/xphead/testCxXphead")
public class TestCxXpheadController extends BaseController {

	@Autowired
	private TestCxXpheadService testCxXpheadService;
	
	@ModelAttribute
	public TestCxXphead get(@RequestParam(required=false) String id) {
		TestCxXphead entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = testCxXpheadService.get(id);
		}
		if (entity == null){
			entity = new TestCxXphead();
		}
		return entity;
	}
	
	@RequiresPermissions("xphead:testCxXphead:view")
	@RequestMapping(value = {"list", ""})
	public String list(TestCxXphead testCxXphead, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<TestCxXphead> page = testCxXpheadService.findPage(new Page<TestCxXphead>(request, response), testCxXphead); 
		model.addAttribute("page", page);
		return "modules/xphead/testCxXpheadList";
	}
	
	
	@RequiresPermissions("xphead:testCxXphead:view")
	@RequestMapping(value = {"xpheadjson"})
	public String xpheadjson(TestCxXphead testCxXphead, HttpServletRequest request, HttpServletResponse response, Model model) throws Exception{
		String url = "http://192.168.2.176:8889/DSCSClient";
		HttpPost httpPost = new HttpPost(url);
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		String cmd = "queryData";
		nvps.add(new BasicNameValuePair("cmd", cmd));  
		
		String slxlh = testCxXphead.getSlxlh();
		String keys = "";
		if(StringUtils.isNotBlank(slxlh)){
			keys += "\"BILL_NO\":\""+slxlh+"\"";
		}
		
		nvps.add(new BasicNameValuePair("arg", "{\"Keys\":{"+keys+"},\"TableName\":\"yccb_xpxx_cljg_head\"}")); 
		httpPost.setEntity(new UrlEncodedFormEntity(nvps));
		HttpClient httpclient = new DefaultHttpClient();	
		HttpResponse resp= httpclient.execute(httpPost);
		
		HttpEntity entity = resp.getEntity();
		String jsonresp = EntityUtils.toString(entity);
		logger.info("jsonresp====="+jsonresp);
		JSONObject json = new JSONObject(jsonresp);
		List<XpHead> xpheadQueryList = new ArrayList<XpHead>();
		//正常调用
		if(json.getInt("CompleteCode")==0){
			JSONArray jsonarray = json.getJSONArray("Data");
			for(int i=0;i<jsonarray.length();i++){
				//logger.info("#####  i===="+i);
				JSONObject o = jsonarray.getJSONObject(i);
				XpHead xp = new XpHead();
				String bill_no = o.getString("BILL_NO");
				String insertTime = o.getString("insertTime");
				String cl_zt = o.getString("CL_ZT");
				String filt_name = o.getString("FILE_NAME");
				String opt_type = o.getString("OPT_TYPE");
				String sk_type = o.getString("SK_TYPE");
				String xpsl = o.getString("XPSL");
				
				xp.setBill_no(bill_no);
				xp.setInsertTime(insertTime);
				xp.setCl_zt(cl_zt);
				xp.setFilt_name(filt_name);
				xp.setOpt_type(opt_type);
				xp.setSk_type(sk_type);
				xp.setXpsl(xpsl);
				
				xpheadQueryList.add(xp);
			}
			
			
			logger.info(json.toString());
		}else{
			logger.info(json.toString());
			throw new Exception("接口调用失败!");
			
		}
		
		model.addAttribute("page1", xpheadQueryList);
		return "modules/xphead/testCxXpheadList";
	}
	
	

	@RequiresPermissions("xphead:testCxXphead:view")
	@RequestMapping(value = "form")
	public String form(TestCxXphead testCxXphead, Model model) {
		model.addAttribute("testCxXphead", testCxXphead);
		return "modules/xphead/testCxXpheadForm";
	}

	@RequiresPermissions("xphead:testCxXphead:edit")
	@RequestMapping(value = "save")
	public String save(TestCxXphead testCxXphead, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, testCxXphead)){
			return form(testCxXphead, model);
		}
		testCxXpheadService.save(testCxXphead);
		addMessage(redirectAttributes, "保存小票成功");
		return "redirect:"+Global.getAdminPath()+"/xphead/testCxXphead/?repage";
	}
	
	@RequiresPermissions("xphead:testCxXphead:edit")
	@RequestMapping(value = "delete")
	public String delete(TestCxXphead testCxXphead, RedirectAttributes redirectAttributes) {
		testCxXpheadService.delete(testCxXphead);
		addMessage(redirectAttributes, "删除小票成功");
		return "redirect:"+Global.getAdminPath()+"/xphead/testCxXphead/?repage";
	}

}