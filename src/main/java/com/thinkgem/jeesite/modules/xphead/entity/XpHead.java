package com.thinkgem.jeesite.modules.xphead.entity;

public class XpHead {
	private String bill_no;
	private String cl_zt;
	private String filt_name;
	private String opt_type;
	private String sk_type;
	private String xpsl;
	private String insertTime;
	public String getBill_no() {
		return bill_no;
	}
	public void setBill_no(String bill_no) {
		this.bill_no = bill_no;
	}
	public String getCl_zt() {
		return cl_zt;
	}
	public void setCl_zt(String cl_zt) {
		this.cl_zt = cl_zt;
	}
	public String getFilt_name() {
		return filt_name;
	}
	public void setFilt_name(String filt_name) {
		this.filt_name = filt_name;
	}
	public String getOpt_type() {
		return opt_type;
	}
	public void setOpt_type(String opt_type) {
		this.opt_type = opt_type;
	}
	public String getSk_type() {
		return sk_type;
	}
	public void setSk_type(String sk_type) {
		this.sk_type = sk_type;
	}
	public String getXpsl() {
		return xpsl;
	}
	public void setXpsl(String xpsl) {
		this.xpsl = xpsl;
	}
	public String getInsertTime() {
		return insertTime;
	}
	public void setInsertTime(String insertTime) {
		this.insertTime = insertTime;
	}
	
}
