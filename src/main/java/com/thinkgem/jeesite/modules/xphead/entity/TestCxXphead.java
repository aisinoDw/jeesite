/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.xphead.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 1Entity
 * @author 1
 * @version 2016-02-25
 */
public class TestCxXphead extends DataEntity<TestCxXphead> {
	
	private static final long serialVersionUID = 1L;
	private String slxlh;		// 受理序列号
	
	public TestCxXphead() {
		super();
	}

	public TestCxXphead(String id){
		super(id);
	}

	@Length(min=0, max=255, message="受理序列号长度必须介于 0 和 255 之间")
	public String getSlxlh() {
		return slxlh;
	}

	public void setSlxlh(String slxlh) {
		this.slxlh = slxlh;
	}
	
}