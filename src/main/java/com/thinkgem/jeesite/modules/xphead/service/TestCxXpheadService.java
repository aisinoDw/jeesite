/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.xphead.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.xphead.entity.TestCxXphead;
import com.thinkgem.jeesite.modules.xphead.dao.TestCxXpheadDao;

/**
 * 1Service
 * @author 1
 * @version 2016-02-25
 */
@Service
@Transactional(readOnly = true)
public class TestCxXpheadService extends CrudService<TestCxXpheadDao, TestCxXphead> {

	public TestCxXphead get(String id) {
		return super.get(id);
	}
	
	public List<TestCxXphead> findList(TestCxXphead testCxXphead) {
		return super.findList(testCxXphead);
	}
	
	public Page<TestCxXphead> findPage(Page<TestCxXphead> page, TestCxXphead testCxXphead) {
		return super.findPage(page, testCxXphead);
	}
	
	@Transactional(readOnly = false)
	public void save(TestCxXphead testCxXphead) {
		super.save(testCxXphead);
	}
	
	@Transactional(readOnly = false)
	public void delete(TestCxXphead testCxXphead) {
		super.delete(testCxXphead);
	}
	
}