/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.spjggl.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.spjggl.entity.TestGlSpjg;
import com.thinkgem.jeesite.modules.spjggl.dao.TestGlSpjgDao;

/**
 * 1Service
 * @author 1
 * @version 2016-01-19
 */
@Service
@Transactional(readOnly = true)
public class TestGlSpjgService extends CrudService<TestGlSpjgDao, TestGlSpjg> {

	public TestGlSpjg get(String id) {
		return super.get(id);
	}
	
	public List<TestGlSpjg> findList(TestGlSpjg testGlSpjg) {
		return super.findList(testGlSpjg);
	}
	
	public Page<TestGlSpjg> findPage(Page<TestGlSpjg> page, TestGlSpjg testGlSpjg) {
		return super.findPage(page, testGlSpjg);
	}
	
	@Transactional(readOnly = false)
	public void save(TestGlSpjg testGlSpjg) {
		super.save(testGlSpjg);
	}
	
	@Transactional(readOnly = false)
	public void delete(TestGlSpjg testGlSpjg) {
		super.delete(testGlSpjg);
	}
	
}