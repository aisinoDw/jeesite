/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.spjggl.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 1Entity
 * @author 1
 * @version 2016-01-19
 */
public class TestGlSpjg extends DataEntity<TestGlSpjg> {
	
	private static final long serialVersionUID = 1L;
	private String mc;		// 名称
	private String dj;		// 单价
	private String ggxh;		// 规格型号
	private String jldw;		// 计量单位
	private String shul;		// 数量
	private String sl;		// 税率
	
	public TestGlSpjg() {
		super();
	}

	public TestGlSpjg(String id){
		super(id);
	}

	@Length(min=1, max=255, message="名称长度必须介于 1 和 255 之间")
	public String getMc() {
		return mc;
	}

	public void setMc(String mc) {
		this.mc = mc;
	}
	
	@Length(min=0, max=255, message="单价长度必须介于 0 和 255 之间")
	public String getDj() {
		return dj;
	}

	public void setDj(String dj) {
		this.dj = dj;
	}
	
	@Length(min=0, max=255, message="规格型号长度必须介于 0 和 255 之间")
	public String getGgxh() {
		return ggxh;
	}

	public void setGgxh(String ggxh) {
		this.ggxh = ggxh;
	}
	
	@Length(min=0, max=255, message="计量单位长度必须介于 0 和 255 之间")
	public String getJldw() {
		return jldw;
	}

	public void setJldw(String jldw) {
		this.jldw = jldw;
	}
	
	@Length(min=0, max=255, message="数量长度必须介于 0 和 255 之间")
	public String getShul() {
		return shul;
	}

	public void setShul(String shul) {
		this.shul = shul;
	}
	
	@Length(min=0, max=255, message="税率长度必须介于 0 和 255 之间")
	public String getSl() {
		return sl;
	}

	public void setSl(String sl) {
		this.sl = sl;
	}
	
}