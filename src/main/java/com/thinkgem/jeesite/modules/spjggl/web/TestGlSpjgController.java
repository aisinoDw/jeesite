/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.spjggl.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.spjggl.entity.TestGlSpjg;
import com.thinkgem.jeesite.modules.spjggl.service.TestGlSpjgService;

/**
 * 1Controller
 * @author 1
 * @version 2016-01-19
 */
@Controller
@RequestMapping(value = "${adminPath}/spjggl/testGlSpjg")
public class TestGlSpjgController extends BaseController {

	@Autowired
	private TestGlSpjgService testGlSpjgService;
	
	@ModelAttribute
	public TestGlSpjg get(@RequestParam(required=false) String id) {
		TestGlSpjg entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = testGlSpjgService.get(id);
		}
		if (entity == null){
			entity = new TestGlSpjg();
		}
		return entity;
	}
	
	@RequiresPermissions("spjggl:testGlSpjg:view")
	@RequestMapping(value = {"list", ""})
	public String list(TestGlSpjg testGlSpjg, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<TestGlSpjg> page = testGlSpjgService.findPage(new Page<TestGlSpjg>(request, response), testGlSpjg); 
		model.addAttribute("page", page);
		return "modules/spjggl/testGlSpjgList";
	}

	@RequiresPermissions("spjggl:testGlSpjg:view")
	@RequestMapping(value = "form")
	public String form(TestGlSpjg testGlSpjg, Model model) {
		model.addAttribute("testGlSpjg", testGlSpjg);
		return "modules/spjggl/testGlSpjgForm";
	}

	@RequiresPermissions("spjggl:testGlSpjg:edit")
	@RequestMapping(value = "save")
	public String save(TestGlSpjg testGlSpjg, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, testGlSpjg)){
			return form(testGlSpjg, model);
		}
		testGlSpjgService.save(testGlSpjg);
		addMessage(redirectAttributes, "保存商品成功");
		return "redirect:"+Global.getAdminPath()+"/spjggl/testGlSpjg/?repage";
	}
	
	@RequiresPermissions("spjggl:testGlSpjg:edit")
	@RequestMapping(value = "delete")
	public String delete(TestGlSpjg testGlSpjg, RedirectAttributes redirectAttributes) {
		testGlSpjgService.delete(testGlSpjg);
		addMessage(redirectAttributes, "删除商品成功");
		return "redirect:"+Global.getAdminPath()+"/spjggl/testGlSpjg/?repage";
	}

}