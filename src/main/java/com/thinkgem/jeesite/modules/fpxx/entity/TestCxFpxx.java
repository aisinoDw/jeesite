/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.fpxx.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 1Entity
 * @author 1
 * @version 2016-02-26
 */
public class TestCxFpxx extends DataEntity<TestCxFpxx> {
	
	private static final long serialVersionUID = 1L;
	private String nsrsbh;		// 企业税号
	
	public TestCxFpxx() {
		super();
	}

	public TestCxFpxx(String id){
		super(id);
	}

	@Length(min=0, max=255, message="企业税号长度必须介于 0 和 255 之间")
	public String getNsrsbh() {
		return nsrsbh;
	}

	public void setNsrsbh(String nsrsbh) {
		this.nsrsbh = nsrsbh;
	}
	
}