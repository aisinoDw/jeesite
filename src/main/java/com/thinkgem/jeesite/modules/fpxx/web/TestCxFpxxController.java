/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.fpxx.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.fpxx.entity.TestCxFpxx;
import com.thinkgem.jeesite.modules.fpxx.service.TestCxFpxxService;

/**
 * 1Controller
 * @author 1
 * @version 2016-02-26
 */
@Controller
@RequestMapping(value = "${adminPath}/fpxx/testCxFpxx")
public class TestCxFpxxController extends BaseController {

	@Autowired
	private TestCxFpxxService testCxFpxxService;
	
	@ModelAttribute
	public TestCxFpxx get(@RequestParam(required=false) String id) {
		TestCxFpxx entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = testCxFpxxService.get(id);
		}
		if (entity == null){
			entity = new TestCxFpxx();
		}
		return entity;
	}
	
	@RequiresPermissions("fpxx:testCxFpxx:view")
	@RequestMapping(value = {"list", ""})
	public String list(TestCxFpxx testCxFpxx, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<TestCxFpxx> page = testCxFpxxService.findPage(new Page<TestCxFpxx>(request, response), testCxFpxx); 
		model.addAttribute("page", page);
		return "modules/fpxx/testCxFpxxList";
	}

	@RequiresPermissions("fpxx:testCxFpxx:view")
	@RequestMapping(value = "form")
	public String form(TestCxFpxx testCxFpxx, Model model) {
		model.addAttribute("testCxFpxx", testCxFpxx);
		return "modules/fpxx/testCxFpxxForm";
	}

	@RequiresPermissions("fpxx:testCxFpxx:edit")
	@RequestMapping(value = "save")
	public String save(TestCxFpxx testCxFpxx, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, testCxFpxx)){
			return form(testCxFpxx, model);
		}
		testCxFpxxService.save(testCxFpxx);
		addMessage(redirectAttributes, "保存发票成功");
		return "redirect:"+Global.getAdminPath()+"/fpxx/testCxFpxx/?repage";
	}
	
	@RequiresPermissions("fpxx:testCxFpxx:edit")
	@RequestMapping(value = "delete")
	public String delete(TestCxFpxx testCxFpxx, RedirectAttributes redirectAttributes) {
		testCxFpxxService.delete(testCxFpxx);
		addMessage(redirectAttributes, "删除发票成功");
		return "redirect:"+Global.getAdminPath()+"/fpxx/testCxFpxx/?repage";
	}

}