/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.fpxx.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.fpxx.entity.TestCxFpxx;
import com.thinkgem.jeesite.modules.fpxx.dao.TestCxFpxxDao;

/**
 * 1Service
 * @author 1
 * @version 2016-02-26
 */
@Service
@Transactional(readOnly = true)
public class TestCxFpxxService extends CrudService<TestCxFpxxDao, TestCxFpxx> {

	public TestCxFpxx get(String id) {
		return super.get(id);
	}
	
	public List<TestCxFpxx> findList(TestCxFpxx testCxFpxx) {
		return super.findList(testCxFpxx);
	}
	
	public Page<TestCxFpxx> findPage(Page<TestCxFpxx> page, TestCxFpxx testCxFpxx) {
		return super.findPage(page, testCxFpxx);
	}
	
	@Transactional(readOnly = false)
	public void save(TestCxFpxx testCxFpxx) {
		super.save(testCxFpxx);
	}
	
	@Transactional(readOnly = false)
	public void delete(TestCxFpxx testCxFpxx) {
		super.delete(testCxFpxx);
	}
	
}