/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.jhsj.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.jhsj.entity.TestJhSj;
import com.thinkgem.jeesite.modules.jhsj.dao.TestJhSjDao;

/**
 * 1Service
 * @author 1
 * @version 2016-01-14
 */
@Service
@Transactional(readOnly = true)
public class TestJhSjService extends CrudService<TestJhSjDao, TestJhSj> {

	public TestJhSj get(String id) {
		return super.get(id);
	}
	
	public List<TestJhSj> findList(TestJhSj testJhSj) {
		return super.findList(testJhSj);
	}
	
	public Page<TestJhSj> findPage(Page<TestJhSj> page, TestJhSj testJhSj) {
		return super.findPage(page, testJhSj);
	}
	
	@Transactional(readOnly = false)
	public void save(TestJhSj testJhSj) {
		super.save(testJhSj);
	}
	
	@Transactional(readOnly = false)
	public void delete(TestJhSj testJhSj) {
		super.delete(testJhSj);
	}
	
}