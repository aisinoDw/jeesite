/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.jhsj.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 1Entity
 * @author 1
 * @version 2016-01-14
 */
public class TestJhSj extends DataEntity<TestJhSj> {
	
	private static final long serialVersionUID = 1L;
	private String kprq;		// 开票日期
	private String hjje;		// 合计金额
	private String hjse;		// 合计税额
	private String xpsl;		// 小票数量
	
	public TestJhSj() {
		super();
	}

	public TestJhSj(String id){
		super(id);
	}

	@Length(min=1, max=255, message="开票日期长度必须介于 1 和 255 之间")
	public String getKprq() {
		return kprq;
	}

	public void setKprq(String kprq) {
		this.kprq = kprq;
	}
	
	@Length(min=0, max=255, message="合计金额长度必须介于 0 和 255 之间")
	public String getHjje() {
		return hjje;
	}

	public void setHjje(String hjje) {
		this.hjje = hjje;
	}
	
	@Length(min=0, max=255, message="合计税额长度必须介于 0 和 255 之间")
	public String getHjse() {
		return hjse;
	}

	public void setHjse(String hjse) {
		this.hjse = hjse;
	}
	
	@Length(min=0, max=255, message="小票数量长度必须介于 0 和 255 之间")
	public String getXpsl() {
		return xpsl;
	}

	public void setXpsl(String xpsl) {
		this.xpsl = xpsl;
	}
	
}