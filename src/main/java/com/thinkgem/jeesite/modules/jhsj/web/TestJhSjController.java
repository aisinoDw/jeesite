/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.jhsj.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.jhsj.entity.TestJhSj;
import com.thinkgem.jeesite.modules.jhsj.service.TestJhSjService;

/**
 * 1Controller
 * @author 1
 * @version 2016-01-14
 */
@Controller
@RequestMapping(value = "${adminPath}/jhsj/testJhSj")
public class TestJhSjController extends BaseController {

	@Autowired
	private TestJhSjService testJhSjService;
	
	@ModelAttribute
	public TestJhSj get(@RequestParam(required=false) String id) {
		TestJhSj entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = testJhSjService.get(id);
		}
		if (entity == null){
			entity = new TestJhSj();
		}
		return entity;
	}
	
	@RequiresPermissions("jhsj:testJhSj:view")
	@RequestMapping(value = {"list", ""})
	public String list(TestJhSj testJhSj, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<TestJhSj> page = testJhSjService.findPage(new Page<TestJhSj>(request, response), testJhSj); 
		List<TestJhSj> list = testJhSjService.findListGraph(testJhSj);
		model.addAttribute("page", page);
		model.addAttribute("list",list);
		return "modules/jhsj/testJhSjList";
	}

	@RequiresPermissions("jhsj:testJhSj:view")
	@RequestMapping(value = "form")
	public String form(TestJhSj testJhSj, Model model) {
		model.addAttribute("testJhSj", testJhSj);
		return "modules/jhsj/testJhSjForm";
	}

	@RequiresPermissions("jhsj:testJhSj:edit")
	@RequestMapping(value = "save")
	public String save(TestJhSj testJhSj, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, testJhSj)){
			return form(testJhSj, model);
		}
		testJhSjService.save(testJhSj);
		addMessage(redirectAttributes, "保存时间成功");
		return "redirect:"+Global.getAdminPath()+"/jhsj/testJhSj/?repage";
	}
	
	@RequiresPermissions("jhsj:testJhSj:edit")
	@RequestMapping(value = "delete")
	public String delete(TestJhSj testJhSj, RedirectAttributes redirectAttributes) {
		testJhSjService.delete(testJhSj);
		addMessage(redirectAttributes, "删除时间成功");
		return "redirect:"+Global.getAdminPath()+"/jhsj/testJhSj/?repage";
	}

}