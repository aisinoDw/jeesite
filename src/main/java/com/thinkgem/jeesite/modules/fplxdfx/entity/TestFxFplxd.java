/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.fplxdfx.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 1Entity
 * @author 1
 * @version 2016-01-19
 */
public class TestFxFplxd extends DataEntity<TestFxFplxd> {
	
	private static final long serialVersionUID = 1L;
	private String dqbh;		// 地区编号
	private String dqmc;		// 地区名称
	private String hjje;		// 合计金额
	private String hjse;		// 合计税额
	private String yf;		// 月份
	
	public TestFxFplxd() {
		super();
	}

	public TestFxFplxd(String id){
		super(id);
	}

	@Length(min=1, max=255, message="地区编号长度必须介于 1 和 255 之间")
	public String getDqbh() {
		return dqbh;
	}

	public void setDqbh(String dqbh) {
		this.dqbh = dqbh;
	}
	
	@Length(min=0, max=255, message="地区名称长度必须介于 0 和 255 之间")
	public String getDqmc() {
		return dqmc;
	}

	public void setDqmc(String dqmc) {
		this.dqmc = dqmc;
	}
	
	@Length(min=0, max=255, message="合计金额长度必须介于 0 和 255 之间")
	public String getHjje() {
		return hjje;
	}

	public void setHjje(String hjje) {
		this.hjje = hjje;
	}
	
	@Length(min=0, max=255, message="合计税额长度必须介于 0 和 255 之间")
	public String getHjse() {
		return hjse;
	}

	public void setHjse(String hjse) {
		this.hjse = hjse;
	}
	
	@Length(min=0, max=255, message="月份长度必须介于 0 和 255 之间")
	public String getYf() {
		return yf;
	}

	public void setYf(String yf) {
		this.yf = yf;
	}
	
}