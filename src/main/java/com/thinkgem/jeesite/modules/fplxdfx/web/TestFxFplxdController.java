/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.fplxdfx.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.fplxdfx.entity.TestFxFplxd;
import com.thinkgem.jeesite.modules.fplxdfx.service.TestFxFplxdService;

/**
 * 1Controller
 * @author 1
 * @version 2016-01-19
 */
@Controller
@RequestMapping(value = "${adminPath}/fplxdfx/testFxFplxd")
public class TestFxFplxdController extends BaseController {

	@Autowired
	private TestFxFplxdService testFxFplxdService;
	
	@ModelAttribute
	public TestFxFplxd get(@RequestParam(required=false) String id) {
		TestFxFplxd entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = testFxFplxdService.get(id);
		}
		if (entity == null){
			entity = new TestFxFplxd();
		}
		return entity;
	}
	
	@RequiresPermissions("fplxdfx:testFxFplxd:view")
	@RequestMapping(value = {"list", ""})
	public String list(TestFxFplxd testFxFplxd, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<TestFxFplxd> page = testFxFplxdService.findPage(new Page<TestFxFplxd>(request, response), testFxFplxd); 
		model.addAttribute("page", page);
		return "modules/fplxdfx/testFxFplxdList";
	}

	@RequiresPermissions("fplxdfx:testFxFplxd:view")
	@RequestMapping(value = "form")
	public String form(TestFxFplxd testFxFplxd, Model model) {
		model.addAttribute("testFxFplxd", testFxFplxd);
		return "modules/fplxdfx/testFxFplxdForm";
	}

	@RequiresPermissions("fplxdfx:testFxFplxd:edit")
	@RequestMapping(value = "save")
	public String save(TestFxFplxd testFxFplxd, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, testFxFplxd)){
			return form(testFxFplxd, model);
		}
		testFxFplxdService.save(testFxFplxd);
		addMessage(redirectAttributes, "保存发票成功");
		return "redirect:"+Global.getAdminPath()+"/fplxdfx/testFxFplxd/?repage";
	}
	
	@RequiresPermissions("fplxdfx:testFxFplxd:edit")
	@RequestMapping(value = "delete")
	public String delete(TestFxFplxd testFxFplxd, RedirectAttributes redirectAttributes) {
		testFxFplxdService.delete(testFxFplxd);
		addMessage(redirectAttributes, "删除发票成功");
		return "redirect:"+Global.getAdminPath()+"/fplxdfx/testFxFplxd/?repage";
	}

}