/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.fplxdfx.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.fplxdfx.entity.TestFxFplxd;
import com.thinkgem.jeesite.modules.fplxdfx.dao.TestFxFplxdDao;

/**
 * 1Service
 * @author 1
 * @version 2016-01-19
 */
@Service
@Transactional(readOnly = true)
public class TestFxFplxdService extends CrudService<TestFxFplxdDao, TestFxFplxd> {

	public TestFxFplxd get(String id) {
		return super.get(id);
	}
	
	public List<TestFxFplxd> findList(TestFxFplxd testFxFplxd) {
		return super.findList(testFxFplxd);
	}
	
	public Page<TestFxFplxd> findPage(Page<TestFxFplxd> page, TestFxFplxd testFxFplxd) {
		return super.findPage(page, testFxFplxd);
	}
	
	@Transactional(readOnly = false)
	public void save(TestFxFplxd testFxFplxd) {
		super.save(testFxFplxd);
	}
	
	@Transactional(readOnly = false)
	public void delete(TestFxFplxd testFxFplxd) {
		super.delete(testFxFplxd);
	}
	
}