<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>商品分类管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript" src="${ctxStatic}/graph/graphic_draw_Csxse.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/csxsefx/testFxCsxse/">商品分类列表</a></li>
		<shiro:hasPermission name="csxsefx:testFxCsxse:edit"><li><a href="${ctx}/csxsefx/testFxCsxse/form">商品分类添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="testFxCsxse" action="${ctx}/csxsefx/testFxCsxse/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">

			<li>
			<label>选择时间：</label>
			<form:select path="" onclick="time_change(this.value);" style="width:100px">
			<option value="2015">2015</option>
			<option value="2016">2016</option>
			</form:select>
			</li>
<!-- 			<li class="btns"><input id="graphSubmit" type="button"  class="btn btn-primary" value="查询界面"/></li> -->
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>商品分类</th>
				<th>分类销量</th>
				<th>统计时间</th>
				<th>年销售额</th>
				<shiro:hasPermission name="csxsefx:testFxCsxse:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="testFxCsxse">
			<tr>
				<td><a href="${ctx}/csxsefx/testFxCsxse/form?id=${testFxCsxse.id}">
					${testFxCsxse.spfl}
				</a></td>
				<td>
					${testFxCsxse.flxl}
				</td>
				<td>
					${testFxCsxse.tjsj}
				</td>
				<td>
					${testFxCsxse.nxse}
				</td>
				<shiro:hasPermission name="csxsefx:testFxCsxse:edit"><td>
    				<a href="${ctx}/csxsefx/testFxCsxse/form?id=${testFxCsxse.id}">修改</a>
					<a href="${ctx}/csxsefx/testFxCsxse/delete?id=${testFxCsxse.id}" onclick="return confirmx('确认要删除该商品分类吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
	<div id="Csxse1" style="height:600px;width:600px;float:left;display:none"  ></div>
	<div id="Csxse2" style="height:400px;width:480px;float:left;display:none" ></div>
</body>
</html>