<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>发票管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/fplydfx/testFxFplyd/">发票列表</a></li>
		<shiro:hasPermission name="fplydfx:testFxFplyd:edit"><li><a href="${ctx}/fplydfx/testFxFplyd/form">发票添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="testFxFplyd" action="${ctx}/fplydfx/testFxFplyd/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>地区编号：</label>
				<form:input path="dqbh" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>地区名称：</label>
				<form:input path="dqmc" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>地区编号</th>
				<th>地区名称</th>
				<th>合计金额</th>
				<th>合计税额</th>
				<th>月份</th>
				<shiro:hasPermission name="fplydfx:testFxFplyd:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="testFxFplyd">
			<tr>
				<td><a href="${ctx}/fplydfx/testFxFplyd/form?id=${testFxFplyd.id}">
					${testFxFplyd.dqbh}
				</a></td>
				<td>
					${testFxFplyd.dqmc}
				</td>
				<td>
					${testFxFplyd.hjje}
				</td>
				<td>
					${testFxFplyd.hjse}
				</td>
				<td>
					${testFxFplyd.yf}
				</td>
				<shiro:hasPermission name="fplydfx:testFxFplyd:edit"><td>
    				<a href="${ctx}/fplydfx/testFxFplyd/form?id=${testFxFplyd.id}">修改</a>
					<a href="${ctx}/fplydfx/testFxFplyd/delete?id=${testFxFplyd.id}" onclick="return confirmx('确认要删除该发票吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>