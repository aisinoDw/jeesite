<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>商品管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/spjgdq/testSpjgDq/">商品列表</a></li>
		<shiro:hasPermission name="spjgdq:testSpjgDq:edit"><li><a href="${ctx}/spjgdq/testSpjgDq/form">商品添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="testSpjgDq" action="${ctx}/spjgdq/testSpjgDq/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>商品名称：</label>
				<form:input path="mc" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>税务机关代码：</label>
				<form:input path="swjgdm" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>税务机关名称：</label>
				<form:input path="swjgmc" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>不含税单价：</label>
				<form:input path="dj" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>商品名称</th>
				<th>税务机关代码</th>
				<th>税务机关名称</th>
				<th>不含税单价</th>
				<shiro:hasPermission name="spjgdq:testSpjgDq:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="testSpjgDq">
			<tr>
				<td><a href="${ctx}/spjgdq/testSpjgDq/form?id=${testSpjgDq.id}">
					${testSpjgDq.mc}
				</a></td>
				<td>
					${testSpjgDq.swjgdm}
				</td>
				<td>
					${testSpjgDq.swjgmc}
				</td>
				<td>
					${testSpjgDq.dj}
				</td>
				<shiro:hasPermission name="spjgdq:testSpjgDq:edit"><td>
    				<a href="${ctx}/spjgdq/testSpjgDq/form?id=${testSpjgDq.id}">修改</a>
					<a href="${ctx}/spjgdq/testSpjgDq/delete?id=${testSpjgDq.id}" onclick="return confirmx('确认要删除该商品吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>