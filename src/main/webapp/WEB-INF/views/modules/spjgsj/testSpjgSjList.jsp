<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>商品管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
	<script type="text/javascript" src="{ctxStatic}/datepicker/WdatePicker.js"></script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/spjgsj/testSpjgSj/">商品列表</a></li>
		<shiro:hasPermission name="spjgsj:testSpjgSj:edit"><li><a href="${ctx}/spjgsj/testSpjgSj/form">商品添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="testSpjgSj" action="${ctx}/spjgsj/testSpjgSj/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>商品名称：</label>
				<form:input path="mc" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>开票日期：</label>
				<%-- <form:input path="kprq" htmlEscape="false" maxlength="255" class="input-medium"/> --%>
				<form:input path="kprq" type="text" style="width:90px" name="inputDate" value="201601" onclick="WdatePicker({el:this,isShowOthers:true,dateFmt:'yyyyMMdd'});"/>
			</li>
			<li><label>不含税单价：</label>
				<form:input path="dj" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>商品名称</th>
				<th>开票日期</th>
				<th>不含税单价</th>
				<shiro:hasPermission name="spjgsj:testSpjgSj:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="testSpjgSj">
			<tr>
				<td><a href="${ctx}/spjgsj/testSpjgSj/form?id=${testSpjgSj.id}">
					${testSpjgSj.mc}
				</a></td>
				<td>
					${testSpjgSj.kprq}
				</td>
				<td>
					${testSpjgSj.dj}
				</td>
				<shiro:hasPermission name="spjgsj:testSpjgSj:edit"><td>
    				<a href="${ctx}/spjgsj/testSpjgSj/form?id=${testSpjgSj.id}">修改</a>
					<a href="${ctx}/spjgsj/testSpjgSj/delete?id=${testSpjgSj.id}" onclick="return confirmx('确认要删除该商品吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>