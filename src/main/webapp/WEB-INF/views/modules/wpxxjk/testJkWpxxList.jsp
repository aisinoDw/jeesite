<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>纳税人管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript" src="${ctxStatic}/graph/graphic_draw_wpxx.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/wpxxjk/testJkWpxx/">纳税人列表</a></li>
		<shiro:hasPermission name="wpxxjk:testJkWpxx:edit"><li><a href="${ctx}/wpxxjk/testJkWpxx/form">纳税人添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="testJkWpxx" action="${ctx}/wpxxjk/testJkWpxx/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>纳税人识别号：</label>
				<form:input path="nsrsbh" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>纳税人名称：</label>
				<form:input path="nsrmc" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/>
			<input type="button" class="btn btn-primary" value="显示图表"  onclick="wpxx();"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>纳税人识别号</th>
				<th>纳税人名称</th>
				<th>合计金额</th>
				<th>无票金额</th>
				<th>无票比例</th>
				<shiro:hasPermission name="wpxxjk:testJkWpxx:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="testJkWpxx">
			<tr>
				<td><a href="${ctx}/wpxxjk/testJkWpxx/form?id=${testJkWpxx.id}">
					${testJkWpxx.nsrsbh}
				</a></td>
				<td>
					${testJkWpxx.nsrmc}
				</td>
				<td>
					${testJkWpxx.hjje}
				</td>
				<td>
					${testJkWpxx.wpje}
				</td>
				<td>
					${testJkWpxx.wpbl}
				</td>
				<shiro:hasPermission name="wpxxjk:testJkWpxx:edit"><td>
    				<a href="${ctx}/wpxxjk/testJkWpxx/form?id=${testJkWpxx.id}">修改</a>
					<a href="${ctx}/wpxxjk/testJkWpxx/delete?id=${testJkWpxx.id}" onclick="return confirmx('确认要删除该纳税人吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
	<div id="wpxx" style="height:400px;width:1200px;display:none"></div>
	
</body>
</html>