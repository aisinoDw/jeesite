<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>小票管理</title>
	<meta name="decorator" content="default"/>
	<link rel="stylesheet" type="text/css" href="${ctxStatic}/DataTables-1.10.10/media/css/jquery.dataTables.css">
	<script type="text/javascript" charset="utf8"  src="${ctxStatic}/DataTables-1.10.10/media/js/dataTables.bootstrap.js"></script>
	
	<script type="text/javascript">
		$(document).ready(function() {
			$('.datatable').dataTable();
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/xplist/testCxXplist/">小票列表</a></li>
		<shiro:hasPermission name="xplist:testCxXplist:edit"><li><a href="${ctx}/xplist/testCxXplist/form">小票添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="testCxXplist" action="${ctx}/xplist/testCxXplist/xplistjson" method="post" class="breadcrumb form-search">
<%-- 		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/> --%>
		<ul class="ul-form">
			<li><label>小票代码：</label>
				<form:input path="xpdm" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>小票号码：</label>
				<form:input path="xphm" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>受理序列号：</label>
				<form:input path="slxlh" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<%-- <table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>小票代码</th>
				<th>小票号码</th>
				<th>受理序列号</th>
				<shiro:hasPermission name="xplist:testCxXplist:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="testCxXplist">
			<tr>
				<td><a href="${ctx}/xplist/testCxXplist/form?id=${testCxXplist.id}">
					${testCxXplist.xpdm}
				</a></td>
				<td>
					${testCxXplist.xphm}
				</td>
				<td>
					${testCxXplist.slxlh}
				</td>
				<shiro:hasPermission name="xplist:testCxXplist:edit"><td>
    				<a href="${ctx}/xplist/testCxXplist/form?id=${testCxXplist.id}">修改</a>
					<a href="${ctx}/xplist/testCxXplist/delete?id=${testCxXplist.id}" onclick="return confirmx('确认要删除该小票吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div> --%>
	
	  <div class="row-fluid">
	<!-- <table id="searchTable" class="table table-striped table-bordered table-condensed" > -->
	<table id="searchTable" class="table table-striped table-bordered table-hover datatable">
		<thead>
			<tr>
				<th>小票代码</th>
				<th>小票号码</th>
				<th>账单号码</th>
				<th>验签状态</th>
				<th>验签信息</th>
				<th>验签状态</th>
				<th>处理信息</th>
				<th>处理结果</th>
				<th>存入时间</th>
				<th>返回代码</th>
				<th>支付备注</th>
				<th>服务器名称</th>

			</tr>
		</thead>
		<tbody>
		<c:if test="${!empty page1}">
		<c:forEach items="${page1}" var="testCxXpcx">
			<tr>
				<td>
					${testCxXpcx.xpdm}
				</td>
				<td>
					${testCxXpcx.xphm}
				</td>
				<td>
					${testCxXpcx.bill_no}
				</td>
				<td>
					${testCxXpcx.yqzt}
				</td>
				<td>
					${testCxXpcx.yq_msg}
				</td>
				<td>
					${testCxXpcx.yq_status}
				</td>
				<td>
					${testCxXpcx.error_message}
				</td>
				<td>
					${testCxXpcx.cljg}
				</td>
				<td>
					${testCxXpcx.insert_date}
				</td>
				<td>
					${testCxXpcx.response_code}
				</td>
				<td>
					${testCxXpcx.zfbz}
				</td>
				<td>
					${testCxXpcx.server_name}
				</td>		
			    </tr>
			    
		</c:forEach>
		</c:if>
		</tbody>
	</table>
	</div> 
	
</body>
</html>