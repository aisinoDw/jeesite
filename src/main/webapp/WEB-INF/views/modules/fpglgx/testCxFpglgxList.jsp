<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>发票管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/fpglgx/testCxFpglgx/">发票列表</a></li>
		<shiro:hasPermission name="fpglgx:testCxFpglgx:edit"><li><a href="${ctx}/fpglgx/testCxFpglgx/form">发票添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="testCxFpglgx" action="${ctx}/fpglgx/testCxFpglgx/fpglgxjson" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>发票代码：</label>
				<form:input path="fpdm" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>发票号码：</label>
				<form:input path="fphm" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
<%-- 	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>发票代码</th>
				<th>发票号码</th>
				<shiro:hasPermission name="fpglgx:testCxFpglgx:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="testCxFpglgx">
			<tr>
				<td><a href="${ctx}/fpglgx/testCxFpglgx/form?id=${testCxFpglgx.id}">
					${testCxFpglgx.fpdm}
				</a></td>
				<td>
					${testCxFpglgx.fphm}
				</td>
				<shiro:hasPermission name="fpglgx:testCxFpglgx:edit"><td>
    				<a href="${ctx}/fpglgx/testCxFpglgx/form?id=${testCxFpglgx.id}">修改</a>
					<a href="${ctx}/fpglgx/testCxFpglgx/delete?id=${testCxFpglgx.id}" onclick="return confirmx('确认要删除该发票吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body> --%>

	<table id="searchTable" class="table table-striped table-bordered table-hover datatable">
		<thead>
			<tr>
                <th>关系ID</th>
                <th>小票代码</th>
                <th>小票号码</th>
                <th>开票金额</th>
                <th>小票流水号</th>
                <th>企业税号</th>
                <th>价税合计</th>
                <th>不可开票金额</th>
                <th>对应发票状态</th>
            </tr>
		</thead>
		<tbody>
		<c:if test="${!empty page1}">
		<c:forEach items="${page1}" var="testCxXpcx">
			<tr>
				<td>
					${testCxXpcx.gxid}
				</td>
				<td>
					
				</td>
				<td>
					
				</td>
				<td>
					${testCxXpcx.kpje}
				</td>
				<td>
					${testCxXpcx.xplsh}
				</td>
				<td>
					${testCxXpcx.nsrsbh}
				</td>
				<td>
					${testCxXpcx.jshj}
				</td>
				<td>
					${testCxXpcx.bkkpje}
				</td>
				<td>
					${testCxXpcx.dyfpzt}
				</td>
			    </tr>
			    
		</c:forEach>
		</c:if>
		</tbody>
	</table>



</html>