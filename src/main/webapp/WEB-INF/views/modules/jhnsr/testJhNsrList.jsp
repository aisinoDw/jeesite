<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<%@ page import="java.util.*" %>
<%@ page import="com.thinkgem.jeesite.modules.jhnsr.entity.TestJhNsr" %>
<html>
<head>
	<title>纳税人管理</title>
	<meta name="decorator" content="default"/>
	<script src="${ctxStatic}/graph/graphic_draw.js" type="text/javascript"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#graphSubmit").click(function(){
				$("#contentTable").hide();
				$("#main1").show();
			})
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
	
	<%
	 ArrayList<TestJhNsr> list_test = (ArrayList<TestJhNsr>)request.getAttribute("list");
     List<String> arr_nsrmc = new ArrayList<String>();
     List<String> arr_hhje =  new ArrayList<String>();
     
	 for(int i=0;i<list_test.size();i++){
		 TestJhNsr test = (TestJhNsr)list_test.get(i);
		 arr_nsrmc.add(test.getNsrmc().toString());
		 arr_hhje.add(test.getHjje().toString());		
	 }
     request.setAttribute("arr_nsrmc",arr_nsrmc);
     request.setAttribute("arr_hhje",arr_hhje);

	%>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/jhnsr/testJhNsr/">纳税人列表</a></li>
		<shiro:hasPermission name="jhnsr:testJhNsr:edit"><li><a href="${ctx}/jhnsr/testJhNsr/form">纳税人添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="testJhNsr" action="${ctx}/jhnsr/testJhNsr/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<input id="arr_nsrmc" name="arr_nsrmc" type="hidden" value="${arr_nsrmc}"/>
		<input id="arr_hhje" name="arr_hhje" type="hidden" value="${arr_hhje}"/>
		<ul class="ul-form">
			<li><label>纳税人识别号：</label>
				<form:input path="nsrsbh" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>纳税人名称：</label>
				<form:input path="nsrmc" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li>&nbsp;&nbsp;&nbsp;</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="btns"><input id="graphSubmit" type="button" class="btn btn-primary" value="查询界面"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>纳税人识别号</th>
				<th>纳税人名称</th>
				<th>合计金额</th>
				<th>合计税额</th>
				<th>小票数量</th>
				<shiro:hasPermission name="jhnsr:testJhNsr:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="testJhNsr">
			<tr>
				<td><a href="${ctx}/jhnsr/testJhNsr/form?id=${testJhNsr.nsrsbh}">
					${testJhNsr.nsrsbh}
				</a></td>
				<td>
					${testJhNsr.nsrmc}
				</td>
				<td>
					${testJhNsr.hjje}
				</td>
				<td>
					${testJhNsr.hjse}
				</td>
				<td>
					${testJhNsr.xpsl}
				</td>
				<shiro:hasPermission name="jhnsr:testJhNsr:edit"><td>
    				<a href="${ctx}/jhnsr/testJhNsr/form?id=${testJhNsr.nsrsbh}">修改</a>
					<a href="${ctx}/jhnsr/testJhNsr/delete?id=${testJhNsr.nsrsbh}" onclick="return confirmx('确认要删除该纳税人吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
	<div id="main1" style="height:400px;width:800px;display:none;"></div>
</body>
</html>