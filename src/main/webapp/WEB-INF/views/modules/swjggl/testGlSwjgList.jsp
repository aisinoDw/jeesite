<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>税务机关管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/swjggl/testGlSwjg/">税务机关列表</a></li>
		<shiro:hasPermission name="swjggl:testGlSwjg:edit"><li><a href="${ctx}/swjggl/testGlSwjg/form">税务机关添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="testGlSwjg" action="${ctx}/swjggl/testGlSwjg/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>税务机关代码：</label>
				<form:input path="swjgdm" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>税务机关名称：</label>
				<form:input path="swjgmc" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>税务机关代码</th>
				<th>税务机关名称</th>
				<th>地区编号</th>
				<th>地区名称</th>
				<shiro:hasPermission name="swjggl:testGlSwjg:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="testGlSwjg">
			<tr>
				<td><a href="${ctx}/swjggl/testGlSwjg/form?id=${testGlSwjg.id}">
					${testGlSwjg.swjgdm}
				</a></td>
				<td>
					${testGlSwjg.swjgmc}
				</td>
				<td>
					${testGlSwjg.dqbh}
				</td>
				<td>
					${testGlSwjg.dqmc}
				</td>
				<shiro:hasPermission name="swjggl:testGlSwjg:edit"><td>
    				<a href="${ctx}/swjggl/testGlSwjg/form?id=${testGlSwjg.id}">修改</a>
					<a href="${ctx}/swjggl/testGlSwjg/delete?id=${testGlSwjg.id}" onclick="return confirmx('确认要删除该税务机关吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>