<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>

<html>
<head>
	<title>商品管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript" src="${ctxStatic}/graph/graphic_draw_glspxx.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/glspxx/testFxGlspxx/">商品列表</a></li>
		<shiro:hasPermission name="glspxx:testFxGlspxx:edit"><li><a href="${ctx}/glspxx/testFxGlspxx/form">商品添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="testFxGlspxx" action="${ctx}/glspxx/testFxGlspxx/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li>
			<label>选择关联：</label>
<%-- 			<form:select path="" onclick="goods_change(this.value);" style="width:100px"> --%>
<!-- 			<option value="guanxi">牛奶_麦片</option> -->
<!-- 			<option value="guanxis">牛奶_花生</option> -->
<%-- 			</form:select> --%>
            <input type="button" value="牛奶_麦片" onclick="goods_change(this.value);"/>
			</li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>商品名称</th>
				<th>合计金额</th>
				<th>开票日期</th>
				<th>纳税人名称</th>
				<shiro:hasPermission name="glspxx:testFxGlspxx:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="testFxGlspxx">
			<tr>
				<td><a href="${ctx}/glspxx/testFxGlspxx/form?id=${testFxGlspxx.id}">
					${testFxGlspxx.spmc}
				</a></td>
				<td>
					${testFxGlspxx.hjje}
				</td>
				<td>
					${testFxGlspxx.kprq}
				</td>
				<td>
					${testFxGlspxx.nsrmc}
				</td>
				<shiro:hasPermission name="glspxx:testFxGlspxx:edit"><td>
    				<a href="${ctx}/glspxx/testFxGlspxx/form?id=${testFxGlspxx.id}">修改</a>
					<a href="${ctx}/glspxx/testFxGlspxx/delete?id=${testFxGlspxx.id}" onclick="return confirmx('确认要删除该商品吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
	<div id="glspxx" style="height:400px;width:800px;display:none"></div>
</body>
</html>