<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>发票管理</title>
	<meta name="decorator" content="default"/>
	<link rel="stylesheet" type="text/css" href="${ctxStatic}/DataTables-1.10.10/media/css/jquery.dataTables.css">
	<script type="text/javascript" charset="utf8"  src="${ctxStatic}/DataTables-1.10.10/media/js/dataTables.bootstrap.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('.datatable').dataTable();
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/fpxx/testCxFpxx/">发票列表</a></li>
		<shiro:hasPermission name="fpxx:testCxFpxx:edit"><li><a href="${ctx}/fpxx/testCxFpxx/form">发票添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="testCxFpxx" action="${ctx}/fpxx/testCxFpxx/" method="post" class="breadcrumb form-search">
		<%-- <input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/> --%>
		<ul class="ul-form">
			<li><label>企业税号：</label>
				<form:input path="nsrsbh" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<%-- <table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>企业税号</th>
				<shiro:hasPermission name="fpxx:testCxFpxx:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="testCxFpxx">
			<tr>
				<td><a href="${ctx}/fpxx/testCxFpxx/form?id=${testCxFpxx.id}">
					${testCxFpxx.nsrsbh}
				</a></td>
				<shiro:hasPermission name="fpxx:testCxFpxx:edit"><td>
    				<a href="${ctx}/fpxx/testCxFpxx/form?id=${testCxFpxx.id}">修改</a>
					<a href="${ctx}/fpxx/testCxFpxx/delete?id=${testCxFpxx.id}" onclick="return confirmx('确认要删除该发票吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div> --%>
	
		  <div class="row-fluid">
	<!-- <table id="searchTable" class="table table-striped table-bordered table-condensed" > -->
	<table id="searchTable" class="table table-striped table-bordered table-hover datatable">
		<thead>
			<tr>
				<th>发票代码</th>
				<th>发票号码</th>
				<th>购方企业税号</th>
				<th>销方企业税号</th>
				<th>金额</th>
				<th>税额</th>
				<th>开票日期</th>
				<th>发票信息</th>

			</tr>
		</thead>
		<tbody>
		<c:if test="${!empty page1}">
		<c:forEach items="${page1}" var="testCxXpcx">
			<tr>
				<td>
					${testCxXpcx.fpdm}
				</td>
				<td>
					${testCxXpcx.fphm}
				</td>
				<td>
					${testCxXpcx.gfqysh}
				</td>
				<td>
					${testCxXpcx.xfqysh}
				</td>
				<td>
					${testCxXpcx.je}
				</td>
				<td>
					${testCxXpcx.se}
				</td>
				<td>
					${testCxXpcx.kprq}
				</td>
				<td>
					${testCxXpcx.fpxx}
				</td>
				
			    </tr>
			    
		</c:forEach>
		</c:if>
		</tbody>
	</table>
	</div> 
	
	
</body>
</html>