<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>纳税人管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/nsrydjk/testJkNsryd/">纳税人列表</a></li>
		<shiro:hasPermission name="nsrydjk:testJkNsryd:edit"><li><a href="${ctx}/nsrydjk/testJkNsryd/form">纳税人添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="testJkNsryd" action="${ctx}/nsrydjk/testJkNsryd/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>纳税人识别号：</label>
				<form:input path="nsrsbh" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>纳税人名称：</label>
				<form:input path="nsrmc" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>纳税人识别号</th>
				<th>纳税人名称</th>
				<th>风险值</th>
				<th>疑点编号</th>
				<th>疑点名称</th>
				<th>疑点明细</th>
				<shiro:hasPermission name="nsrydjk:testJkNsryd:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="testJkNsryd">
			<tr>
				<td><a href="${ctx}/nsrydjk/testJkNsryd/form?id=${testJkNsryd.id}">
					${testJkNsryd.nsrsbh}
				</a></td>
				<td>
					${testJkNsryd.nsrmc}
				</td>
				<td>
					${testJkNsryd.fxz}
				</td>
				<td>
					${testJkNsryd.ydbh}
				</td>
				<td>
					${testJkNsryd.ydmc}
				</td>
				<td>
					${testJkNsryd.ydmx}
				</td>
				<shiro:hasPermission name="nsrydjk:testJkNsryd:edit"><td>
    				<a href="${ctx}/nsrydjk/testJkNsryd/form?id=${testJkNsryd.id}">修改</a>
					<a href="${ctx}/nsrydjk/testJkNsryd/delete?id=${testJkNsryd.id}" onclick="return confirmx('确认要删除该纳税人吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>