<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>小票管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/jxxcyjk/testJkJxxcy/">小票列表</a></li>
		<shiro:hasPermission name="jxxcyjk:testJkJxxcy:edit"><li><a href="${ctx}/jxxcyjk/testJkJxxcy/form">小票添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="testJkJxxcy" action="${ctx}/jxxcyjk/testJkJxxcy/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>小票代码：</label>
				<form:input path="xpdm" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>小票号码：</label>
				<form:input path="xphm" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>小票代码</th>
				<th>小票号码</th>
				<th>开票日期</th>
				<th>合计金额</th>
				<th>合计税额</th>
				<th>购方税号</th>
				<th>购方名称</th>
				<th>购方联系方式</th>
				<th>销方税号</th>
				<th>销方名称</th>
				<th>销货方银行账号</th>
				<th>差异值</th>
				<th>差异等级</th>
				<th>差异明细</th>
				<shiro:hasPermission name="jxxcyjk:testJkJxxcy:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="testJkJxxcy">
			<tr>
				<td><a href="${ctx}/jxxcyjk/testJkJxxcy/form?id=${testJkJxxcy.id}">
					${testJkJxxcy.xpdm}
				</a></td>
				<td>
					${testJkJxxcy.xphm}
				</td>
				<td>
					${testJkJxxcy.kprq}
				</td>
				<td>
					${testJkJxxcy.hjje}
				</td>
				<td>
					${testJkJxxcy.hjse}
				</td>
				<td>
					${testJkJxxcy.gfsh}
				</td>
				<td>
					${testJkJxxcy.gfmc}
				</td>
				<td>
					${testJkJxxcy.gflxfs}
				</td>
				<td>
					${testJkJxxcy.xfsh}
				</td>
				<td>
					${testJkJxxcy.xfmc}
				</td>
				<td>
					${testJkJxxcy.xfyhzh}
				</td>
				<td>
					${testJkJxxcy.cyz}
				</td>
				<td>
					${testJkJxxcy.cydj}
				</td>
				<td>
					${testJkJxxcy.cymx}
				</td>
				<shiro:hasPermission name="jxxcyjk:testJkJxxcy:edit"><td>
    				<a href="${ctx}/jxxcyjk/testJkJxxcy/form?id=${testJkJxxcy.id}">修改</a>
					<a href="${ctx}/jxxcyjk/testJkJxxcy/delete?id=${testJkJxxcy.id}" onclick="return confirmx('确认要删除该小票吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>