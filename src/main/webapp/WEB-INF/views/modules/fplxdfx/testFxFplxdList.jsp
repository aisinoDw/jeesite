<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>发票管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/fplxdfx/testFxFplxd/">发票列表</a></li>
		<shiro:hasPermission name="fplxdfx:testFxFplxd:edit"><li><a href="${ctx}/fplxdfx/testFxFplxd/form">发票添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="testFxFplxd" action="${ctx}/fplxdfx/testFxFplxd/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>地区编号：</label>
				<form:input path="dqbh" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>地区名称：</label>
				<form:input path="dqmc" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>地区编号</th>
				<th>地区名称</th>
				<th>合计金额</th>
				<th>合计税额</th>
				<th>月份</th>
				<shiro:hasPermission name="fplxdfx:testFxFplxd:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="testFxFplxd">
			<tr>
				<td><a href="${ctx}/fplxdfx/testFxFplxd/form?id=${testFxFplxd.id}">
					${testFxFplxd.dqbh}
				</a></td>
				<td>
					${testFxFplxd.dqmc}
				</td>
				<td>
					${testFxFplxd.hjje}
				</td>
				<td>
					${testFxFplxd.hjse}
				</td>
				<td>
					${testFxFplxd.yf}
				</td>
				<shiro:hasPermission name="fplxdfx:testFxFplxd:edit"><td>
    				<a href="${ctx}/fplxdfx/testFxFplxd/form?id=${testFxFplxd.id}">修改</a>
					<a href="${ctx}/fplxdfx/testFxFplxd/delete?id=${testFxFplxd.id}" onclick="return confirmx('确认要删除该发票吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>