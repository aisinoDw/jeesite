<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<%@ page import="com.thinkgem.jeesite.modules.jhsj.entity.TestJhSj" %>
<%@ page import="java.util.*" %>

<html>
<head>
	<title>时间管理</title>
	<meta name="decorator" content="default"/>
	<script src="${ctxStatic}/graph/graphic_draw_sj.js" type="text/javascript"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$(document).ready(function() {
				$("#graphSubmit").click(function(){
					$("#contentTable").hide();
					$(".pagination").hide();
					$("#jhsj1").show();
				})
			});
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>	
	<%
	 ArrayList<TestJhSj> list_jhsj = (ArrayList<TestJhSj>)request.getAttribute("list");
     List<String> arr_kprq = new ArrayList<String>();
     List<String> arr_hjje =  new ArrayList<String>();
     List<String> arr_hjse =  new ArrayList<String>();
     
     
     System.out.println("list_jhsj");
	 for(int i=0;i<list_jhsj.size();i++){
		 TestJhSj test = (TestJhSj)list_jhsj.get(i);
		 
		 arr_kprq.add(test.getKprq().toString());
		 arr_hjse.add(test.getHjse().toString());
		 arr_hjje.add(test.getHjje().toString());	
	 }
     request.setAttribute("arr_kprq",arr_kprq);
     request.setAttribute("arr_hjse",arr_hjse);
     request.setAttribute("arr_hjje",arr_hjje);

	%>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/jhsj/testJhSj/">时间列表</a></li>
		<shiro:hasPermission name="jhsj:testJhSj:edit"><li><a href="${ctx}/jhsj/testJhSj/form">时间添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="testJhSj" action="${ctx}/jhsj/testJhSj/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		
		<input id="arr_kprq" name="arr_kprq" type="hidden" value="${arr_kprq}"/>
		<input id="arr_hjse" name="arr_hjse" type="hidden" value="${arr_hjse}"/>
		<input id="arr_hjje" name="arr_hjje" type="hidden" value="${arr_hjje}"/>
		
		<ul class="ul-form">
			<li><label>开票日期：</label>
				<form:input path="kprq" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>合计金额：</label>
				<form:input path="hjje" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>合计税额：</label>
				<form:input path="hjse" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>小票数量：</label>
				<form:input path="xpsl" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="btns"><input id="graphSubmit" class="btn btn-primary" type="button" value="查询图表"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>开票日期</th>
				<th>合计金额</th>
				<th>合计税额</th>
				<th>小票数量</th>
				<shiro:hasPermission name="jhsj:testJhSj:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="testJhSj">
			<tr>
				<td><a href="${ctx}/jhsj/testJhSj/form?id=${testJhSj.id}">
					${testJhSj.kprq}
				</a></td>
				<td>
					${testJhSj.hjje}
				</td>
				<td>
					${testJhSj.hjse}
				</td>
				<td>
					${testJhSj.xpsl}
				</td>
				<shiro:hasPermission name="jhsj:testJhSj:edit"><td>
    				<a href="${ctx}/jhsj/testJhSj/form?id=${testJhSj.id}">修改</a>
					<a href="${ctx}/jhsj/testJhSj/delete?id=${testJhSj.id}" onclick="return confirmx('确认要删除该时间吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
	<div id="jhsj1" style="height:400px;width:800px;display:none;"></div>
</body>
</html>