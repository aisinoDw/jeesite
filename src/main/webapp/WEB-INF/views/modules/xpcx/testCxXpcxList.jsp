<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>小票管理</title>
	<meta name="decorator" content="default"/>

	
		<link rel="stylesheet" type="text/css" href="${ctxStatic}/DataTables-1.10.10/media/css/jquery.dataTables.css">
		<!-- DataTables -->

		<%-- <script type="text/javascript" charset="utf8"  src="${ctxStatic}/DataTables-1.10.10/media/js/jquery.dataTables.js"></script> --%>
		<script type="text/javascript" charset="utf8"  src="${ctxStatic}/DataTables-1.10.10/media/js/dataTables.bootstrap.js"></script>
		<script src="${ctxStatic}/graph/xpcx.js" type="text/javascript"></script>
	<script type="text/javascript">

		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
	<script type="text/javascript" src="{ctxStatic}/datepicker/WdatePicker.js"></script>

	
	
</head>
<body>
	
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/xpcx/testCxXpcx/">小票列表</a></li>
		<shiro:hasPermission name="xpcx:testCxXpcx:edit"><li><a href="${ctx}/xpcx/testCxXpcx/form">小票添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="testCxXpcx" action="${ctx}/xpcx/testCxXpcx/xpxxjson" method="post" class="breadcrumb form-search">
		<%-- <input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/> --%>
		<ul class="ul-form">
			<li><label>企业税号：</label>
				<form:input path="nsrsbh" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>业务流水号：</label>
				<form:input path="ywlsh" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>报税起始日期：</label>
				<%-- <form:input path="bsqsrq" htmlEscape="false" maxlength="255" class="input-medium"/> --%>
				<form:input path="bsqsrq" type="text" style="width:90px" name="bsqsrq" value="" onclick="WdatePicker({el:this,isShowOthers:true,dateFmt:'yyyyMMddHHmm'});"/>
			</li>
			<li><label>报税截止日期：</label>
				<%-- <form:input path="bsjzrq" htmlEscape="false" maxlength="255" class="input-medium"/> --%>
				<form:input path="bsjzrq" type="text" style="width:90px" name="bsjzrq" value="" onclick="WdatePicker({el:this,isShowOthers:true,dateFmt:'yyyyMMddHHmm'});"/>
			</li>
			<li><label>小票代码：</label>
				<form:input path="xpdm" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>小票号码：</label>
				<form:input path="xphm" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<!-- <li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li> -->
			<!-- <li class="btns"><input id="searchSubmit" type="button"  class="btn btn-primary" value="小票查询"/></li> -->
			<li class="btns"><input id="searchSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
<%-- 	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>企业税号</th>
				<th>业务流水号</th>
				<th>报税起始日期</th>
				<th>报税截止日期</th>
				<th>小票代码</th>
				<th>小票号码</th>
				<shiro:hasPermission name="xpcx:testCxXpcx:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="testCxXpcx">
			<tr>
				<td><a href="${ctx}/xpcx/testCxXpcx/form?id=${testCxXpcx.id}">
					${testCxXpcx.nsrsbh}
				</a></td>
				<td>
					${testCxXpcx.ywlsh}
				</td>
				<td>
					${testCxXpcx.bsqsrq}
				</td>
				<td>
					${testCxXpcx.bsjzrq}
				</td>
				<td>
					${testCxXpcx.xpdm}
				</td>
				<td>
					${testCxXpcx.xphm}
				</td>
				<shiro:hasPermission name="xpcx:testCxXpcx:edit"><td>
    				<a href="${ctx}/xpcx/testCxXpcx/form?id=${testCxXpcx.id}">修改</a>
					<a href="${ctx}/xpcx/testCxXpcx/delete?id=${testCxXpcx.id}" onclick="return confirmx('确认要1删除该小票吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div> --%>
  	<div class="row-fluid">
	<!-- <table id="searchTable" class="table table-striped table-bordered table-condensed" > -->
	<table id="searchTable" class="table table-striped table-bordered table-hover datatable">
		<thead>
			<tr>
				<th>企业税号</th>
				<th>小票代码</th>
				<th>小票号码</th>
				<th>合计金额</th>
				<th>合计税额</th>
				<th>开票日期</th>
				<th>小票信息</th>
<!-- 				<th width="1px"></th>
				<th width="1px"></th>
				<th width="1px"></th> -->
			</tr>
		</thead>
		<tbody>
		<c:if test="${!empty page1}">
		<c:forEach items="${page1}" var="testCxXpcx">
			<tr>
				<td>
					${testCxXpcx.nsrsbh}
				</td>
				<td>
					${testCxXpcx.xpdm}
				</td>
				<td>
					${testCxXpcx.xphm}
				</td>
				<td>
					${testCxXpcx.hjje}
				</td>
				<td>
					${testCxXpcx.hjse}
				</td>
				<td>
					${testCxXpcx.kprq}
				</td>
    			 <td>
    				<li class="btn btn-primary"><a  href="javascript:void(0)" type="button"  value="结算信息" onclick="openJsxx(this);" style="color:white" >结算信息</a> </li>
    				<!-- <li class="btns"><input id="jsSubmit" type="button"  class="btn btn-primary" value="结算信息" data-toggle="modal" data-target="#jsxxModal"/></li> -->
    				<!-- <li class="btns"><input id="mxSubmit" type="button"  class="btn btn-primary" value="明细信息" data-toggle="modal" data-target="#mxxxModal"/></li> -->
    				<li class="btn btn-primary"><a href="javascript:void(0)" type="button"  value="明细信息" onclick="openMxxx(this);" style="color:white" >明细信息</a> </li>
    				<!-- <li class="btns"><input id="qtSubmit" type="button"  class="btn btn-primary" value="其他信息" data-toggle="modal" data-target="#qtxxModal"/></li> -->
    				<!-- <li class="btn btn-primary"><a href="javascript:void(0)" type="button"  value="其他信息" onclick="openQtxx(this);" style="color:white;z-index:1051;position:relative" >其他信息</a></li> -->
    				<li class="btn btn-primary"><a href="javascript:void(0)" type="button"  value="其他信息" onclick="openQtxx(this);" style="color:white" >其他信息</a></li>
<!-- 				</td>

				<td width="1px"> -->
		
					<!-- 结算信息弹出框 -->
					<div class="modal fade" id="${testCxXpcx.xphm}js" aria-hidden="true" style="z-index:-1">
					<div class="modal-dialog">
					    <div class="modal-content">
				     	<form id="saveDeviceFormJs" action="saveDevice" method="post">
				            <div class="modal-header">
				           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

				            <h3>结算信息</h3>
				            </div>
				            <div class="modal-body">
				                <ul>
				                  <table id="jsTable" class="table table-striped table-bordered table-condensed" >
									<thead>
										<tr>
											<th>小票号码</th>
											<th>小票代码</th>
											<th>金额</th>
											<th>结算方式</th>
											<th>卡号</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${testCxXpcx.jslist}" var="jsList">
											<tr>
												<td>
													${testCxXpcx.xphm}
												</td>
												<td>
													${testCxXpcx.xpdm}
												</td>
												<td>
													${jsList.je}
												</td>
												<td>
													${jsList.jsfs}
												</td>
												<td>
													${jsList.kh}
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>		
				
				                </ul>
				            </div>
				            <div class="modal-footer">
		                <!-- <a class="button" data-dismiss="modal" aria-hidden="true">确定</a> -->
		                <button type="button" class="btn btn-default" data-dismiss="modal">确定</button>
				            </div>
				        </form>
				    </div>
				    </div>
				    </div>
				   
				

	<!-- 		    </td>

				<td width="1px" > -->
					
					<!-- 明细信息弹出框 -->
					<div class="modal fade" id="${testCxXpcx.xphm}mx" aria-hidden="true" style="z-index:-1">
					<div class="modal-dialog">
					    <div class="modal-content">
				     	<form id="saveDeviceFormMx" action="saveDevice" method="post">
				            <div class="modal-header">
				            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
				            <h3>明细信息</h3>
				            </div>
				            <div class="modal-body">
				                <ul>
				                  <table id="jsTable" class="table table-striped table-bordered table-condensed" >
									<thead>
										<tr>
											<th>小票号码</th>
											<th>小票代码</th>
											<th>金额</th>
											<th>名称</th>
											<th>单价</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${testCxXpcx.mxlist}" var="mxList">
											<tr>
												<td>
													${testCxXpcx.xphm}
												</td>
												<td>
													${testCxXpcx.xpdm}
												</td>
												<td>
													${mxList.je}
												</td>
												<td>
													${mxList.mc}
												</td>
												<td>
													${mxList.dj}
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>		
				
				                </ul>
				            </div>
				            <div class="modal-footer">
				            <button type="button" class="btn btn-default" data-dismiss="modal">确定</button>
		                <!-- <a class="button" data-dismiss="modal" aria-hidden="true">确定</a> -->
		               <!--  <a class="button" onclick="submitOK()">确定</a> -->
				            </div>
				        </form>
				    </div>
				    </div>
				    </div>

<!-- 			    </td>
			    <td width="1px"> -->
			    	<!-- 其他信息弹出框 -->
					<div class="modal fade bs-example-modal-lg" id="qtxxModalLabel" aria-hidden="true" style="left: 300px;width:1000px;z-index:-1;top: 1px;height: 450px;">
					<div class="modal-dialog modal-lg">
					    <div class="modal-content">
				     	<form id="saveDeviceFormQt" action="saveDevice" method="post">
				            <div class="modal-header">
				            <button type="button" class="close" data-dismiss="modal">×</button>
				            <h3>其他信息</h3>
				            </div>
				            <div class="modal-body" style="max-height: 300px">
				                <ul>
				                    <!-- <li><span>企业税号:</span><input id="nsrsbh1" type="text" name="personName"/></li>
				                    <li><span>小票代码:</span><input id="xpdm1" type="text" name="xpdm1"/></li> -->
				                    <table class="table table-bordered table-responsive table-striped table-condensed" style="table-layout:fixed; word-wrap:break-word;">
				
				                            <tbody><tr>
				                                <th>购方名称</th>
				                                <td>${testCxXpcx.gfmc}</td>
				                                <th>购方税号</th>
				                                <td>${testCxXpcx.gfsh}</td>
				                                <th>购方联系方式</th>
				                                <td>${testCxXpcx.gflxfs}</td>
				
				                            </tr>
				                            <tr>
				                                <th>销方税号</th>
				                                <td>${testCxXpcx.xfsh}</td>
				                                <th>销货方银行账号</th>
				                                <td>${testCxXpcx.xfyhzh}</td>
				                                <th>销货方地址电话</th>
				                                <td>${testCxXpcx.xfdzdh}</td>
				                            </tr>
				                            <tr>
				                                <th>纳税人名称</th>
				                                <td>${testCxXpcx.nsrmc}</td>
				                                <th>销售类型</th>
				                                <td>${testCxXpcx.xslx}</td>
				                                <th>交易类型</th>
				                                <td>${testCxXpcx.jylx}</td>
				                            </tr>
				                            <tr>
				                                <th>价税合计</th>
				                                <td>${testCxXpcx.jshj}</td>
				                                <th>不可开票金额</th>
				                                <td>${testCxXpcx.bkkpje}</td>
				                                <th>不可开票说明</th>
				                                <td>${testCxXpcx.bkkpsm}</td>
				                            </tr>
				                            <tr>
				                                <th>收款员</th>
				                                <td>${testCxXpcx.sky}</td>
				                                <th>代开标志</th>
				                                <td>${testCxXpcx.dkbz}</td>
				                                <th>交易小票类别</th>
				                                <td>${testCxXpcx.jyxplb}</td>
				                            </tr>
				                            <tr>
				                                <th>小票赋码日期</th>
				                                <td>${testCxXpcx.fmrq}</td>
				                                <th>小票开票日期</th>
				                                <td>${testCxXpcx.kprq}</td>
				                                <th>小票入库日期</th>
				                                <td>${testCxXpcx.insertTime}</td>
				                            </tr>
				
				                            <tr>
				                                <th>商品行行数</th>
				                                <td>${testCxXpcx.sphhs}</td>
				                                <th>税控设备编号</th>
				                                <td>${testCxXpcx.sbbh}</td>
				                                <th>行业扩展字段</th>
				                                <td>${testCxXpcx.hykzzd}</td>
				                            </tr>
				                            <tr>
				                                <th>退换货原因</th>
				                                <td>${testCxXpcx.thhyy}</td>
				                                <th>小票备注</th>
				                                <td>${testCxXpcx.bz}</td>
				                                <th>小票校验码</th>
				                                <td>${testCxXpcx.jym}</td>
				                            </tr>
				                            <tr>
				                                <th>原小票代码</th>
				                                <td></td>
				                                <th>原小票号码</th>
				                                <td></td>
				                                <th>原流水号</th>
				                                <td></td>
				                            </tr>
				                            <tr>
				                                <th>小票签名</th>
				                                <td style="text-align: center; overflow:hidden;" colspan="5" class="ng-binding">${testCxXpcx.sign}</td>
				                            </tr>
				                        </tbody></table>
				
				                </ul>
				            </div>
				            <div class="modal-footer">
				            	<button type="button" class="btn btn-default" data-dismiss="modal">确定</button>
				                <!-- <a class="button" data-dismiss="modal" aria-hidden="true">确定</a> -->
				               <!--  <a class="button" onclick="submitOK()">确定</a> -->
				            </div>
				        </form>
				    </div>
				    </div>
				    </div>

			    </td> 
			    </tr>
			    
		</c:forEach>
		</c:if>
		</tbody>
	</table>
	</div> 
	
	
</body>
</html>