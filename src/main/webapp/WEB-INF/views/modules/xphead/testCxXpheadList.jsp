<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>小票管理</title>
	<meta name="decorator" content="default"/>
	
	<link rel="stylesheet" type="text/css" href="${ctxStatic}/DataTables-1.10.10/media/css/jquery.dataTables.css">
	<script type="text/javascript" charset="utf8"  src="${ctxStatic}/DataTables-1.10.10/media/js/dataTables.bootstrap.js"></script>
	
	<script type="text/javascript">
		$(document).ready(function() {
			$('.datatable').dataTable();
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/xphead/testCxXphead/">小票列表</a></li>
		<shiro:hasPermission name="xphead:testCxXphead:edit"><li><a href="${ctx}/xphead/testCxXphead/form">小票添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="testCxXphead" action="${ctx}/xphead/testCxXphead/xpheadjson" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>受理序列号：</label>
				<form:input path="slxlh" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
<%-- 	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>受理序列号</th>
				<shiro:hasPermission name="xphead:testCxXphead:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="testCxXphead">
			<tr>
				<td><a href="${ctx}/xphead/testCxXphead/form?id=${testCxXphead.id}">
					${testCxXphead.slxlh}
				</a></td>
				<shiro:hasPermission name="xphead:testCxXphead:edit"><td>
    				<a href="${ctx}/xphead/testCxXphead/form?id=${testCxXphead.id}">修改</a>
					<a href="${ctx}/xphead/testCxXphead/delete?id=${testCxXphead.id}" onclick="return confirmx('确认要删除该小票吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div> --%>
	
	
	<div class="row-fluid">
		<table id="searchTable" class="table table-striped table-bordered table-hover datatable">
		<thead>
			<tr>
				<th>文件名称</th>
				<th>收款类型</th>
				<th>小票数量</th>
				<th>操作类型</th>
				<th>处理状态</th>
				<th>账单号码</th>
				<th>存入时间</th>

			</tr>
		</thead>
		<tbody>
		<c:if test="${!empty page1}">
		<c:forEach items="${page1}" var="testCxXpcx">
			<tr>
				<td>
					${testCxXpcx.filt_name}
				</td>
				<td>
					${testCxXpcx.sk_type}
				</td>
				<td>
					${testCxXpcx.xpsl}
				</td>
				<td>
					${testCxXpcx.opt_type}
				</td>
				<td>
					${testCxXpcx.cl_zt}
				</td>
				<td>
					${testCxXpcx.bill_no}
				</td>
				<td>
					${testCxXpcx.insertTime}
				</td>
						
			    </tr>
			    
		</c:forEach>
		</c:if>
		</tbody>
	</table>
	</div> 
	
	
</body>
</html>