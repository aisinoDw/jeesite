<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>销方管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/xxfx/testFxXx/">销方列表</a></li>
		<shiro:hasPermission name="xxfx:testFxXx:edit"><li><a href="${ctx}/xxfx/testFxXx/form">销方添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="testFxXx" action="${ctx}/xxfx/testFxXx/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>销方税号：</label>
				<form:input path="xfsh" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>销方名称：</label>
				<form:input path="xfmc" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>销方税号</th>
				<th>销方名称</th>
				<th>销方联系方式</th>
				<th>销货方银行账号</th>
				<th>合计金额</th>
				<th>合计税额</th>
				<shiro:hasPermission name="xxfx:testFxXx:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="testFxXx">
			<tr>
				<td><a href="${ctx}/xxfx/testFxXx/form?id=${testFxXx.id}">
					${testFxXx.xfsh}
				</a></td>
				<td>
					${testFxXx.xfmc}
				</td>
				<td>
					${testFxXx.xflxfs}
				</td>
				<td>
					${testFxXx.xfyhzh}
				</td>
				<td>
					${testFxXx.hjje}
				</td>
				<td>
					${testFxXx.hjse}
				</td>
				<shiro:hasPermission name="xxfx:testFxXx:edit"><td>
    				<a href="${ctx}/xxfx/testFxXx/form?id=${testFxXx.id}">修改</a>
					<a href="${ctx}/xxfx/testFxXx/delete?id=${testFxXx.id}" onclick="return confirmx('确认要删除该销方吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>