<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>小票管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
	<script type="text/javascript" src="{ctxStatic}/datepicker/WdatePicker.js"></script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/xptj/testCxXptj/">小票列表</a></li>
		<shiro:hasPermission name="xptj:testCxXptj:edit"><li><a href="${ctx}/xptj/testCxXptj/form">小票添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="testCxXptj" action="${ctx}/xptj/testCxXptj/xptjjson" method="post" class="breadcrumb form-search">
		<%-- <input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/> --%>
		<ul class="ul-form">
			<li><label>企业税号：</label>
				<form:input path="nsrsbh" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>开票机号：</label>
				<form:input path="kpjh" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>报税起始日期：</label>
				<form:input path="bsqsrq" type="text" style="width:90px" name="bsqsrq" value="" onclick="WdatePicker({el:this,isShowOthers:true,dateFmt:'yyyyMMddHHmm'});"/>
				<%-- <form:input path="bsqsrq" htmlEscape="false" maxlength="255" class="input-medium"/> --%>
			</li>
			<li><label>报税截止日期：</label>
				<form:input path="bsjzrq" type="text" style="width:90px" name="bsjzrq" value="" onclick="WdatePicker({el:this,isShowOthers:true,dateFmt:'yyyyMMddHHmm'});"/>
				<%-- <form:input path="bsjzrq" htmlEscape="false" maxlength="255" class="input-medium"/> --%>
			</li>
			<li><label>正负票：</label>
				<form:select path="zfp" style="width:120px">  
           <option value class >请选择</option>
           <form:option value="0">正</form:option>
           <form:option value="1">负</form:option>
 
        </form:select>
				<%-- <form:input path="zfp" htmlEscape="false" maxlength="255" class="input-medium"/> --%>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
<%-- 	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>企业税号</th>
				<th>开票机号</th>
				<th>报税起始日期</th>
				<th>报税截止日期</th>
				<th>正负票</th>
				<shiro:hasPermission name="xptj:testCxXptj:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="testCxXptj">
			<tr>
				<td><a href="${ctx}/xptj/testCxXptj/form?id=${testCxXptj.id}">
					${testCxXptj.nsrsbh}
				</a></td>
				<td>
					${testCxXptj.kpjh}
				</td>
				<td>
					${testCxXptj.bsqsrq}
				</td>
				<td>
					${testCxXptj.bsjzrq}
				</td>
				<td>
					${testCxXptj.zfp}
				</td>
				<shiro:hasPermission name="xptj:testCxXptj:edit"><td>
    				<a href="${ctx}/xptj/testCxXptj/form?id=${testCxXptj.id}">修改</a>
					<a href="${ctx}/xptj/testCxXptj/delete?id=${testCxXptj.id}" onclick="return confirmx('确认要删除该小票吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table> --%>
	<!-- <table id="searchTable" class="table table-striped table-bordered table-condensed" > -->
	<table id="searchTable" class="table table-striped table-bordered table-hover datatable">
		<thead>
			<tr>
				<th>#</th>
				<th>数量统计</th>
				<th>金额统计</th>
				<th>税额统计</th>
			</tr>
		</thead>
		<tbody>
		<c:if test="${!empty page1}">
		<c:forEach items="${page1}" var="testCxXpcx">
			<tr>
				<td>
					统计结果
				</td>
				<td>
					${testCxXpcx.slzj}
				</td>
				<td>
					${testCxXpcx.hjje}
				</td>
				<td>
					${testCxXpcx.hjse}
				</td>

			    </tr>
			    
		</c:forEach>
		</c:if>
		</tbody>
	</table>

</body>
</html>