<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>购方管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/jxfx/testFxJx/">购方列表</a></li>
		<shiro:hasPermission name="jxfx:testFxJx:edit"><li><a href="${ctx}/jxfx/testFxJx/form">购方添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="testFxJx" action="${ctx}/jxfx/testFxJx/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>购方税号：</label>
				<form:input path="gfsh" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>购方名称：</label>
				<form:input path="gfmc" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>购方税号</th>
				<th>购方名称</th>
				<th>购方联系方式</th>
				<th>合计金额</th>
				<th>合计税额</th>
				<shiro:hasPermission name="jxfx:testFxJx:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="testFxJx">
			<tr>
				<td><a href="${ctx}/jxfx/testFxJx/form?id=${testFxJx.id}">
					${testFxJx.gfsh}
				</a></td>
				<td>
					${testFxJx.gfmc}
				</td>
				<td>
					${testFxJx.gflxfs}
				</td>
				<td>
					${testFxJx.hjje}
				</td>
				<td>
					${testFxJx.hjse}
				</td>
				<shiro:hasPermission name="jxfx:testFxJx:edit"><td>
    				<a href="${ctx}/jxfx/testFxJx/form?id=${testFxJx.id}">修改</a>
					<a href="${ctx}/jxfx/testFxJx/delete?id=${testFxJx.id}" onclick="return confirmx('确认要删除该购方吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>