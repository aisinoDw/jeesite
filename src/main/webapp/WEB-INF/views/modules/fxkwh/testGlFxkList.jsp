<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>风险管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/fxkwh/testGlFxk/">风险列表</a></li>
		<shiro:hasPermission name="fxkwh:testGlFxk:edit"><li><a href="${ctx}/fxkwh/testGlFxk/form">风险添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="testGlFxk" action="${ctx}/fxkwh/testGlFxk/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>风险代码：</label>
				<form:input path="fxdm" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>风险名称：</label>
				<form:input path="fxmc" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>风险代码</th>
				<th>风险名称</th>
				<th>风险值</th>
				<th>录入人</th>
				<th>录入日期</th>
				<shiro:hasPermission name="fxkwh:testGlFxk:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="testGlFxk">
			<tr>
				<td><a href="${ctx}/fxkwh/testGlFxk/form?id=${testGlFxk.id}">
					${testGlFxk.fxdm}
				</a></td>
				<td>
					${testGlFxk.fxmc}
				</td>
				<td>
					${testGlFxk.fxz}
				</td>
				<td>
					${testGlFxk.lrr}
				</td>
				<td>
					${testGlFxk.lrrq}
				</td>
				<shiro:hasPermission name="fxkwh:testGlFxk:edit"><td>
    				<a href="${ctx}/fxkwh/testGlFxk/form?id=${testGlFxk.id}">修改</a>
					<a href="${ctx}/fxkwh/testGlFxk/delete?id=${testGlFxk.id}" onclick="return confirmx('确认要删除该风险吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>