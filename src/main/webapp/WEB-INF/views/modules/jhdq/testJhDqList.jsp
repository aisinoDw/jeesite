<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<%@ page import="com.thinkgem.jeesite.modules.jhdq.entity.TestJhDq" %>
<%@ page import="java.util.*" %>
<html>
<head>
	<title>地区管理</title>
	<meta name="decorator" content="default"/>
	<script src="${ctxStatic}/graph/graphic_draw_dq.js" type="text/javascript"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$(document).ready(function() {
				$("#graphSubmit").click(function(){
					$("#contentTable").hide();
					$(".pagination").hide();
					$("#jhdq1").show();			
				})
			});
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
	
		<%
	 ArrayList<TestJhDq> list_jhdq = (ArrayList<TestJhDq>)request.getAttribute("list");
     List<String> arr_swjgmc = new ArrayList<String>();
     List<String> arr_hjse =  new ArrayList<String>();
     
     
	    
     for(int i=0;i<list_jhdq.size();i++){
		 TestJhDq test = (TestJhDq)list_jhdq.get(i);
		 arr_swjgmc.add(test.getSwjgmc().toString());
		 arr_hjse.add(test.getHjse().toString());		
	 }
     request.setAttribute("arr_swjgmc",arr_swjgmc);
     request.setAttribute("arr_hjse",arr_hjse);

	%>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/jhdq/testJhDq/">地区列表</a></li>
		<shiro:hasPermission name="jhdq:testJhDq:edit"><li><a href="${ctx}/jhdq/testJhDq/form">地区添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="testJhDq" action="${ctx}/jhdq/testJhDq/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<input id="arr_swjgmc" name="arr_swjgmc" type="hidden" value="${arr_swjgmc}"/>
		<input id="arr_hjse" name="arr_hjse" type="hidden" value="${arr_hjse}"/>
		<ul class="ul-form">
			<li><label>税务机关代码：</label>
				<form:input path="swjgdm" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>税务机关名称：</label>
				<form:input path="swjgmc" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="btns"><input id="graphSubmit" type="button"  class="btn btn-primary" value="查询界面"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>税务机关代码</th>
				<th>税务机关名称</th>
				<th>合计税额</th>
				<th>小票数量</th>
				<shiro:hasPermission name="jhdq:testJhDq:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="testJhDq">
			<tr>
				<td><a href="${ctx}/jhdq/testJhDq/form?id=${testJhDq.id}">
					${testJhDq.swjgdm}
				</a></td>
				<td>
					${testJhDq.swjgmc}
				</td>
				<td>
					${testJhDq.hjse}
				</td>
				<td>
					${testJhDq.xpsl}
				</td>
				<shiro:hasPermission name="jhdq:testJhDq:edit"><td>
    				<a href="${ctx}/jhdq/testJhDq/form?id=${testJhDq.id}">修改</a>
					<a href="${ctx}/jhdq/testJhDq/delete?id=${testJhDq.id}" onclick="return confirmx('确认要删除该地区吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
	<div id="jhdq1" style="height:400px;width:800px;display:none;"></div>
</body>
</html>