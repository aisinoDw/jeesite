/**
 * 
 */
var spfl = [];
var nxse = [];
var ser = []; 
var myChart = null;
var myChart2 = null;


 function time_change(value){

	

	$.ajax( {
		type : "get",
		url : "/jeesite/a/csxsefx/testFxCsxse/ajaxsefx",
		data: {"year":value},
		dataType:"json",
		success : function(list) {

		//alert(list.length);//12
	    //alert(list[0].length);//10

		for(var p = 0 ; p<list[0].length ;p++){
			
				spfl.push(list[0][p].spfl);
				nxse.push({"value":list[0][p].nxse,"name":list[0][p].spfl});			
		}
		
		var tArray = new Array();   //先声明一维
		for(var k=0;k<list.length;k++){        //一维长度为i,i为变量，可以根据实际情况改变

		tArray[k]=new Array();    //声明二维，每一个一维数组里面的一个元素都是一个数组；

		for(var j=0;j<list[0].length;j++){      //一维数组里面每个元素数组可以包含的数量p，p也是一个变量；

		tArray[k][j]="";       //这里将变量初始化，我这边统一初始化为空，后面在用所需的值覆盖里面的值
		 }
		}
		
		for(var i = 0 ; i<list.length ;i ++){
		for(var p = 0 ; p<list[i].length ;p++){
			
			tArray[p][i] = list[i][p].flxl;
		}
		}

		for(var i = 0 ; i<list[0].length ;i ++){
		ser.push({"name":spfl[i],"type":'bar',"stack":'总量',"data":tArray[i]});
		}		
		draw_graph(list);
		},
        error: function (XMLHttpRequest, textStatus, errorThrown) {
        alert(321);
} 
	});
	
	
	
}

function draw_graph(list){
	
	$("#contentTable").hide();
	$(".pagination").hide();
	$("#Csxse1").show();
	$("#Csxse2").show();
    require.config({
        paths: {
            echarts: '/jeesite/static/dist'
        }
    });     
	
    require(
            [
                'echarts',
                'echarts/chart/bar', // 使用柱状图就加载bar模块，按需加载
                'echarts/chart/pie',
                'echarts/chart/line'
            ],
            function (ec) {
                // 基于准备好的dom，初始化echarts图表
//                if(myChart!=null&&myChart2!=null){
//                	
//                }

                myChart = ec.init(document.getElementById('Csxse1')); 
                myChart2 = ec.init(document.getElementById('Csxse2'));   
                
            
                option = {
                	    title : {
                	        text: '商超销售额关联分析',
                	        subtext: '',
                	        x:'center'
                	    },
                	    tooltip : {
                	        trigger: 'item',
                	        formatter: "{a} <br/>{b} : {c} ({d}%)"
                	    },
                	    legend: {
                	        orient : 'vertical',
                	        x : 'left',
                	        data:spfl
                	    },
                	    calculable : true,
                	    series : [
                	        {
                	            name:'访问来源',
                	            type:'pie',
                	            radius : '55%',
                	            center: ['50%', 225],
                	            data:nxse
                	        }
                	    ]
                	};

                	option2 = {
                	    tooltip : {
                	        trigger: 'axis',
                	        axisPointer : {
                	            type: 'shadow'
                	        }
                	    },
                	    legend: {
                	        data:spfl
                	    },
                	    toolbox: {
                	        show : true,
                	        orient : 'vertical',
                	        y : 'center',
                	        feature : {
                	            mark : {show: true},
                	            magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                	            restore : {show: true},
                	            saveAsImage : {show: true}
                	        }
                	    },
                	    calculable : true,
                	    xAxis : [
                	        {
                	            type : 'category',
                	            data : ['一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月']
                	        }
                	    ],
                	    yAxis : [
                	        {
                	            type : 'value',
                	            splitArea : {show : true}
                	        }
                	    ],
                	    grid: {
                	        x2:40
                	    },
                	    series :ser
               	    	
                	};
                    myChart.setOption(option); 
                	myChart2.setOption(option2);

                	myChart.connect(myChart2);
                	myChart2.connect(myChart);
                	
                	


                	setTimeout(function (){
                	    window.onresize = function () {
                	        myChart.resize();
                	        myChart2.resize();
                	    }
                	},200)
                	                    
                        
                	                    
        
                // 为echarts对象加载数据 
               
             


            }
        );
	
	
	

    
    
    
}