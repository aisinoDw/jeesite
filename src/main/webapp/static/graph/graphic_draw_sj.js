/**
 * 
 */
   // 路径配置
        require.config({
            paths: {
                echarts: '/jeesite/static/dist'
            }
        });        
             
        // 使用
        require(
            [
                'echarts',
                'echarts/chart/line',
                'echarts/chart/bar',
                'echarts/chart/pie'// 使用柱状图就加载bar模块，按需加载
            ],
            function (ec) {
                // 基于准备好的dom，初始化echarts图表
                var myChart = ec.init(document.getElementById('jhsj1'));
                
                var arr_kprq,arr_hjse,arr_hjje;     
                
                
                arr_kprq = document.getElementById('arr_kprq').value;
                arr_hjse = document.getElementById('arr_hjse').value;
                arr_hjje = document.getElementById('arr_hjje').value;
                
                
                arr_kprq = (arr_kprq.toString()).substring(1,arr_kprq.length-1);
                arr_kprq = arr_kprq.split(",");                
                arr_hjse = (arr_hjse.toString()).substring(1,arr_hjse.length-1);
                arr_hjse = arr_hjse.split(",");
                arr_hjje = (arr_hjje.toString()).substring(1,arr_hjje.length-1);
                arr_hjje = arr_hjje.split(",");
                
                
        
       
        
              var   option = {
                	    tooltip : {
                	        trigger: 'axis'
                	    },
                	    legend: {
                	        data:['合计税额','合计金额']
                	    },
                	    toolbox: {
                	        show : true,
                	        feature : {
                	            mark : {show: true},
                	            dataView : {show: true, readOnly: false},
                	            magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                	            restore : {show: true},
                	            saveAsImage : {show: true}
                	        }
                	    },
                	    calculable : true,
                	    xAxis : [
                	        {
                	            type : 'category',
                	            boundaryGap : false,
                	            data : arr_kprq
                	        }
                	    ],
                	    yAxis : [
                	        {
                	            type : 'value'
                	        }
                	    ],
                	    series : [
                	        {
                	            name:'合计税额',
                	            type:'line',
                	            stack: '总量',
                	            data:arr_hjse
                	        },
                	        {
                	            name:'合计金额',
                	            type:'line',
                	            stack: '总量',
                	            data:arr_hjje
                	        }
                	    ]
                	};
                	                    
                			                
                // 为echarts对象加载数据 
                myChart.setOption(option); 
              
            }
        );
        
