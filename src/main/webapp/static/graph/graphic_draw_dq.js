/**
 * 
 */
   // 路径配置
        require.config({
            paths: {
                echarts: '/jeesite/static/dist'
            }
        });        
             
        // 使用
        require(
            [
                'echarts',
                'echarts/chart/pie' // 使用柱状图就加载bar模块，按需加载
            ],
            function (ec) {
                // 基于准备好的dom，初始化echarts图表
                var myChart = ec.init(document.getElementById('jhdq1'));
                
                var arr_swjgmc,arr_hhse;             
                arr_swjgmc = document.getElementById('arr_swjgmc').value;
                arr_hjse = document.getElementById('arr_hjse').value;
                var arr_swjgmc = (arr_swjgmc.toString()).substring(1,arr_swjgmc.length-1);
                arr_swjgmc = arr_swjgmc.split(",");                
                var arr_hjse = (arr_hjse.toString()).substring(1,arr_hjse.length-1);
                arr_hjse = arr_hjse.split(",");
                var hdsb =[];
                for(var i=0; i<arr_swjgmc.length;i++){
                  hdsb.push( {  value : arr_hjse[i] ,  name : arr_swjgmc[i] } );
                }           
               
      
                var option = {
                	    title : {
                	        text: '聚合地区',
                	        x:'center'
                	    },
                	    tooltip : {
                	        trigger: 'item',
                	        formatter: "{a} <br/>{b} : {c} ({d}%)"
                	    },
                	    legend: {
                	        orient : 'vertical',
                	        x : 'left',
                	        data:arr_swjgmc
                	    },
                	    toolbox: {
                	        show : true,
                	        feature : {
                	            mark : {show: true},
                	            dataView : {show: true, readOnly: false},
                	            magicType : {
                	                show: true, 
                	                type: ['pie', 'funnel'],
                	                option: {
                	                    funnel: {
                	                        x: '25%',
                	                        width: '50%',
                	                        funnelAlign: 'left',
                	                        max: 1548
                	                    }
                	                }
                	            },
                	            restore : {show: true},
                	            saveAsImage : {show: true}
                	        }
                	    },
                	    calculable : true,
                	    series : [
                	        {
                	            name:'访问来源',
                	            type:'pie',
                	            radius : '55%',
                	            center: ['50%', '60%'],
                	            data: hdsb
                	        }
                	    ]
                	};
                	                    
        
                // 为echarts对象加载数据 
                myChart.setOption(option); 
                document.getElementById('jhdq1').style.dispaly="none";
            }
        );