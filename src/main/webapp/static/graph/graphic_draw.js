/**
 * 
 */

   // 路径配置
        require.config({
            paths: {
                echarts: '/jeesite/static/dist'
            }
        });        
             
        // 使用
        require(
            [
                'echarts',
                'echarts/chart/bar' // 使用柱状图就加载bar模块，按需加载
            ],
            function (ec) {
                // 基于准备好的dom，初始化echarts图表
                var myChart = ec.init(document.getElementById('main1'));
                
                var arr_nsrmc,arr_hhje;             
                arr_nsrmc = document.getElementById('arr_nsrmc').value;
                arr_hhje = document.getElementById('arr_hhje').value;
                var arr_nsrmc = (arr_nsrmc.toString()).substring(1,arr_nsrmc.length-1);
                arr_nsrmc = arr_nsrmc.split(",");                
                var arr_hhje = (arr_hhje.toString()).substring(1,arr_hhje.length-1);
                arr_hhje = arr_hhje.split(",");
               
      
                var option = {
                    tooltip: {
                        show: true
                    },
                    legend: {
                        data:['合计金额']
                    },
                    xAxis : [
                        {
                            type : 'category',
                            data : arr_nsrmc
                        }
                    ],
                    yAxis : [
                        {
                            type : 'value'
                        }
                    ],
                    series : [
                        {
                            "name":"合计金额",
                            "type":"bar",
                            "data":arr_hhje
                        }
                    ]
                };
        
                // 为echarts对象加载数据 
                myChart.setOption(option); 
                document.getElementById('main1').style.dispaly="none";
            }
        );